Documentación/Documentation:
1) Descripción general/general description:
Español/Spanish
Sistema de recomendación desarrollado en java como trabajo fin de grado. Implementa métricas de rating
como RMSE o MAE y métricas de ranking como Ndcg, mrr, precision y recall. El sistema desarrollado se
basa en vecinos (k vecinos por definir). Admite normalización por mean centering. Mains de ejemplo se pueden
encontrar en:
-src/es/uam/eps/tfg/system/test/alt/mains
Y el más importante de todos es:
src/es/uam/eps/tfg/system/test/alt/ConsoleComparison.java

Inglés/English:
Recommender system developed in java as a end of degree project. It implements rating metrics as 
RMSE and MAE and ranking ones like Ndcg, mrr, precision and recall. It is a neighborhood based
recommerder system (k needs to be chosen). It admits normalization by mean centering. 
Examples of mains can de found in:
-src/es/uam/eps/tfg/system/test/alt/mains
And the most important one is:
src/es/uam/eps/tfg/system/test/alt/ConsoleComparison.java


2) Distribución de los ficheros/File distribution:

Español/Spanish:
En el subdirectorio de src se encuentran los ficheros fuentes de la aplicación. En documentation/javadoc, se encuentra
la documentación (javadoc) de todo el sistema completo. Hay también un directorio de diagrams
donde se encuentran los diagramas de clases de las estructuras (dentro del directorio
de documentation), los nodos y el sistema de evaluación. El directorio de
results, contiene un excel con los resultados de los recomendadores con varias métricas.
El resto de carpetas con la nomenclatura results_timemem... contienen resultados específicos 
sobre distintas configuraciones de las estructuras.

Inglés/English:
In the src folder, the source code can be found. In documentation/javadoc, it is located the documentation
of the application (javadoc). The is also a diagram directory (inside documentation/javadoc), in which you can find the structures diagrams,
the nodes diagram and the evaluationClass diagram. In results directory there in an excel file where you can see the 
result of some metrics. The rest of folders that contains results_timemem... contains specific results of
different configurations of the structures.