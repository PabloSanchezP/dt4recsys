package es.uam.eps.tfg.test.structures.trees;

import java.io.FileOutputStream;
import java.io.PrintStream;

import es.uam.eps.tfg.datatest.Key;
import es.uam.eps.tfg.structures.trees.bsearchtrees.RedBlackTree;

/***
 * Test for Red-Black-Tree. It writes in a txt file two columns: -Number of
 * nodes -Time in nanoseconds spent to search those nodes. After doing that, you
 * can plot the file and you should see a logarithmic function.
 * 
 * @author Pablo
 * 
 */
public class RedBlackTreeTest {
	public static void main(String[] args) {
		int incr = 20000;
		int i, j, MAX = 12000000;
		Key[] array;
		RedBlackTree<Key> t = new RedBlackTree<Key>();
		array = new Key[MAX];
		for (i = 0, j = 0; i < MAX; i++, j++) {
			array[i] = new Key(i);
			t.insert(array[i]);
			if (j == incr) {
				System.out.println(i + " elementos insertados");
				j = 0;
			}
		}
		try {
			FileOutputStream bsearch;
			PrintStream pbs;
			bsearch = new FileOutputStream("RED-BLACK.txt");
			pbs = new PrintStream(bsearch);

			System.out.println("START");
			long time1, time2;
			for (i = incr; i < MAX; i += incr) {
				time1 = System.nanoTime();
				if (t.search(array[i]) == null)
					System.out.println("ERROR!");
				time2 = System.nanoTime();
			}

			for (i = incr; i < MAX; i += incr) {
				time1 = System.nanoTime();
				if (t.search(array[i]) == null)
					System.out.println("ERROR!");
				time2 = System.nanoTime();
				pbs.println(i + " " + (time2 - time1));
			}

			pbs.close();
			System.out.println("END");
		} catch (Exception e) {
			System.err.println("Error writing to file");
		}

	}
}
