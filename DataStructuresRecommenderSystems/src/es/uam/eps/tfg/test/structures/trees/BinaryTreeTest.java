package es.uam.eps.tfg.test.structures.trees;

import java.io.FileOutputStream;
import java.io.PrintStream;

import es.uam.eps.tfg.datatest.Key;
import es.uam.eps.tfg.structures.trees.bsearchtrees.BinarySearchTree;

/***
 * Class for testing binary tree worst case behavior. The function obtained
 * should be linear. (X)
 * 
 * @author Pablo
 * 
 */
public class BinaryTreeTest {
	public static void main(String[] args) {
		int incr = 5000;
		int i, j, MAX = 200000;
		Key[] array;
		BinarySearchTree<Key> t = new BinarySearchTree<Key>();
		array = new Key[MAX];
		for (i = 0, j = 0; i < MAX; i++, j++) {
			array[i] = new Key(i);
			t.insert(array[i]);
			if (j == incr) {
				System.out.println(i + " elementos insertados");
				j = 0;
			}
		}
		try {
			FileOutputStream bsearch;
			PrintStream pbs;
			bsearch = new FileOutputStream("BSEARCH.txt");
			pbs = new PrintStream(bsearch);

			System.out.println("START");
			long time1, time2;
			for (i = incr; i < MAX; i += incr) {
				time1 = System.nanoTime();
				if (t.search(array[i]) == null)
					System.out.println("ERROR!");
				time2 = System.nanoTime();
			}

			for (i = incr; i < MAX; i += incr) {
				time1 = System.nanoTime();
				if (t.search(array[i]) == null)
					System.out.println("ERROR!");
				time2 = System.nanoTime();
				pbs.println(i + " " + (time2 - time1));
			}

			pbs.close();
			System.out.println("END");
		} catch (Exception e) {
			System.err.println("Error writing to file");
		}
	}
}
