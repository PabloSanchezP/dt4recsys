package es.uam.eps.tfg.test.structures.heaps;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;

import es.uam.eps.tfg.datatest.Key;
import es.uam.eps.tfg.structures.trees.bheaps.MaxHeap;

/***
 * Class for testing Heapsort asymptotic behavior. If you plot the file, you
 * should obtain a function x*log(x)
 * 
 * @author Pablo
 * 
 */
public class MaxHeapTest {
	public static void main(String[] args) {
		int items = 10000;
		int total = 1000000;
		FileOutputStream binaryheap;
		PrintStream pbh;
		try {
			binaryheap = new FileOutputStream("HeapTest.txt");
			pbh = new PrintStream(binaryheap);
			for (int j = items; j < total; j += items) {
				Integer[] arr = new Integer[j];
				MaxHeap<Key> heap = new MaxHeap<Key>(j);
				for (int i = 0; i < arr.length; i++) {
					arr[i] = i;
				}
				Collections.shuffle(Arrays.asList(arr));
				for (int i = 0; i < arr.length; i++) {
					heap.insert(new Key(arr[i]));
				}
				long time1, time2;
				time1 = System.nanoTime();
				heap.heapSort();
				time2 = System.nanoTime();
				pbh.println(j + " " + (time2 - time1));
				System.out.println("Iteracion 1: " + j + "elementos");
			}
		} catch (Exception e) {
			System.err.println("Error writing to file");
		}
	}
}
