package es.uam.eps.tfg.test.algorithms;

import es.uam.eps.tfg.algorithms.MatrixOptimalOrder;

/**
 * Class to test The matrix optimal order algorithm.
 * 
 * @author Pablo
 * 
 */
public class MatrixOptimalOrderTest {
	public static void main(String[] args) {

		int[] matrices = new int[7];
		matrices[0] = 30;
		matrices[1] = 35;
		matrices[2] = 15;
		matrices[3] = 5;
		matrices[4] = 10;
		matrices[5] = 20;
		matrices[6] = 25;
		MatrixOptimalOrder nueva = new MatrixOptimalOrder();

		nueva.matrixChainOrder(matrices);
		nueva.printOptimalParenthesis();

	}
}
