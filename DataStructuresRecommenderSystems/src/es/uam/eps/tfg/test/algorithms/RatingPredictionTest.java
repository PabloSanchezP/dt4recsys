package es.uam.eps.tfg.test.algorithms;

import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General;
import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases.BIAS_TYPE;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetRandomRS;
import es.uam.eps.tfg.system.test.alt.ratingprediction.neighbourhood.NeighBourhoodSimilarity;

public class RatingPredictionTest {
	private static final int NEIGHBORS = 10;

	public static void main(String[] args) {
		for (String rec : new String[] {
				"Baseline Random",
				"Baseline Biase",
				"Baseline User Bias",
				"Baseline Item Bias",
				"Baseline System Bias", //
				"Pearson Total0 UB Std", "Pearson Total0 UB MC",
				"Pearson Total3 UB Std",
				"Pearson Total3 UB MC",
				"Pearson Normal UB Std",
				"Pearson Normal UB MC", //
				"Cosine Normal UB Std",
				"Cosine Normal UB MC",
				"LCS UB", //
				"Pearson Total0 IB Std", "Pearson Total0 IB MC",
				"Pearson Total3 IB Std", "Pearson Total3 IB MC",
				"Pearson Normal IB Std", "Pearson Normal IB MC", //
				"Cosine Normal IB Std", "Cosine Normal IB MC", "LCS IB" }) {
			// create dataset
			RecommenderSystemIF<Long, Long> dataset = buildDataset(rec);
			fillDataset(dataset);

			// train recommender
			dataset.train(false);

			// check result
			test1_10(dataset, rec);
			test2_2(dataset, rec);
		}
	}

	private static RecommenderSystemIF<Long, Long> buildDataset(String rec) {
		RecommenderSystemIF<Long, Long> dataset = null;
		if (rec.equals("Baseline Random")) {
			dataset = new BaselineDatasetRandomRS();
		} else if (rec.equals("Baseline Biase")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.COMBINED);
		} else if (rec.equals("Baseline User Bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.USER);
		} else if (rec.equals("Baseline Item Bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.ITEM);
		} else if (rec.equals("Baseline System Bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.SYSTEM);
		} else if (rec.equals("Pearson Total0 UB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total0",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Pearson Total3 UB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total3",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Pearson Total0 UB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total0",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("Pearson Total3 UB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total3",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("Pearson Normal UB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Pearson Normal UB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("Cosine Normal UB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Cosine Normal UB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			// } else if (rec.equals("Cosine Total UB")) {
			// dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Total",
			// Structures.HashTable_RedBlack,
			// ReccomendationScheme.UserBased);
			// ((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS UB")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "LCS",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
		} else if (rec.equals("Pearson Total0 IB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total0",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Pearson Total0 IB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total0",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("Pearson Total3 IB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total3",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Pearson Total3 IB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Total3",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("Pearson Normal IB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Pearson Normal IB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("Cosine Normal IB Std")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("Cosine Normal IB MC")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			// } else if (rec.equals("Cosine Total IB")) {
			// dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Total",
			// Structures.HashTable_RedBlack,
			// ReccomendationScheme.ItemBased);
			// ((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS IB")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "LCS",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
		}
		return dataset;
	}

	private static void fillDataset(RecommenderSystemIF<Long, Long> dataset) {
		dataset.addInteraction(1L, 1L, 5.0);
		dataset.addInteraction(1L, 2L, 4.0);
		dataset.addInteraction(1L, 3L, 3.0);
		dataset.addInteraction(2L, 1L, 2.0);
		dataset.addInteraction(2L, 3L, 5.0);
		dataset.addInteraction(2L, 10L, 5.0);
		dataset.addInteraction(3L, 1L, 4.0);
		dataset.addInteraction(3L, 2L, 5.0);
		dataset.addInteraction(3L, 3L, 3.0);
		dataset.addInteraction(3L, 10L, 2.0);
	}

	public static void test1_10(RecommenderSystemIF<Long, Long> dataset,
			String rec) {
		Long testUser = 1L;
		Long testItem = 10L;

		// check result
		boolean correctResult = false;
		double predictedRating = dataset.predictInteraction(testUser, testItem);
		double computedPrediction = -predictedRating;
		if (rec.equals("Baseline Random")) {
			// always correct (it is impossible to know if it is
			// correct/incorrect)
			computedPrediction = predictedRating;
		} else if (rec.equals("Baseline Biase")) {
			computedPrediction = 3.8 + (4.0 - 3.8) + (3.5 - 3.8);
		} else if (rec.equals("Baseline User Bias")) {
			computedPrediction = 4.0;
		} else if (rec.equals("Baseline Item Bias")) {
			computedPrediction = 3.5;
		} else if (rec.equals("Baseline System Bias")) {
			computedPrediction = 3.8;
		} else if (rec.equals("Pearson Total0 UB Std")) {
			computedPrediction = -0.26;
		} else if (rec.equals("Pearson Total3 UB Std")) {
			computedPrediction = -2.02;
		} else if (rec.equals("Pearson Normal UB Std")) {
			computedPrediction = -2.83;
		} else if (rec.equals("Cosine Normal UB Std")) {
			computedPrediction = 3.01;
		} else if (rec.equals("Cosine Total UB Std")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS UB Std")) {
			computedPrediction = 0; // TODO
		} else if (rec.equals("Pearson Total0 IB Std")) {
			computedPrediction = -1.61;
		} else if (rec.equals("Pearson Total3 IB Std")) {
			computedPrediction = -1.67;
		} else if (rec.equals("Pearson Normal IB Std")) {
			computedPrediction = -1.91;
		} else if (rec.equals("Cosine Normal IB Std")) {
			computedPrediction = 3.77;
		} else if (rec.equals("Cosine Total IB Std")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS IB Std")) {
			computedPrediction = 0; // TODO
		} else if (rec.equals("Pearson Total0 UB MC")) {
			computedPrediction = 2.66;
		} else if (rec.equals("Pearson Total3 UB MC")) {
			computedPrediction = 2.79;
		} else if (rec.equals("Pearson Normal UB MC")) {
			computedPrediction = 2.84;
		} else if (rec.equals("Cosine Normal UB MC")) {
			computedPrediction = 3.34;
		} else if (rec.equals("Cosine Total UB MC")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS UB MC")) {
			computedPrediction = 0; // TODO
		} else if (rec.equals("Pearson Total0 IB MC")) {
			computedPrediction = 2.72;
		} else if (rec.equals("Pearson Total3 IB MC")) {
			computedPrediction = 2.98;
		} else if (rec.equals("Pearson Normal IB MC")) {
			computedPrediction = 3.05;
		} else if (rec.equals("Cosine Normal IB MC")) {
			computedPrediction = 3.46;
		} else if (rec.equals("Cosine Total IB MC")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS IB MC")) {
			computedPrediction = 0; // TODO
		}
		correctResult = Math.abs(predictedRating - computedPrediction) < 0.01;

		System.out.println(rec + ": "
				+ (correctResult ? "correct" : "incorrect") + " result for ("
				+ testUser + ", " + testItem + ")");
		if (!correctResult) {
			System.out.println(rec + ": " + predictedRating + " vs "
					+ computedPrediction);
		}
	}

	public static void test2_2(RecommenderSystemIF<Long, Long> dataset,
			String rec) {
		Long testUser = 2L;
		Long testItem = 2L;

		// check result
		boolean correctResult = false;
		double predictedRating = dataset.predictInteraction(testUser, testItem);
		double computedPrediction = -predictedRating;
		if (rec.equals("Baseline Random")) {
			// always correct (it is impossible to know if it is
			// correct/incorrect)
			computedPrediction = predictedRating;
		} else if (rec.equals("Baseline Biase")) {
			computedPrediction = 3.8 + (4.0 - 3.8) + (4.5 - 3.8);
		} else if (rec.equals("Baseline User Bias")) {
			computedPrediction = 4.0;
		} else if (rec.equals("Baseline Item Bias")) {
			computedPrediction = 4.5;
		} else if (rec.equals("Baseline System Bias")) {
			computedPrediction = 3.8;
		} else if (rec.equals("Pearson Total0 UB Std")) {
			computedPrediction = -4.71;
		} else if (rec.equals("Pearson Total3 UB Std")) {
			computedPrediction = -4.47;
		} else if (rec.equals("Pearson Normal UB Std")) {
			computedPrediction = -4.44;
		} else if (rec.equals("Cosine Normal UB Std")) {
			computedPrediction = 4.56;
		} else if (rec.equals("Cosine Total UB Std")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS UB Std")) {
			computedPrediction = 0; // TODO
		} else if (rec.equals("Pearson Total0 IB Std")) {
			computedPrediction = -2.27;
		} else if (rec.equals("Pearson Total3 IB Std")) {
			computedPrediction = -3.10;
		} else if (rec.equals("Pearson Normal IB Std")) {
			computedPrediction = -3.98;
		} else if (rec.equals("Cosine Normal IB Std")) {
			computedPrediction = 3.50;
		} else if (rec.equals("Cosine Total IB Std")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS IB Std")) {
			computedPrediction = 0; // TODO
		} else if (rec.equals("Pearson Total0 UB MC")) {
			computedPrediction = 2.94;
		} else if (rec.equals("Pearson Total3 UB MC")) {
			computedPrediction = 3.30;
		} else if (rec.equals("Pearson Normal UB MC")) {
			computedPrediction = 3.34;
		} else if (rec.equals("Cosine Normal UB MC")) {
			computedPrediction = 4.84;
		} else if (rec.equals("Cosine Total UB MC")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS UB MC")) {
			computedPrediction = 0; // TODO
		} else if (rec.equals("Pearson Total0 IB MC")) {
			computedPrediction = 3.01;
		} else if (rec.equals("Pearson Total3 IB MC")) {
			computedPrediction = 3.01;
		} else if (rec.equals("Pearson Normal IB MC")) {
			computedPrediction = 4.08;
		} else if (rec.equals("Cosine Normal IB MC")) {
			computedPrediction = 4.36;
		} else if (rec.equals("Cosine Total IB MC")) {
			computedPrediction = 0; // doesn't make sense
		} else if (rec.equals("LCS IB MC")) {
			computedPrediction = 0; // TODO
		}
		correctResult = Math.abs(predictedRating - computedPrediction) < 0.01;

		System.out.println(rec + ": "
				+ (correctResult ? "correct" : "incorrect") + " result for ("
				+ testUser + ", " + testItem + ")");
		if (!correctResult) {
			System.out.println(rec + ": " + predictedRating + " vs "
					+ computedPrediction);
		}
	}
}
