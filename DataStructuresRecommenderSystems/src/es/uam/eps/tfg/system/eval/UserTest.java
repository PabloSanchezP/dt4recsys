package es.uam.eps.tfg.system.eval;

import java.util.List;

public interface UserTest<I> {

	public void setRecommended(List<I> rec);

	public List<I> getRecommended();

	public List<I> getTest();
}
