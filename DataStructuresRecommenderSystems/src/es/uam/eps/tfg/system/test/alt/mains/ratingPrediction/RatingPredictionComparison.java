package es.uam.eps.tfg.system.test.alt.mains.ratingPrediction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;


/***
 * Class used to obtain a txt that compares the algorithms. The static final
 * variables can be customized to run the program.
 * 
 * @author Pablo
 * 
 */
public class RatingPredictionComparison {
	private static final String TRAINING_FILE = "./datasets/LastFm/lastFMtrain1.dat";
	private static final String TEST_FILE = "./datasets/LastFm/lastFMtest1.dat";
	private static final String RESULT_FILE = "./src/es/uam/eps/tfg/system/test/alt/Otro.txt";

	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity
	protected static final int LCSTHRESHOLD = 1;

	private static final int MIN_NEIGHBOURS = 10; // Minimun of MIN_NEIGHBOURS
													// to start the simulation
	private static final int MAX_NEIGHBOURS = 20; // Maximun of MIN_NEIGHBOURS
													// to end the simulation

	public static void main(String[] args) throws Exception {

		// Array of algorithms to be tested
		String[] algorithms = new String[] {
				//"baseline_random",
				//"baseline_user_bias",
				//"baseline_item_bias",
				//"baseline_system_bias",
				//"baseline_bias",
				//"Pearson_Union0_UB_Std",
				//"Pearson_Union3_UB_Std",
				//"Pearson_Union0_UB_MC",
				//"Pearson_Union3_UB_MC",
				//"Pearson_Normal_UB_Std",
				//"Pearson_Normal_UB_MC",
				//"PearsonPos_Union0_UB_Std",
				//"PearsonPos_Union3_UB_Std",
				//"PearsonPos_Union0_UB_MC",
				//"PearsonPos_Union3_UB_MC",
				//"PearsonPos_Normal_UB_Std",
				//"PearsonPos_Normal_UB_MC",
				//"Pearson_Union0_IB_Std",
				//"Pearson_Union0_IB_MC",
				//"Pearson_Union3_IB_Std",
				//"Pearson_Union3_IB_MC",
				//"Pearson_Normal_IB_Std",
				//"Pearson_Normal_IB_MC",
				//"PearsonPos_Union0_IB_Std",
				//"PearsonPos_Union0_IB_MC",
				//"PearsonPos_Union3_IB_Std",
				//"PearsonPos_Union3_IB_MC",
				//"PearsonPos_Normal_IB_Std",
				//"PearsonPos_Normal_IB_MC",
				//"Cosine_Normal_UB_Std",
				//"Cosine_Normal_UB_MC",
				//"Cosine_Union3_UB_MC",
				//"Cosine_Union3_UB_Std",
				//"CosinePos_Normal_UB_Std",
				//"CosinePos_Normal_UB_MC",
				//"CosinePos_Union3_UB_MC",
				//"CosinePos_Union3_UB_Std",
				//"Cosine_Union3_IB_Std",
				//"Cosine_Union3_IB_MC",
				//"Cosine_Normal_IB_Std",
				//"Cosine_Normal_IB_MC",
				//"CosinePos_Union3_IB_Std",
				//"CosinePos_Union3_IB_MC",
				//"CosinePos_Normal_IB_Std",
				//"CosinePos_Normal_IB_MC",
				//"LCS_-1_0_UB_Std",
				//"LCS_30_0_UB_Std",
				//"LCS_30_0_UB_MC",
				//"LCS_-1_0_UB_MC",
				//"LCS_-1_1_UB_Std",
				//"LCS_30_1_UB_Std",
				//"LCS_30_1_UB_MC",
				//"LCS_-1_1_UB_MC",
				//"LCS_-1_0_IB_Std",
				//"LCS_30_0_IB_Std",
				//"LCS_30_0_IB_MC",
				//"LCS_-1_0_IB_MC",
				//"LCS_-1_1_IB_Std",
				//"LCS_30_1_IB_Std",
				//"LCS_30_1_IB_MC",
				//"LCS_-1_1_IB_MC"
		};

		@SuppressWarnings("unchecked")
		RecommenderSystemIF<Long, Long>[] recommender = new RecommenderSystemIF[algorithms.length];

		for (int i = 0; i < algorithms.length; i++)
			recommender[i] = BuildDatasetRatingPrediction.buildDataset(algorithms[i]);

		for (int i = 0; i < algorithms.length; i++) {
			// load data
			BufferedReader in = new BufferedReader(
					new FileReader(TRAINING_FILE));
			String line = null;
			while ((line = in.readLine()) != null) {
				String[] toks = line.split("\t");
				recommender[i].addInteraction(
						Long.parseLong(toks[COLUMN_USER]),
						Long.parseLong(toks[COLUMN_ITEM]),
						Double.parseDouble(toks[COLUMN_RATING]));
			}
			in.close();
			recommender[i].train(false);
			System.out.println(recommender[i].info() + " Train Finished");
		}
		FileWriter result_file = new FileWriter(RESULT_FILE);

		result_file.write("MIN_NEIGHBOURS" + ";");
		for (int i = 0; i < algorithms.length; i++)
			result_file.write(recommender[i].info() + ";");
		result_file.write("\n");

		String data = "";

		for (int j = MIN_NEIGHBOURS; j < MAX_NEIGHBOURS; j += 5) {
			if (data != "") {
				data += "\n";
				result_file.write(data);
				data = j + ";";
			} else
				data = j + ";";
			for (int i = 0; i < algorithms.length; i++) {
				recommender[i].setNeighbours(j);
				BufferedReader in = new BufferedReader(
						new FileReader(TEST_FILE));
				String line = null;
				double mae = 0.0;
				double rmse = 0.0;
				int n = 0;
				while ((line = in.readLine()) != null) {
					String[] toks = line.split("\t");
					double pred = recommender[i].predictInteraction(
							Long.parseLong(toks[COLUMN_USER]),
							Long.parseLong(toks[COLUMN_ITEM]));
					double real = Double.parseDouble(toks[COLUMN_RATING]);
					if (Double.isNaN(pred))
						continue;
					mae += Math.abs(pred - real);
					rmse += (pred - real) * (pred - real);
					n++;
				}
				in.close();
				mae /= n;
				rmse = Math.sqrt(rmse / n);
				data += rmse + ";";
				/* Results */
				System.out.println(recommender[i].info()
						+ "Evaluation -- results: MAE=" + mae + ", RMSE="
						+ rmse);
			}
		}
		result_file.close();
	}

	

}
