package es.uam.eps.tfg.system.test.alt.ratingprediction.neighbourhood;

import java.util.Arrays;
import java.util.List;

import es.uam.eps.tfg.algorithms.LongestCommonSubsequence;
import es.uam.eps.tfg.algorithms.Similarity;
import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.trees.bheaps.MaxPriorityQueue;
import es.uam.eps.tfg.system.test.alt.General;

import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;

/***
 * Class that uses a common algorithm of recommender systems in train method.
 * Depending on the string used as similarity it will use different algorithms.
 * All of the algorithms are computed in the class Similarity
 * 
 * @author Pablo
 * 
 */
public class NeighBourhoodSimilarity extends General implements RecommenderSystemIF<Long, Long> {

	protected String similarity;
	protected int LCSthreshold;
	int LCSlimit = 16;

	public NeighBourhoodSimilarity(int k, String similarity, Structures s, int threshold, ReccomendationScheme scheme) {
		super(k, s, scheme);
		this.similarity = similarity;
		this.LCSthreshold = threshold;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	public NeighBourhoodSimilarity(int k, String similarity, Structures s, int threshold) {
		super(k, s);
		this.similarity = similarity;
		this.LCSthreshold = threshold;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	public NeighBourhoodSimilarity(int k, String similarity, Structures s, ReccomendationScheme scheme) {
		super(k, s, scheme);
		this.similarity = similarity;
		this.LCSthreshold = 1;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	public NeighBourhoodSimilarity(int k, String similarity, Structures s, ReccomendationScheme scheme, int limit,
			int threshold) {
		super(k, s, scheme);
		this.similarity = similarity;
		this.LCSthreshold = threshold;
		if (similarity == "LCS")
			this.limit = limit;
	}

	// Primes
	public NeighBourhoodSimilarity(int prime1, int prime2, int k, String similarity, Structures s, int threshold,
			ReccomendationScheme scheme) {
		super(k, s, scheme, prime1, prime2, 23);
		this.similarity = similarity;
		this.LCSthreshold = threshold;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	public NeighBourhoodSimilarity(int prime1, int prime2, int k, String similarity, Structures s, int threshold) {
		super(k, s, prime1, prime2);
		this.similarity = similarity;
		this.LCSthreshold = threshold;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	public NeighBourhoodSimilarity(int prime1, int prime2, int auxint, int k, String similarity, Structures s,
			ReccomendationScheme scheme) {
		super(k, s, scheme, prime1, prime2, auxint);
		this.similarity = similarity;
		this.LCSthreshold = 1;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	public NeighBourhoodSimilarity(int prime1, int prime2, int auxint, int k, String similarity, Structures s,
			ReccomendationScheme scheme, int limit, int threshold) {
		super(k, s, scheme, prime1, prime2, auxint);
		this.similarity = similarity;
		this.LCSthreshold = threshold;
		if (similarity == "LCS")
			this.limit = limit;
	}
	//Customising subStructures
	public NeighBourhoodSimilarity(int prime1, int prime2, int k, String similarity, Structures s,
			ReccomendationScheme scheme,Structures sub) {
		super(k, s, scheme, prime1, prime2,sub);
		this.similarity = similarity;
		this.LCSthreshold = 1;
		if (similarity == "LCS")
			limit = LCSlimit;
	}

	@Override
	public void train(boolean useOnlyTestUsers) {
		// computes and stores most similar users (neighbors) for each user
		double simmilarity = 0;
		int[] x2 = null;
		int[] y2 = null;
		GeneralStructure<SorteableLong> x = null;
		GeneralStructure<SorteableLong> y = null;

		for (Element u : elementBased.toList()) {
			// optimization: do training only for test users and UB
			if (useOnlyTestUsers && actualScheme.equals(ReccomendationScheme.UserBased) && u.getTest().size() == 0) {
				continue;
			}
			int size = elementBased.getTotalElements() - 1;
			MaxPriorityQueue<WeightedElement> q = new MaxPriorityQueue<WeightedElement>(size);
			// Case of LCS
			if (similarity.equals("LCS")) {
				// Array of integers, as integers are faster than strings
				x2 = new int[u.getElementsRated().getTotalElements()];
				addAll(x2, u.getElementsRated().toList());
				Arrays.sort(x2);
			} else {
				// Case standard
				x = u.getElementsRated();
			}
			for (Element v : elementBased.toList()) {
				// If they are the same users, continue
				if (u == v)
					continue;
				// Case of LCS
				if (similarity.equals("LCS")) {
					y2 = new int[v.getElementsRated().getTotalElements()];
					addAll(y2, v.getElementsRated().toList());
					Arrays.sort(y2);
					simmilarity = LongestCommonSubsequence.longestCommonSubsequence(x2, y2, LCSthreshold);
				} else {
					// Case standard
					y = v.getElementsRated();
					simmilarity = similarity(similarity, x, y);
				}
				double sim = 1.0 * simmilarity;
				q.insert(new WeightedElement(v, sim));
			}
			for (int i = 0; i < size; i++) {
				u.addNeighbour(q.extractMax());
			}
		}
	}

	private void addAll(int[] myFilms, List<SorteableLong> arrayList) {
		int cont = 0;
		for (SorteableLong l : arrayList) {
			/*
			 * I put the ID of the item AND the value of that item, to obtain
			 * the common subsequence of the items rated equally
			 */
			myFilms[cont] = (int) (l.getWrapped() * 100 + l.getValue());
			cont++;
		}

	}

	private double similarity(String algorithm, GeneralStructure<SorteableLong> u, GeneralStructure<SorteableLong> v) {
		if (algorithm.equals("Pearson Normal"))
			return Similarity.pearsonCorrelation(u, v);
		else if (algorithm.equals("Pearson Union0"))
			return Similarity.pearsonCorrelation(u, v, 0);
		else if (algorithm.equals("Pearson Union3"))
			return Similarity.pearsonCorrelation(u, v, 3.0);
		else if (algorithm.equals("Cosine Normal"))
			return Similarity.cosineVector(u, v);
		else if (algorithm.equals("Cosine Common"))
			return Similarity.cosineVectorCommon(u, v);
		else if (algorithm.equals("Cosine Union"))
			return Similarity.cosineVector(u, v, 0);
		if (algorithm.equals("Cosine Union3"))
			return Similarity.cosineVector(u, v, 3);
		return 0;
	}

	@Override
	public String info() {
		return info;
	}

}
