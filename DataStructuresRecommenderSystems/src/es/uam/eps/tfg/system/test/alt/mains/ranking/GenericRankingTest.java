package es.uam.eps.tfg.system.test.alt.mains.ranking;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;

/***
 * Generic test of rankings algorithms. Actually the following algorithms can be
 * used: -Mrr -NDCG -Precision -Recall To see the details of that
 * implementations, read the documentation of each algorithm class.
 * 
 * @author Pablo
 */
public class GenericRankingTest {
	protected static final int COLUMN_USER = 0;
	protected static final int COLUMN_ITEM = 1;
	protected static final int COLUMN_RATING = 2;

	// Variable that indicates the value of test items to consider in ranking
	protected static final double HEIGHT = 4;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity
	protected static final int LCSTHRESHOLD = 0;

	private static final String TRAINING_FILE = "./datasets/Movielens/u1.base";
	private static final String TEST_FILE = "./datasets/Movielens/u1.test";

	public static void main(String[] args) {
		Runtime runtime = Runtime.getRuntime();
		long time1 = System.currentTimeMillis();
		NeighbourhoodRanking dataset = new NeighbourhoodRanking(100, "Pearson Normal",
				Structures.HashTable_List, ReccomendationScheme.UserBased, 30, 0);
		// BaselineRanking dataset = new BaselineRanking(BIAS_TYPE.COMBINED);

		// dataset.testing();

		// load data
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(TRAINING_FILE));
			String line = null;
			while ((line = in.readLine()) != null) {
				String[] toks = line.split("\t");
				dataset.addInteraction(Long.parseLong(toks[COLUMN_USER]),
						Long.parseLong(toks[COLUMN_ITEM]),
						Double.parseDouble(toks[COLUMN_RATING]));
			}
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total items: " + dataset.getTotalItems());
		System.out.println("Total users: " + dataset.getTotalUsers());
		System.out.println(dataset.info());
		dataset.train(false);

		try {
			in = new BufferedReader(new FileReader(TEST_FILE));
			String line = null;
			while ((line = in.readLine()) != null) {
				String[] toks = line.split("\t");
				dataset.addTestInteraction(Long.parseLong(toks[COLUMN_USER]),
						Long.parseLong(toks[COLUMN_ITEM]),
						(double) Long.parseLong(toks[COLUMN_RATING]), HEIGHT);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// dataset.testing();
		dataset.test(0.0);
		dataset.evaluate();
		long time2 = System.currentTimeMillis();

		System.out.println("time spent: " + (time2 - time1) + " miliseconds");
		/* Memory used by my application */
		System.out.println("allocated memory: " + runtime.totalMemory()
				/ 1024.0 / 1024 + " MB");
		System.out.println("-------------------------");

	}

}
