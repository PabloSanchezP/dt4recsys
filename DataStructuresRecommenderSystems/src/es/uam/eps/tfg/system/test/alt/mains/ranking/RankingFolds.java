package es.uam.eps.tfg.system.test.alt.mains.ranking;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;


/**
 * This class will obtain results from the algorithms using ranking metrics.
 * The result will be the average between all the test files
 * 
 * @author Pablo
 *
 */
public class RankingFolds {
	private static final int MIN_NEIGHBOURS = 10; // Minimun of MIN_NEIGHBOURS
	// to start the simulation
	private static final int MAX_NEIGHBOURS = 110; // Maximun of MIN_NEIGHBOURS

	private static final String RESULT_FILE_PRECISION = "./src/es/uam/eps/tfg/system/test/alt/mains/ranking/Prediction.txt";
	private static final String RESULT_FILE_RECALL = "./src/es/uam/eps/tfg/system/test/alt/mains/ranking/Recall.txt";
	private static final String RESULT_FILE_MRR = "./src/es/uam/eps/tfg/system/test/alt/mains/ranking/MRR.txt";
	private static final String RESULT_FILE_NDCG = "./src/es/uam/eps/tfg/system/test/alt/mains/ranking/Ndcg.txt";

	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	private static final double HEIGHT = 4;

	// to end the simulation

	public static void main(String[] args) throws Exception {
		String[] trainfiles = new String[] { "./datasets/Movielens/u1.base",
				"./datasets/Movielens/u2.base", "./datasets/Movielens/u3.base",
				"./datasets/Movielens/u4.base", "./datasets/Movielens/u5.base",

		};

		String[] testfiles = new String[] { "./datasets/Movielens/u1.test",
				"./datasets/Movielens/u2.test", "./datasets/Movielens/u3.test",
				"./datasets/Movielens/u4.test", "./datasets/Movielens/u5.test", };

		// Array of algorithms to be tested
		String[] algorithms = new String[] {
				//"baseline_random",
				//"baseline_user_bias",
				//"baseline_item_bias",
				//"baseline_system_bias",
				//"baseline_bias",
				//"Pearson_Union0_UB_Std",
				//"Pearson_Union3_UB_Std",
				//"Pearson_Union0_UB_MC",
				//"Pearson_Union3_UB_MC",
				//"Pearson_Normal_UB_Std",
				//"Pearson_Normal_UB_MC",
				//"PearsonPos_Union0_UB_Std",
				//"PearsonPos_Union3_UB_Std",
				//"PearsonPos_Union0_UB_MC",
				//"PearsonPos_Union3_UB_MC",
				//"PearsonPos_Normal_UB_Std",
				//"PearsonPos_Normal_UB_MC",
				//"Pearson_Union0_IB_Std",
				//"Pearson_Union0_IB_MC",
				//"Pearson_Union3_IB_Std",
				//"Pearson_Union3_IB_MC",
				//"Pearson_Normal_IB_Std",
				//"Pearson_Normal_IB_MC",
				//"PearsonPos_Union0_IB_Std",
				//"PearsonPos_Union0_IB_MC",
				//"PearsonPos_Union3_IB_Std",
				//"PearsonPos_Union3_IB_MC",
				//"PearsonPos_Normal_IB_Std",
				//"PearsonPos_Normal_IB_MC",
				//"Cosine_Normal_UB_Std",
				//"Cosine_Normal_UB_MC",
				//"Cosine_Union3_UB_MC",
				//"Cosine_Union3_UB_Std",
				//"CosinePos_Normal_UB_Std",
				//"CosinePos_Normal_UB_MC",
				//"CosinePos_Union3_UB_MC",
				//"CosinePos_Union3_UB_Std",
				//"Cosine_Union3_IB_Std",
				//"Cosine_Union3_IB_MC",
				//"Cosine_Normal_IB_Std",
				//"Cosine_Normal_IB_MC",
				//"CosinePos_Union3_IB_Std",
				//"CosinePos_Union3_IB_MC",
				//"CosinePos_Normal_IB_Std",
				//"CosinePos_Normal_IB_MC",
				//"LCS_-1_0_UB_Std",
				//"LCS_30_0_UB_Std",
				//"LCS_30_0_UB_MC",
				//"LCS_-1_0_UB_MC",
				//"LCS_-1_1_UB_Std",
				//"LCS_30_1_UB_Std",
				//"LCS_30_1_UB_MC",
				//"LCS_-1_1_UB_MC",
				//"LCS_-1_0_IB_Std",
				//"LCS_30_0_IB_Std",
				//"LCS_30_0_IB_MC",
				//"LCS_-1_0_IB_MC",
				//"LCS_-1_1_IB_Std",
				//"LCS_30_1_IB_Std",
				//"LCS_30_1_IB_MC",
				//"LCS_-1_1_IB_MC"
		};

		@SuppressWarnings("unchecked")
		RecommenderSystemIF<Long, Long>[] recommender = new RecommenderSystemIF[algorithms.length];

		TreeMap<String, List<TreeMap<Integer, ResultsRanking>>> results = new TreeMap<String, List<TreeMap<Integer, ResultsRanking>>>();
		for (int i = 0; i < algorithms.length; i++) {
			for (int j = 0; j < trainfiles.length; j++) {
				recommender[i] = BuildDatasetRanking.buildDataset(algorithms[i],Structures.HashTable_RedBlack);
				// load data
				BufferedReader in = new BufferedReader(new FileReader(
						trainfiles[j]));
				String line = null;
				while ((line = in.readLine()) != null) {
					String[] toks = line.split("\t");
					recommender[i].addInteraction(
							Long.parseLong(toks[COLUMN_USER]),
							Long.parseLong(toks[COLUMN_ITEM]),
							Double.parseDouble(toks[COLUMN_RATING]));
				}
				in.close();
				recommender[i].train(false);
				System.out.println(recommender[i].info()
						+ " Train Finished: file " + (j + 1) + " of "
						+ trainfiles.length + trainfiles[j]);

				in = new BufferedReader(new FileReader(testfiles[j]));
				line = null;
				while ((line = in.readLine()) != null) {
					String[] toks = line.split("\t");
					recommender[i].addTestInteraction(
							Long.parseLong(toks[COLUMN_USER]),
							Long.parseLong(toks[COLUMN_ITEM]),
							(double) Long.parseLong(toks[COLUMN_RATING]),
							HEIGHT);
				}
				in.close();

				for (int k = MIN_NEIGHBOURS; k < MAX_NEIGHBOURS; k += 10) {
					recommender[i].setNeighbours(k);
					recommender[i].test(0.0);

					String result = ((NeighbourhoodRanking) recommender[i])
							.evaluateV2();
					String toks[] = result.split(";");

					List<TreeMap<Integer, ResultsRanking>> f = results
							.get(recommender[i].info());

					if (f == null) { // Not found
						List<TreeMap<Integer, ResultsRanking>> added = new ArrayList<TreeMap<Integer, ResultsRanking>>();
						TreeMap<Integer, ResultsRanking> neighRanking = new TreeMap<Integer, ResultsRanking>();
						neighRanking.put(
								k,
								new ResultsRanking(Double.parseDouble(toks[0]),
										Double.parseDouble(toks[1]), Double
												.parseDouble(toks[2]), Double
												.parseDouble(toks[3])));
						added.add(neighRanking);
						results.put(recommender[i].info(), added);
					} else {// Found
						boolean found = false;
						for (TreeMap<Integer, ResultsRanking> mn : f) {
							if (mn.get(k) != null) {
								found = true;
								mn.get(k).Update(Double.parseDouble(toks[0]),
										Double.parseDouble(toks[1]),
										Double.parseDouble(toks[2]),
										Double.parseDouble(toks[3]));
							}
						}
						if (found == false) {
							TreeMap<Integer, ResultsRanking> newRanking = new TreeMap<Integer, ResultsRanking>();
							newRanking.put(
									k,
									new ResultsRanking(Double
											.parseDouble(toks[0]), Double
											.parseDouble(toks[1]), Double
											.parseDouble(toks[2]), Double
											.parseDouble(toks[3])));
							f.add(newRanking);
						}
					}
				}
				System.out.println(recommender[i].info()
						+ " Test Finished: file " + (j + 1) + " of "
						+ trainfiles.length + testfiles[j]);
			}

		}

		// PrintResult
		FileWriter result_file1 = new FileWriter(RESULT_FILE_PRECISION);
		FileWriter result_file2 = new FileWriter(RESULT_FILE_RECALL);
		FileWriter result_file3 = new FileWriter(RESULT_FILE_MRR);
		FileWriter result_file4 = new FileWriter(RESULT_FILE_NDCG);

		result_file1.write("NEIGHBOURS" + ";");
		result_file2.write("NEIGHBOURS" + ";");
		result_file3.write("NEIGHBOURS" + ";");
		result_file4.write("NEIGHBOURS" + ";");
		for (int i = 0; i < algorithms.length; i++) {
			result_file1.write(recommender[i].info() + ";");
			result_file2.write(recommender[i].info() + ";");
			result_file3.write(recommender[i].info() + ";");
			result_file4.write(recommender[i].info() + ";");
		}
		result_file1.write("\n");
		result_file2.write("\n");
		result_file3.write("\n");
		result_file4.write("\n");
		// Now print
		for (int k = MIN_NEIGHBOURS; k < MAX_NEIGHBOURS; k += 5) {
			result_file1.write(k + ";");
			result_file2.write(k + ";");
			result_file3.write(k + ";");
			result_file4.write(k + ";");
			for (int i = 0; i < algorithms.length; i++) {
				for (TreeMap<Integer, ResultsRanking> nm : results
						.get(recommender[i].info())) {
					if (nm.get(k) != null) {
						result_file1
								.write((nm.get(k).precision / (double) trainfiles.length)
										+ ";");
						result_file2
								.write((nm.get(k).recall / (double) trainfiles.length)
										+ ";");
						result_file3
								.write((nm.get(k).mrr / (double) trainfiles.length)
										+ ";");
						result_file4
								.write((nm.get(k).ndcg / (double) trainfiles.length)
										+ ";");
					}
				}

			}
			result_file1.write("\n");
			result_file2.write("\n");
			result_file3.write("\n");
			result_file4.write("\n");
		}

		result_file1.close();
		result_file2.close();
		result_file3.close();
		result_file4.close();

	}

	
}

class ResultsRanking {
	double ndcg;
	double precision;
	double recall;
	double mrr;

	public ResultsRanking() {

		ndcg = 0;
		precision = 0;
		recall = 0;
		mrr = 0;

	}

	public ResultsRanking(double precision, double recall, double mrr,
			double ndcg) {

		this.ndcg = ndcg;
		this.precision = precision;
		this.recall = recall;
		this.mrr = mrr;

	}

	public void Update(double precision, double recall, double mrr, double ndcg) {
		this.ndcg += ndcg;
		this.precision += precision;
		this.recall += recall;
		this.mrr += mrr;

	}

	public double getNdcg() {
		return ndcg;
	}

	public void setNdcg(double ndcg) {
		this.ndcg = ndcg;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getRecall() {
		return recall;
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public double getMrr() {
		return mrr;
	}

	public void setMrr(double mrr) {
		this.mrr = mrr;
	}

}
