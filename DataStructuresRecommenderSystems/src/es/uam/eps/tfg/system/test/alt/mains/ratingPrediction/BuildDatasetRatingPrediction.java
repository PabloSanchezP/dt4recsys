package es.uam.eps.tfg.system.test.alt.mains.ratingPrediction;

import es.uam.eps.tfg.system.test.alt.General;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetRandomRS;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases.BIAS_TYPE;
import es.uam.eps.tfg.system.test.alt.ratingprediction.neighbourhood.NeighBourhoodSimilarity;

/**
 * 
 * Class that from a string, returns a Neighborhood similarity class that operates
 * with that algorithm
 * 
 * @author Pablo
 *
 */
public class BuildDatasetRatingPrediction {
	
	static int MIN_NEIGHBOURS=10;
	
	
	public static RecommenderSystemIF<Long, Long> buildDataset(String rec) {
		RecommenderSystemIF<Long, Long> dataset = null;
		
		
		if (rec.equals("baseline_random")) {
			dataset = new BaselineDatasetRandomRS();
		} else if (rec.equals("baseline_user_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.USER);
		} else if (rec.equals("baseline_item_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.ITEM);
		} else if (rec.equals("baseline_system_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.SYSTEM);
		} else if (rec.equals("baseline_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.COMBINED);
		} else if (rec.equals("Pearson_Union0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("PearsonPos_Union0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// Pearson IB
		} else if (rec.equals("Pearson_Union0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("PearsonPos_Union0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// Cosine UB
		} else if (rec.equals("Cosine_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("CosinePos_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
			// Cosine IB
		} else if (rec.equals("Cosine_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("CosinePos_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					Structures.HashTable_RedBlack, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// LCS UB
		} else if (rec.equals("LCS_-1_0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, -1, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 30, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 16, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 30, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 16, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, -1, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, -1, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 30, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_1_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 16, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 30, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_1_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, 16, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased, -1, 1);
			((General) dataset).setMeanCentering(true);
			// LCS UB
		} else if (rec.equals("LCS_-1_0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, -1, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 30, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 16, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 30, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 16, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, -1, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, -1, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 30, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_1_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 16, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 30, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_1_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, 16, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", Structures.HashTable_RedBlack,
					ReccomendationScheme.ItemBased, -1, 1);
			((General) dataset).setMeanCentering(true);
		}
		dataset.setinfo(rec);
		return dataset;
	}
	
	public static RecommenderSystemIF<Long, Long> buildDataset(String rec, Structures struct) {
		RecommenderSystemIF<Long, Long> dataset = null;
		
		
		if (rec.equals("baseline_random")) {
			dataset = new BaselineDatasetRandomRS();
		} else if (rec.equals("baseline_user_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.USER);
		} else if (rec.equals("baseline_item_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.ITEM);
		} else if (rec.equals("baseline_system_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.SYSTEM);
		} else if (rec.equals("baseline_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.COMBINED);
		} else if (rec.equals("Pearson_Union0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("PearsonPos_Union0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// Pearson IB
		} else if (rec.equals("Pearson_Union0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("PearsonPos_Union0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union0", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Union3", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS,
					"Pearson Normal", struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// Cosine UB
		} else if (rec.equals("Cosine_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("CosinePos_Normal_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
			// Cosine IB
		} else if (rec.equals("Cosine_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("CosinePos_Union3_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Union3",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "Cosine Normal",
					struct, ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// LCS UB
		} else if (rec.equals("LCS_-1_0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_0_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_0_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_1_UB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_1_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_UB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 1);
			((General) dataset).setMeanCentering(true);
			// LCS UB
		} else if (rec.equals("LCS_-1_0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_0_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_0_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_1_IB_Std")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_1_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_IB_MC")) {
			dataset = new NeighBourhoodSimilarity(MIN_NEIGHBOURS, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 1);
			((General) dataset).setMeanCentering(true);
		}
		dataset.setinfo(rec);
		return dataset;
	}
	

	
}
