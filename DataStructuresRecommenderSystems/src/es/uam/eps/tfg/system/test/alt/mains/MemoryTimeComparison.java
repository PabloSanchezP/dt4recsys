package es.uam.eps.tfg.system.test.alt.mains;

import es.uam.eps.tfg.structures.ParseStructureFactory;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemFactory;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.mains.ratingPrediction.BuildDatasetRatingPrediction;

/***
 * Class to test memory and time of the algorithms.
 * 
 * 
 * @author Pablo
 *
 */
public class MemoryTimeComparison {

	private static final long MEGABYTE = 1024L * 1024L;
	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity
	protected static final int LCSTHRESHOLD = 1;



	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public static void main(String[] args) throws Exception {
		String[] trainfiles = new String[] { "./datasets/Movielens/u1.base",
				"./datasets/Movielens/u2.base", "./datasets/Movielens/u3.base",
				"./datasets/Movielens/u4.base", "./datasets/Movielens/u5.base",

		};

		String[] testfiles = new String[] { "./datasets/Movielens/u1.test",
				"./datasets/Movielens/u2.test", "./datasets/Movielens/u3.test",
				"./datasets/Movielens/u4.test", "./datasets/Movielens/u5.test", };
		
		String [] structures = {"redblacktree","redblacktree","orderstatistictree","binarysearchtree","doublelinkedlist",
				"btree","hashtableredblacktree","hashtablelist"};

		
		// Array of algorithms to be tested
		String[] algorithms = new String[] { 
				//"baseline_random",
				//"baseline_user_bias",
				//"baseline_item_bias",
				//"baseline_system_bias",
				//"baseline_bias",
				//"Pearson_Union0_UB_Std",
				//"Pearson_Union3_UB_Std",
				//"Pearson_Union0_UB_MC",
				//"Pearson_Union3_UB_MC",
				"Pearson_Normal_UB_Std",
				//"Pearson_Normal_UB_MC",
				//"PearsonPos_Union0_UB_Std",
				//"PearsonPos_Union3_UB_Std",
				//"PearsonPos_Union0_UB_MC",
				//"PearsonPos_Union3_UB_MC",
				//"PearsonPos_Normal_UB_Std",
				//"PearsonPos_Normal_UB_MC",
				//"Pearson_Union0_IB_Std",
				//"Pearson_Union0_IB_MC",
				//"Pearson_Union3_IB_Std",
				//"Pearson_Union3_IB_MC",
				//"Pearson_Normal_IB_Std",
				//"Pearson_Normal_IB_MC",
				//"PearsonPos_Union0_IB_Std",
				//"PearsonPos_Union0_IB_MC",
				//"PearsonPos_Union3_IB_Std",
				//"PearsonPos_Union3_IB_MC",
				//"PearsonPos_Normal_IB_Std",
				//"PearsonPos_Normal_IB_MC",
				//"Cosine_Normal_UB_Std",
				//"Cosine_Normal_UB_MC",
				//"Cosine_Union3_UB_MC",
				//"Cosine_Union3_UB_Std",
				//"CosinePos_Normal_UB_Std",
				//"CosinePos_Normal_UB_MC",
				//"CosinePos_Union3_UB_MC",
				//"CosinePos_Union3_UB_Std",
				//"Cosine_Union3_IB_Std",
				//"Cosine_Union3_IB_MC",
				//"Cosine_Normal_IB_Std",
				//"Cosine_Normal_IB_MC",
				//"CosinePos_Union3_IB_Std",
				//"CosinePos_Union3_IB_MC",
				//"CosinePos_Normal_IB_Std",
				//"CosinePos_Normal_IB_MC",
				//"LCS_-1_0_UB_Std",
				//"LCS_30_0_UB_Std",
				//"LCS_30_0_UB_MC",
				//"LCS_-1_0_UB_MC",
				//"LCS_-1_1_UB_Std",
				//"LCS_30_1_UB_Std",
				//"LCS_30_1_UB_MC",
				//"LCS_-1_1_UB_MC",
				//"LCS_-1_0_IB_Std",
				//"LCS_30_0_IB_Std",
				//"LCS_30_0_IB_MC",
				//"LCS_-1_0_IB_MC",
				//"LCS_-1_1_IB_Std",
				//"LCS_30_1_IB_Std",
				//"LCS_30_1_IB_MC",
				//"LCS_-1_1_IB_MC"
		};
		//System.gc();
		System.out.println("Structure;trainReadingTotalTime;trainReadingTotalMemory;trainTotalTime;trainTotalMemory;testReadingTotalTime;" +
				"testReadingTotalMemory;testTotalTime;testTotalMemory");
		
		Runtime runtime = Runtime.getRuntime();
		
		for (String structure: structures){
			// load data
			long trainReadingTotalTime = 0; long trainReadingTotalMemory=0;
			long trainTotalTime = 0; long trainTotalMemory=0;
			long testReadingTotalTime = 0; long testReadingTotalMemory=0;
			long testTotalTime = 0; long testTotalMemory=0;
			RecommenderSystemIF<Long, Long> dataset = BuildDatasetRatingPrediction.buildDataset(algorithms[0],ParseStructureFactory.parseStructure(structure));
			int i=0;
			for (String fileTrain: trainfiles){
				//System.gc();

				//Reading training file
			    long usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
				long timeBefore = System.currentTimeMillis();

				
				//READ TRAIN
				RecommenderSystemFactory.loadTrainingDataset(dataset,fileTrain,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);
				
			    long usedMemoryAfter  = runtime.totalMemory() - runtime.freeMemory();
				long timeAfter = System.currentTimeMillis();
				trainReadingTotalMemory+=usedMemoryAfter-usedMemoryBefore;
				trainReadingTotalTime+=timeAfter-timeBefore;
				//Finishing reading train. Now we train
				//System.gc();

				//Train
				usedMemoryBefore= runtime.totalMemory() - runtime.freeMemory();
				timeBefore=System.currentTimeMillis();
				dataset.train(false);
				usedMemoryAfter= runtime.totalMemory() - runtime.freeMemory();
				timeAfter=System.currentTimeMillis();
				
				//Obtain memory and time of train
				trainTotalTime+= timeAfter-timeBefore;
				trainTotalMemory+=usedMemoryAfter-usedMemoryBefore;
				
				//System.gc();

				//Read test
				usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
				timeBefore = System.currentTimeMillis();

				RecommenderSystemFactory.loadTestDataset(dataset,testfiles[i],0.0,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);
			    usedMemoryAfter  = runtime.totalMemory() - runtime.freeMemory();
				timeAfter = System.currentTimeMillis();
				
				testReadingTotalMemory+=usedMemoryAfter-usedMemoryBefore;
				testReadingTotalTime+=timeAfter-timeBefore;
				//System.gc();

				int [] threshold = new int[] {1000};
				usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
				timeBefore = System.currentTimeMillis();
				
				
				//Test time
				dataset.test(0.0);
				dataset.computeEvaluationBean(4.0, threshold);
				usedMemoryAfter  = runtime.totalMemory() - runtime.freeMemory();
				timeAfter = System.currentTimeMillis();
				
				testTotalMemory+=usedMemoryAfter-usedMemoryBefore;
				testTotalTime+=timeAfter-timeBefore;
				
				i++;
			}

			double d=trainfiles.length;
		
					//Average of the time/memory of the files
			System.out.println(structure+";"
					+String.format("%.2f",((double)trainReadingTotalTime/d))+";"+String.format("%.2f",((double)trainReadingTotalMemory/d))
					+";"+String.format("%.2f",((double)trainTotalTime/d))+";"+String.format("%.2f",((double)trainTotalMemory/d))
					+";"+String.format("%.2f",((double)testReadingTotalTime/d))+";"+String.format("%.2f",((double)testReadingTotalMemory/d))
					+";"+String.format("%.2f",((double)testTotalTime/d))+";"+String.format("%.2f",((double)testTotalMemory/d)));
			
		}
		
		

	}
	


	

	

}
