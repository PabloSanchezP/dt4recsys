package es.uam.eps.tfg.system.test.alt.ratingprediction.baselines;

import java.util.HashMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.eval.UserTest;
import es.uam.eps.tfg.system.eval.EvaluatorUtils;
import es.uam.eps.tfg.system.eval.EvaluatorUtils.EvaluationBean;

/**
 * Class that implements a baseline algorithm explained in the recommender
 * system handbook It computes the mean of all items,and for each items, it
 * computes the sum of the subtractions of the rating and the total mean
 * (dividing that sum by the total of users that have rated that item) For every
 * uses, it also computes it biases by the sum of the rating, subtracting the
 * mean and the bias of the item
 * 
 * 
 * @author Pablo
 * 
 */
public class BaselineDatasetMeanBiases implements
		RecommenderSystemIF<Long, Long> {

	public enum BIAS_TYPE {
		USER, ITEM, SYSTEM, COMBINED;
	}

	protected Map<Long, UserBias> users;
	protected Map<Long, ItemBias> items;
	protected double mean;
	protected int totalIteractions;
	protected BIAS_TYPE biasType;
	protected String info;

	public BaselineDatasetMeanBiases(BIAS_TYPE type) {
		biasType = type;
		users = new HashMap<Long, UserBias>();
		items = new HashMap<Long, ItemBias>();
		mean = 0;
		totalIteractions = 0;
	}

	@Override
	public int getTotalItems() {
		return items.size();
	}

	@Override
	public int getTotalUsers() {
		return users.size();
	}

	@Override
	public void addInteraction(Long user, Long item, Double interaction) {
		UserBias u = users.get(user);

		if (items.get(item) == null)
			items.put(item, new ItemBias(item));

		ItemBias ac = items.get(item);

		// New user
		if (u == null) {
			u = new UserBias(user);
			u.addInteraction(ac, interaction);
			users.put(user, u);
		} else
			// User found
			u.addInteraction(ac, interaction);

		mean += interaction;
		totalIteractions++;
	}

	@Override
	public void addTestInteraction(Long user, Long item, Double interaction,
			Double threshold) {
		if (interaction < threshold)
			return;
		UserBias u = users.get(user);
		if (u == null)
			return;
		u.addTestIteraction(item, interaction);
	}

	@Override
	public void train(boolean useTestUsers) {
		// Mean stores the total mean of the films
		mean /= totalIteractions;
		double cont = 0;
		double acc = 0;

		// Item biases
		for (long iditem : items.keySet()) {
			ItemBias item = items.get(iditem);
			cont = 0;
			acc = 0;
			for (long iduser : users.keySet()) {
				UserBias user = users.get(iduser);
				Double v = user.userItemMap.get(iditem);
				if (v != null) {
					cont++;
					acc += v - mean;
				}
			}
			item.biase = acc / cont;
		}

		// User biase
		for (long iduser : users.keySet()) {
			cont = 0;
			acc = 0;
			UserBias user = users.get(iduser);
			for (Entry<Long, Double> entry : user.userItemMap.entrySet()) {
				cont++;
				// acc += entry.getValue() - items.get(entry.getKey()).biase -
				// mean; <-- por c�mo se devuelve despu�s, esta f�rmula no
				// ser�a
				// correcta
				acc += entry.getValue() - mean;
			}
			user.biase = acc / cont;
		}

	}

	@Override
	public void test(double defaultValue) {
		for (long user : users.keySet()) {
			UserBias u = users.get(user);
			List<SorteableLong> result = new ArrayList<SorteableLong>();
			for (long i : items.keySet()) {
				// if the item has been rated, then we do not consider it
				if (!u.getRatedItems().contains(i)) {
					double p = predictInteraction(user, i);
					if (!Double.isNaN(p)) {
						result.add(new SorteableLong(i, p));
					} else if (!Double.isNaN(defaultValue)) {
						result.add(new SorteableLong(i, defaultValue));
					}
				}
			}
			Collections.sort(result, Collections.reverseOrder());
			u.setRecommended(result);
		}
	}

	@Override
	public EvaluationBean computeEvaluationBean(double relevanceThreshold,
			int[] cutoffs) {
		return EvaluatorUtils.evaluate(relevanceThreshold, cutoffs,
				new ArrayList<UserBias>(users.values()));
	}

	@Override
	public String info() {
		return info;
	}

	@Override
	public void setNeighbours(int k) {
		return;
	}

	@Override
	public void setMinimumSimilarity(double w) {
		return;
	}

	@Override
	public void setinfo(String info) {
		this.info = info;
	}

	@Override
	public double predictInteraction(Long user, Long item) {
		double ibiase = 0;
		double ubiase = 0;
		// Search ibiase
		ItemBias i = items.get(item);
		if (i != null)
			ibiase = i.biase;

		// Search ubiase
		UserBias u = users.get(user);
		if (u != null)
			ubiase = u.biase;

		switch (biasType) {
		case COMBINED:
			return mean + ibiase + ubiase;
		case SYSTEM:
			return mean;
		case USER:
			return mean + ubiase;
		case ITEM:
			return mean + ibiase;
		}
		return Double.NaN;
	}

	@Override
	public List<Long> rankItems(Long user) {
		return null;
	}

	/**
	 * Private user class for the baseline
	 * 
	 * @author Pablo
	 * 
	 */
	public static class UserBias implements UserTest<SorteableLong> {
		private Long id;
		public TreeMap<Long, Double> userItemMap;
		private List<SorteableLong> testItems;
		private List<SorteableLong> reccomendedItems;
		private double biase;

		public UserBias(Long id) {
			userItemMap = new TreeMap<Long, Double>();
			testItems = new ArrayList<SorteableLong>();
			reccomendedItems = new ArrayList<SorteableLong>();
			this.id = id;
			biase = 0;
		}

		public void addRecommendedItem(SorteableLong l) {
			reccomendedItems.add(l);
		}

		public void addTestIteraction(Long itemId, Double value) {
			testItems.add(new SorteableLong(itemId, value));
		}

		@Override
		public List<SorteableLong> getTest() {
			return testItems;
		}

		public void addInteraction(ItemBias ac, Double value) {
			userItemMap.put(ac.id, value);
		}

		public Set<Long> getRatedItems() {
			return userItemMap.keySet();
		}

		@Override
		public boolean equals(Object obj) {

			boolean result = false;
			if (obj instanceof ItemBias) {
				ItemBias that = (ItemBias) obj;
				result = (this.id.longValue() == that.id.longValue());
			}
			return result;
		}

		@Override
		public String toString() {
			return "U" + id;
		}

		public void setRecommended(List<SorteableLong> rec) {
			reccomendedItems = rec;
		}

		@Override
		public List<SorteableLong> getRecommended() {
			return reccomendedItems;
		}
	}

	/***
	 * Private static Item class of the Baseline
	 * 
	 * @author Pablo
	 * 
	 */
	public static class ItemBias implements Comparable<ItemBias> {
		private Long id;
		private double biase;

		public ItemBias(Long id) {
			this.id = id;
			biase = 0;
		}

		@Override
		public boolean equals(Object obj) {
			boolean result = false;
			if (obj instanceof UserBias) {
				UserBias that = (UserBias) obj;
				result = (this.id.longValue() == that.id.longValue());
			}
			return result;
		}

		@Override
		public String toString() {
			return "I" + id;
		}

		@Override
		public int compareTo(ItemBias arg0) {
			if (arg0.id < this.id)
				return 1;
			else
				return -1;
		}
	}
}
