package es.uam.eps.tfg.system.test.alt.mains.ratingPrediction;

import java.io.BufferedReader;
import java.io.FileReader;

import es.uam.eps.tfg.system.test.alt.General;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases.BIAS_TYPE;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetRandomRS;
import es.uam.eps.tfg.system.test.alt.ratingprediction.neighbourhood.NeighBourhoodSimilarity;

/**
 * Class to test the rating predictions algorithms. All the static final
 * variables must be configured before launch.
 * 
 * @author Pablo
 * 
 */
public class GenericRatingPredictionTest {

	private static final String TRAINING_FILE = "./datasets/Movielens/u1.base";
	private static final String TEST_FILE = "./datasets/Movielens/u1.test";
	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity
	protected static final int LCSTHRESHOLD = 0;

	private static final int NEIGHBORS = 400; // Variable to use the first k
												// neighbors

	public static void main(String[] args) throws Exception {

		// Algorithms supported. The last one is the one that will be used
		args = new String[] { "Baseline Random" };

		args = new String[] { "Cosine Union" };

		args = new String[] { "Cosine Normal" };

		args = new String[] { "Pearson Union" };
		args = new String[] { "LCS" };
		args = new String[] { "Baseline Biase" };

		args = new String[] { "Pearson Normal" };

		RecommenderSystemIF<Long, Long> dataset = null;

		if (args[0].equals("Baseline Random")) {
			dataset = new BaselineDatasetRandomRS();

		} else if (args[0].equals("Baseline Biase")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.COMBINED);

		} else if (args[0].equals("Pearson Union")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Union0",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);

		} else if (args[0].equals("Pearson Normal")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Pearson Normal",
					Structures.BTree, ReccomendationScheme.UserBased);

		} else if (args[0].equals("Cosine Normal")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Normal",
					Structures.BTree, ReccomendationScheme.UserBased);

		} else if (args[0].equals("Cosine Union")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "Cosine Union",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased);
		} else if (args[0].equals("LCS")) {
			dataset = new NeighBourhoodSimilarity(NEIGHBORS, "LCS",
					Structures.HashTable_RedBlack, ReccomendationScheme.UserBased, -1, 0);
		}

		((General) dataset).setMeanCentering(false);
		/*
		 * dataset = new NeighBourhoodSimilarity
		 * (30,"LCS",Structures.HashTable,ReccomendationScheme.UserBased,32,1);
		 */
		// dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.COMBINED);

		System.out.println(dataset.info());
		Runtime runtime = Runtime.getRuntime();
		long time1 = System.currentTimeMillis();

		// load data
		BufferedReader in = new BufferedReader(new FileReader(TRAINING_FILE));
		String line = null;
		while ((line = in.readLine()) != null) {
			String[] toks = line.split("\t");

			dataset.addInteraction(Long.parseLong(toks[COLUMN_USER]),
					Long.parseLong(toks[COLUMN_ITEM]),
					Double.parseDouble(toks[COLUMN_RATING]));
		}
		in.close();

		long time2 = System.currentTimeMillis();
		System.out.println("Total items: " + dataset.getTotalItems());
		System.out.println("Total users: " + dataset.getTotalUsers());
		/* Time spent */
		System.out.println("Data loading -- Time spent: " + (time2 - time1)
				+ " miliseconds");
		/* Memory used by my application */
		System.out.println("Data loading -- Allocated memory: "
				+ runtime.totalMemory() / 1024.0 / 1024 + " MB");
		// train the recommender
		time1 = System.currentTimeMillis();
		dataset.train(false);
		time2 = System.currentTimeMillis();
		/* Time spent */
		System.out.println("Training -- Time spent: " + (time2 - time1)
				+ " miliseconds");
		/* Memory used by my application */
		System.out.println("Training -- Allocated memory: "
				+ runtime.totalMemory() / 1024.0 / 1024 + " MB");
		// evaluate the recommender
		time1 = System.currentTimeMillis();
		in = new BufferedReader(new FileReader(TEST_FILE));
		line = null;
		double mae = 0.0;
		double rmse = 0.0;
		int n = 0;

		while ((line = in.readLine()) != null) {
			String[] toks = line.split("\t");
			double pred = dataset.predictInteraction(
					Long.parseLong(toks[COLUMN_USER]),
					Long.parseLong(toks[COLUMN_ITEM]));
			double real = Double.parseDouble(toks[COLUMN_RATING]);
			// Ignore NaN numbers
			if (Double.isNaN(pred))
				continue;

			if (pred < 1)
				pred = 1;
			if (pred > 5)
				pred = 5;
			mae += Math.abs(pred - real);
			rmse += (pred - real) * (pred - real);
			n++;
		}

		in.close();
		mae /= n;
		rmse = Math.sqrt(rmse / n);
		time2 = System.currentTimeMillis();
		/* Results */
		System.out.println("Evaluation -- results: MAE=" + mae + ", RMSE="
				+ rmse);
		/* Time spent */
		System.out.println("Evaluation -- Time spent: " + (time2 - time1)
				+ " miliseconds");
		/* Memory used by my application */
		System.out.println("Evaluation -- Allocated memory: "
				+ runtime.totalMemory() / 1024.0 / 1024 + " MB");
	}
}
