package es.uam.eps.tfg.system.test.alt.mains.ratingPrediction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;

/***
 * Class to obtain the RMSE of the algorithms
 * from several train and test files.
 * The result will be the RMSE of the files divided by the number of
 * files. 
 * @author Pablo
 *
 */
public class RatingPredictionComparisonFolds {

	private static final String RESULT_FILE = "./src/es/uam/eps/tfg/system/test/alt/TotalRMSE.txt";

	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity
	protected static final int LCSTHRESHOLD = 1;

	private static final int MIN_NEIGHBOURS = 10; // Minimun of MIN_NEIGHBOURS
													// to start the simulation
	private static final int MAX_NEIGHBOURS = 110; // Maximun of MIN_NEIGHBOURS
													// to end the simulation

	public static void main(String[] args) throws Exception {
		String[] trainfiles = new String[] { 
		//"./datasets/Movielens/u1.base",
		/*
		 * "./datasets/Movielens/u2.base", "./datasets/Movielens/u3.base",
		 * "./datasets/Movielens/u4.base", "./datasets/Movielens/u5.base",
		 */
		
		  "./datasets/LastFm/lastFMtrain1.dat",
		"./datasets/LastFm/lastFMtrain2.dat",
		  "./datasets/LastFm/lastFMtrain3.dat",
		  "./datasets/LastFm/lastFMtrain4.dat",
		  "./datasets/LastFm/lastFMtrain5.dat",
		 

		};

		String[] testfiles = new String[] { 
				//"./datasets/Movielens/u1.test",
		/*
		 * "./datasets/Movielens/u2.test", "./datasets/Movielens/u3.test",
		 * "./datasets/Movielens/u4.test", "./datasets/Movielens/u5.test",
		 */
		
		  "./datasets/LastFm/lastFMtest1.dat",
		 "./datasets/LastFm/lastFMtest2.dat",
		 "./datasets/LastFm/lastFMtest3.dat",
		 "./datasets/LastFm/lastFMtest4.dat",
		  "./datasets/LastFm/lastFMtest5.dat",
		 
		};
		// Array of algorithms to be tested
		String[] algorithms = new String[] {
				//"baseline_random",
				//"baseline_user_bias",
				//"baseline_item_bias",
				//"baseline_system_bias",
				//"baseline_bias",
				//"Pearson_Union0_UB_Std",
				//"Pearson_Union3_UB_Std",
				//"Pearson_Union0_UB_MC",
				//"Pearson_Union3_UB_MC",
				//"Pearson_Normal_UB_Std",
				//"Pearson_Normal_UB_MC",
				//"PearsonPos_Union0_UB_Std",
				"PearsonPos_Union3_UB_Std",
				"PearsonPos_Union0_UB_MC",
				//"PearsonPos_Union3_UB_MC",
				"PearsonPos_Normal_UB_Std",
				"PearsonPos_Normal_UB_MC",
				//"Pearson_Union0_IB_Std",
				//"Pearson_Union0_IB_MC",
				//"Pearson_Union3_IB_Std",
				//"Pearson_Union3_IB_MC",
				//"Pearson_Normal_IB_Std",
				//"Pearson_Normal_IB_MC",
				//"PearsonPos_Union0_IB_Std",
				//"PearsonPos_Union0_IB_MC",
				//"PearsonPos_Union3_IB_Std",
				//"PearsonPos_Union3_IB_MC",
				//"PearsonPos_Normal_IB_Std",
				//"PearsonPos_Normal_IB_MC",
				//"Cosine_Normal_UB_Std",
				//"Cosine_Normal_UB_MC",
				"Cosine_Union3_UB_MC",
				"Cosine_Union3_UB_Std",
				//"CosinePos_Normal_UB_Std",
				//"CosinePos_Normal_UB_MC",
				//"CosinePos_Union3_UB_MC",
				//"CosinePos_Union3_UB_Std",
				//"Cosine_Union3_IB_Std",
				//"Cosine_Union3_IB_MC",
				//"Cosine_Normal_IB_Std",
				//"Cosine_Normal_IB_MC",
				//"CosinePos_Union3_IB_Std",
				//"CosinePos_Union3_IB_MC",
				//"CosinePos_Normal_IB_Std",
				//"CosinePos_Normal_IB_MC",
				//"LCS_-1_0_UB_Std",
				//"LCS_30_0_UB_Std",
				//"LCS_30_0_UB_MC",
				//"LCS_-1_0_UB_MC",
				//"LCS_-1_1_UB_Std",
				//"LCS_30_1_UB_Std",
				//"LCS_30_1_UB_MC",
				//"LCS_-1_1_UB_MC",
				//"LCS_-1_0_IB_Std",
				//"LCS_30_0_IB_Std",
				//"LCS_30_0_IB_MC",
				//"LCS_-1_0_IB_MC",
				//"LCS_-1_1_IB_Std",
				//"LCS_30_1_IB_Std",
				//"LCS_30_1_IB_MC",
				//"LCS_-1_1_IB_MC"
		};

		@SuppressWarnings("unchecked")
		RecommenderSystemIF<Long, Long>[] recommender = new RecommenderSystemIF[algorithms.length];
		TreeMap<String, List<TreeMap<Integer, Double>>> results = new TreeMap<String, List<TreeMap<Integer, Double>>>();

		for (int i = 0; i < algorithms.length; i++) {
			for (int j = 0; j < trainfiles.length; j++) {
				recommender[i] = BuildDatasetRatingPrediction.buildDataset(algorithms[i]);
				// load data
				BufferedReader in = new BufferedReader(new FileReader(
						trainfiles[j]));
				String line = null;
				while ((line = in.readLine()) != null) {
					String[] toks = line.split("\t");
					recommender[i].addInteraction(
							Long.parseLong(toks[COLUMN_USER]),
							Long.parseLong(toks[COLUMN_ITEM]),
							Double.parseDouble(toks[COLUMN_RATING]));
				}
				in.close();
				recommender[i].train(false);
				System.out.println(recommender[i].info()
						+ " Train Finished: file " + (j + 1) + " of "
						+ trainfiles.length + trainfiles[j]);

				for (int k = MIN_NEIGHBOURS; k < MAX_NEIGHBOURS; k += 5) {
					recommender[i].setNeighbours(k);
					in = new BufferedReader(new FileReader(testfiles[j]));
					line = null;
					double rmse = 0.0;
					int n = 0;
					while ((line = in.readLine()) != null) {
						String[] toks = line.split("\t");
						double pred = recommender[i].predictInteraction(
								Long.parseLong(toks[COLUMN_USER]),
								Long.parseLong(toks[COLUMN_ITEM]));
						double real = Double.parseDouble(toks[COLUMN_RATING]);
						if (Double.isNaN(pred))
							continue;
						rmse += (pred - real) * (pred - real);
						n++;
					}
					in.close();
					rmse = Math.sqrt(rmse / n);
					List<TreeMap<Integer, Double>> f = results
							.get(recommender[i].info());
					if (f == null) { // Not found
						List<TreeMap<Integer, Double>> added = new ArrayList<TreeMap<Integer, Double>>();
						TreeMap<Integer, Double> neighRMSE = new TreeMap<Integer, Double>();
						neighRMSE.put(k, rmse);
						added.add(neighRMSE);
						results.put(recommender[i].info(), added);
					} else {// Found
						boolean found = false;
						for (TreeMap<Integer, Double> mn : f) {
							if (mn.get(k) != null) {
								found = true;
								mn.put(k, mn.get(k) + rmse);
							}
						}
						if (found == false) {
							TreeMap<Integer, Double> neighRMSE = new TreeMap<Integer, Double>();
							neighRMSE.put(k, rmse);
							f.add(neighRMSE);
						}
					}
				}
				System.out.println(recommender[i].info()
						+ " Test Finished: file " + (j + 1) + " of "
						+ trainfiles.length + testfiles[j]);
			}

		}
		// PrintResult
		FileWriter result_file = new FileWriter(RESULT_FILE);

		result_file.write("NEIGHBOURS" + ";");
		for (int i = 0; i < algorithms.length; i++)
			result_file.write(recommender[i].info() + ";");
		result_file.write("\n");
		// Now print
		for (int k = MIN_NEIGHBOURS; k < MAX_NEIGHBOURS; k += 5) {
			result_file.write(k + ";");
			for (int i = 0; i < algorithms.length; i++) {
				for (TreeMap<Integer, Double> nm : results.get(recommender[i]
						.info())) {
					if (nm.get(k) != null)
						result_file
								.write((nm.get(k) / (double) trainfiles.length)
										+ ";");
				}

			}
			result_file.write("\n");
		}

		result_file.close();
	}

	
}
