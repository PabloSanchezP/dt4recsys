package es.uam.eps.tfg.system.test.alt.mains;

import es.uam.eps.tfg.system.test.alt.General;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemFactory;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;

/**
 * Main class to test the performance of different data structures
 * All static values should be configured.
 * @author Pablo
 *
 */
public class MemoryTimeStructuresComparison {
	private static final double MEGABYTE = 1024L * 1024L;
	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	private static final int SMALL_PRIME = 139;
	private static final int MEDIUM_PRIME = 827;
	private static final int BIG_PRIME = 2143;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity

	public static double bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}
	
	public static double milisecondsToSeconds(double seconds){
		return seconds/1000.0;
	}

	//./datasets/Movielens/u1.base
	public static void main(String[] args) throws Exception {
		
		String[] trainfiles = new String[] { 
				"./datasets/Movielens/u1.base",
				//"u2.base", 
				//"u3.base",
		/* "./datasets/Movielens/u4.base", "./datasets/Movielens/u5.base", */

		};

		String[] testfiles = new String[] { 
				"./datasets/Movielens/u1.test",
				//"u2.test", 
				//"u3.test",
		/* "./datasets/Movielens/u4.test", "./datasets/Movielens/u5.test", */};

		

		// Array of algorithms to be tested

		System.gc();
		System.out
				.println("Structure;trainReadingTotalTime;trainReadingTotalMemory;trainTotalTime;trainTotalMemory;testReadingTotalTime;"
						+ "testReadingTotalMemory;testTotalTime;testTotalMemory");

		Runtime runtime = Runtime.getRuntime();
		// First time-> baseline

		// load data
		long trainReadingTotalTime = 0;
		long trainReadingTotalMemory = 0;
		long trainTotalTime = 0;
		long trainTotalMemory = 0;
		long testReadingTotalTime = 0;
		long testReadingTotalMemory = 0;
		long testTotalTime = 0;
		long testTotalMemory = 0;
		RecommenderSystemIF<Long, Long> dataset = parseStructure(args[0]);
		int i = 0;
		for (String fileTrain : trainfiles) {


			System.gc();

			// Reading training file
			long usedMemoryBefore = runtime.totalMemory()
					- runtime.freeMemory();
			long timeBefore = System.currentTimeMillis();

			// READ TRAIN
			RecommenderSystemFactory.loadTrainingDataset(dataset, fileTrain,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);

			long usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			long timeAfter = System.currentTimeMillis();
			trainReadingTotalMemory += usedMemoryAfter - usedMemoryBefore;
			trainReadingTotalTime += timeAfter - timeBefore;
			// Finishing reading train. Now we train
			 System.gc();

			// Train
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();
			dataset.train(false);

			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();

			// Obtain memory and time of train
			trainTotalTime += timeAfter - timeBefore;
			trainTotalMemory += usedMemoryAfter - usedMemoryBefore;

			System.gc();

			// Read test
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();

			RecommenderSystemFactory.loadTestDataset(dataset, testfiles[i], 0.0,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);
			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();

			testReadingTotalMemory += usedMemoryAfter - usedMemoryBefore;
			testReadingTotalTime += timeAfter - timeBefore;
			System.gc();

			int[] threshold = new int[] { 1000 };
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();

			// Test time
			dataset.test(0.0);

			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();
			testTotalMemory += usedMemoryAfter - usedMemoryBefore;
			testTotalTime += timeAfter - timeBefore;
			
			
			System.gc();
			
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();
			//COmpute evBean
			dataset.computeEvaluationBean(4.0, threshold);

			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();

			testTotalMemory += usedMemoryAfter - usedMemoryBefore;
			testTotalTime += timeAfter - timeBefore;

			i++;
		}

		/*
		 * System.out.println(structure+";"
		 * +(double)trainReadingTotalTime+";"+trainReadingTotalMemory
		 * +";"+trainTotalTime+";"+trainTotalMemory
		 * +";"+testReadingTotalTime+";"+testReadingTotalMemory
		 * +";"+testTotalTime+";"+testTotalMemory);
		 */

		//Seconds and MB
		double d = trainfiles.length;
		System.out.println(args[0] + ";"
				+ String.format("%.2f", ((double) trainReadingTotalTime / (d*1000)))
				+ ";"
				+ String.format("%.2f", ((double) trainReadingTotalMemory / (d*MEGABYTE)))
				+ ";" + String.format("%.2f", ((double) trainTotalTime / (d*1000)))
				+ ";" + String.format("%.2f", ((double) trainTotalMemory / (d*MEGABYTE)))
				+ ";"
				+ String.format("%.2f", ((double) testReadingTotalTime / (d*1000)))
				+ ";"
				+ String.format("%.2f", ((double) testReadingTotalMemory / (d*MEGABYTE)))
				+ ";" + String.format("%.2f", ((double) testTotalTime / (d*1000)))
				+ ";" + String.format("%.2f", ((double) testTotalMemory / (d*MEGABYTE))));

	}



	private static RecommenderSystemIF<Long, Long> parseStructure(
			String structure) {
		RecommenderSystemIF<Long, Long> result = null;

		if (structure.equals("hashtableredBlackTreeSmallPearsonRedBlack")) {
			result = new NeighbourhoodRanking(SMALL_PRIME, SMALL_PRIME, 100,
					"Pearson Normal", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased,Structures.RedBlackTree);
		} else if (structure.equals("hashtableredBlackTreeSmallPearson23")) {
			result = new NeighbourhoodRanking(SMALL_PRIME, SMALL_PRIME, 23, 100,
					"Pearson Normal", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased);
		} else if (structure.equals("hashtableredBlackTreeMediumPearsonRedBlack")) {
			result = new NeighbourhoodRanking(MEDIUM_PRIME, MEDIUM_PRIME, 100,
					"Pearson Normal", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased,Structures.RedBlackTree);
		} else if (structure.equals("hashtableredBlackTreeMediumPearson23")) {
			result = new NeighbourhoodRanking(MEDIUM_PRIME, MEDIUM_PRIME, 23, 100,
					"Pearson Normal", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased);
		}else if (structure.equals("hashtableredBlackTreeBigPearsonRedBlack")) {
			result = new NeighbourhoodRanking(BIG_PRIME, BIG_PRIME, 100,
					"Pearson Normal", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased,Structures.RedBlackTree);
		} else if (structure.equals("hashtableredBlackTreebigPearson23")) {
			result = new NeighbourhoodRanking(BIG_PRIME, BIG_PRIME, 23, 100,
					"Pearson Normal", Structures.HashTable_RedBlack,
					ReccomendationScheme.UserBased);
		}
		// Now other structures
		else if (structure.equals("redBlackTreePearson")) {
			result = new NeighbourhoodRanking(100,
					"Pearson Normal", Structures.RedBlackTree,
					ReccomendationScheme.UserBased);
		} else if (structure.equals("bTreePearson")) {
			result = new NeighbourhoodRanking(100,
					"Cosine Normal", Structures.BTree,
					ReccomendationScheme.UserBased);
		} else if (structure.equals("orderStatisticTreePearson")) {
			result = new NeighbourhoodRanking(100,
					"Cosine Normal", Structures.OrderStatisticTree,
					ReccomendationScheme.UserBased);
		} else if (structure.equals("linkedlistPearson")) {
			result = new NeighbourhoodRanking(100,
					"Cosine Normal", Structures.DoubleLinkedList,
					ReccomendationScheme.UserBased);
		}
		if (result != null)
			((General) result).setMeanCentering(false);
		return result;
	}
}
