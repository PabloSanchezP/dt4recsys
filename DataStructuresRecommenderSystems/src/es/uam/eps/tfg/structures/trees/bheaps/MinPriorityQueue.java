package es.uam.eps.tfg.structures.trees.bheaps;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.HeapNode;

/**
 * MinPriorityQueue implementation.
 * 
 * @author Pablo
 * 
 * @param <T>
 */
public class MinPriorityQueue<T extends Sorteable<T>> extends MinHeap<T> {

	public MinPriorityQueue(int maximun_lenght) {
		super(maximun_lenght);
	}

	public T minimun() {
		return this.nodes[0].getK();
	}

	@Override
	public void insert(T element) {
		if (this.lenght <= this.size + 1)
			return;
		size++;
		this.nodes[size] = null;
		heapDecreaseKey(size, element);
	}

	public T extractMin() {
		if (this.size == -1)
			return null;
		T result = this.nodes[0].getK();
		this.nodes[0] = this.nodes[size];
		this.size--;
		this.minHeapify(0);
		return result;
	}

	private void heapDecreaseKey(int index, T key) {
		this.nodes[index] = new HeapNode<T>(key);
		while (index > 0
				&& this.nodes[this.parent(index)].getK().sortAgainst(
						this.nodes[index].getK()) > 0) {
			/* Swap elements nodes[index] nodes[parent(index)] */
			HeapNode<T> aux = this.nodes[index];
			this.nodes[index] = this.nodes[parent(index)];
			this.nodes[parent(index)] = aux;
			index = this.parent(index);
		}
	}

	@Override
	public String getStructure() {
		return "MinPriorityQueue";
	}

}
