package es.uam.eps.tfg.structures.trees.bheaps;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.HeapNode;

/***
 * Class of min heap. It inherits from BinaryHeap. It has the same structure as
 * MaxHeap but its methods are for building a min heap.
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 * @version 1.0
 */
public class MinHeap<T extends Sorteable<T>> extends BinaryHeap<T> {

	public MinHeap(int maximun_lenght) {
		super(maximun_lenght);
	}

	/***
	 * This method builds the min-heap. This should be called after inserting
	 * all the nodes of the heap.
	 */
	public void buildMinHeap() {
		this.size = this.lenght - 1;
		for (int i = (int) (this.lenght * 0.5); i >= 0; i--) {
			minHeapify(i);
		}
	}

	/***
	 * To maintain the min-heap property we must call min_heapify. This method
	 * ensure that every children is greater than its parents
	 * 
	 * @param index
	 *            of the node in the array to apply min_heapify
	 */
	public void minHeapify(int index) {
		int smallest;
		int right = right(index);
		int left = left(index);
		smallest = index;
		/*
		 * If the key of the node in the left is less than the key index, the
		 * largest element is the left one
		 */
		if (left <= this.size
				&& this.nodes[left].getK()
						.sortAgainst(this.nodes[index].getK()) < 0)
			smallest = left;

		/* Now, we compare the key in the right */
		if (right <= this.size
				&& this.nodes[right].getK().sortAgainst(
						this.nodes[smallest].getK()) < 0)
			smallest = right;

		if (smallest != index) {
			/* Swap elements, and call again heapify */
			HeapNode<T> aux = this.nodes[index];
			this.nodes[index] = this.nodes[smallest];
			this.nodes[smallest] = aux;

			minHeapify(smallest);
		}
	}

	/***
	 * Method to test if an array is a min heap
	 * 
	 * @param root
	 *            the index of the array
	 * @return true if it is a min heap o flase if not
	 */
	public boolean checkMinHeap(int root) {
		if (root >= this.nodes.length || (2 * root + 2) >= this.nodes.length)
			return true;

		/* check if root is the smallest element */
		if (this.nodes[root].getK()
				.sortAgainst(this.nodes[2 * root + 1].getK()) > 0
				|| (this.nodes[root].getK().sortAgainst(
						this.nodes[2 * root + 2].getK()) > 0))
			return false;

		if (!checkMinHeap(2 * root + 1)) /* check leftsubtree */
			return false;

		if (!checkMinHeap(2 * root + 2)) /* check rightsubtree */
			return false;

		return true;
	}

	@Override
	public String getStructure() {
		return "MinHeap";
	}
}
