package es.uam.eps.tfg.structures.trees.bheaps;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.HeapNode;

/***
 * Abstract class of a binary heap. In a binary heap, the nodes always have 2
 * children, the left and the right.
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 * @version 1.0
 */
public abstract class BinaryHeap<T extends Sorteable<T>> implements
		GeneralStructure<T> {
	protected int size;
	protected int lenght;
	protected HeapNode<T>[] nodes;

	/***
	 * Constructor method it has one argument, the number of nodes that it has.
	 * 
	 * @param maximun_lenght
	 */
	@SuppressWarnings("unchecked")
	public BinaryHeap(int maximumLenght) {
		this.lenght = maximumLenght;
		this.nodes = new HeapNode[lenght];
		this.size = -1;
	}

	/***
	 * Parent function. It returns the parent of the index. As the array starts
	 * in 0, we need to deduct 1 to the index
	 * 
	 * @param index
	 * @return the index of the parents node
	 */
	public int parent(int index) {
		return (int) ((index - 1) * 0.5);
	}

	/***
	 * Left function. It returns the node that is on the left of the index. As
	 * the array starts in 0, we need to add 1 to the index
	 * 
	 * @param index
	 * @return the index of the left node
	 */
	public int left(int index) {
		return 2 * index + 1;
	}

	/***
	 * Right function. It returns the node that is on the right of the index. As
	 * the array starts in 0, we need to add 2 to the index
	 * 
	 * @param index
	 * @return the index of the right node
	 */
	public int right(int index) {
		return 2 * index + 2;
	}

	/** {@inheritDoc} */
	@Override
	public void insert(T element) {
		size++;
		HeapNode<T> n = new HeapNode<T>(element);
		this.nodes[size] = (HeapNode<T>) n;
	}

	/***
	 * Method only included to debug the BinaryHeap
	 */
	public void printHeap(PrintStream out) {
		for (int i = 0; i < this.nodes.length; i++)
			this.nodes[i].printNode(out);
	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < this.nodes.length; i++)
			result += this.nodes[i].toString() + " ";
		return result;

	}

	/** {@inheritDoc} */
	@Override
	public T search(T k) {
		for (int i = 0; i < this.nodes.length; i++)
			if (this.nodes[i].getK().sortAgainst(k) == 0)
				return this.nodes[i].getK();
		return null;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void delete(T element) {
		size--;
		HeapNode<T>[] newnodes = new HeapNode[lenght];
		for (int i = 0; i < size; i++)
			if (!this.nodes[i].getK().equals(element))
				newnodes[i] = this.nodes[i];
		this.nodes = newnodes;
	}

	@Override
	public int getTotalElements() {
		return this.size + 1;
	}

	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		for (int i = 0; i < this.size; i++)
			result.add(this.nodes[i].getK());
		return result;
	}
}
