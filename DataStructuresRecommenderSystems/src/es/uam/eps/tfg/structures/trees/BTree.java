package es.uam.eps.tfg.structures.trees;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.BNode;

/**
 * BTree implementation. A BTree has a root, an integer t that will be used to
 * calculate the maximum and the minimum keys that can be stored in each node.
 * 
 * @author Pablo
 * 
 * @param <T>
 *            the key to store
 */
public class BTree<T extends Sorteable<T>> implements GeneralStructure<T> {
	private BNode<T> root;
	private int totalElements;
	// url of similar implementation ->
	// https://github.com/JPWKU/BTree/blob/master/BTree.java

	public BTree(int t) {
		root = new BNode<T>(t);
		root.setLeaf(true);
		root.setN(0);
		totalElements = 0;
	}
	
	public BTree() {
		root = new BNode<T>(3); //3 by default
		root.setLeaf(true);
		root.setN(0);
		totalElements = 0;
	}

	@Override
	public T search(T k) {
		return root.search(k);
	}

	@Override
	public void insert(T element) {
		BNode<T> r = this.root;
		if (r.getN() == 2 * r.getT() - 1) {
			BNode<T> s = new BNode<T>(r.getT());
			this.root = s;
			s.setLeaf(false);
			s.setN(0);
			s.getC().set(0, r);
			s.splitChild(0);
			s.insertNonFull(element);
		} else
			r.insertNonFull(element);

		totalElements++;
	}

	@Override
	public void delete(T element) {

		if (recursiveRemove(this.root, element) == true)
			this.totalElements--;

		if (this.root != null && this.root.getN() == 0) {
			//Reset root
			this.root = new BNode<T>(this.root.getT());
			root.setLeaf(true);
			root.setN(0);
			totalElements = 0;
		}
	}

	public boolean recursiveRemove(BNode<T> node, T k) {
		boolean f = false;
		int i = 0;
		int[] pos;
		if (node == null)
			return false;

		pos = node.postition(k);
		i = pos[0];
		if (pos[1] != -1)
			f = true;

		if (f == true) {
			if (node.getC().get(i) != null) {
				node.copyPredecessor(i);
				recursiveRemove(node.getC().get(i), node.getKeys().get(i));
			} else
				node.removeData(i);
		} else
			recursiveRemove(node.getC().get(i), k);

		if (node.getC().get(i) != null) {
			if (node.getC().get(i).getN() < ((node.getT() * 2) - 1) / 2)
				node.restore(i);
		}
		return true;

	}

	@Override
	public String getStructure() {
		return "BTree_" + root.getT();
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	@Override
	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		return toArrayListRec(this.root, result);
	}

	private List<T> toArrayListRec(BNode<T> root2, List<T> result) {
		if (root2 != null) {
			if (root2.isLeaf() == true) {
				for (int i = 0; i < root2.getN(); i++)
					result.add(root2.getKeys().get(i));
			} else {
				for (BNode<T> t : root2.getC())
					if (t != null)
						toArrayListRec(t, result);

				for (int i = 0; i < root2.getN(); i++) {
					if (root2.getKeys().get(i) != null)
						result.add(root2.getKeys().get(i));
				}
			}
		}
		return result;
	}

}
