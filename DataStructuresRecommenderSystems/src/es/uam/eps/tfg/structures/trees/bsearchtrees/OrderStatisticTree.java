package es.uam.eps.tfg.structures.trees.bsearchtrees;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.BinaryNode;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.OrderStatisticNode;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.RedBlackNode;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.RedBlackNode.Color;

/****
 * OrderStatisticTree class. It is an extension of the OrderStatisticNode <T>
 * tree class. It works with Order-Statistics-Nodes, which has the attribute
 * size. This trees allow the user to get the i-element of the tree as well as
 * obtain the rank of a node.
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 * @version 1.0
 */
public class OrderStatisticTree<T extends Sorteable<T>> extends RedBlackTree<T> {
	public OrderStatisticTree() {
		super();
		nil = new OrderStatisticNode<T>(null);
		((OrderStatisticNode<T>) nil).setSize(0);
		nil.setColor(Color.BLACK);
		nil.setLeft(nil);
		nil.setRight(nil);
		this.root = nil;
	}

	/** {@inheritDoc} */
	@Override
	public void insert(T element) {
		OrderStatisticNode<T> node = new OrderStatisticNode<T>(element);
		OrderStatisticNode<T> y = (OrderStatisticNode<T>) this.nil;
		OrderStatisticNode<T> x = ((OrderStatisticNode<T>) this.root);
		while (!x.equals(this.nil)) {
			y = x;
			x.setSize(x.getSize() + 1);
			int diff = node.getK().sortAgainst(x.getK());

			/* Smaller node */
			if (diff < 0)
				x = (OrderStatisticNode<T>) x.getLeft();
			else if (diff > 0)
				x = (OrderStatisticNode<T>) x.getRight();
			else if (this.duplicatesAllowed == true)
				x = (OrderStatisticNode<T>) x.getLeft();
			else
				return;
		}
		((BinaryNode<T>) node).setParent(y);
		/* Root */
		if (y.equals(this.nil)) {
			this.root = (BinaryNode<T>) node;
			((RedBlackNode<T>) node).setColor(Color.BLACK);
		}
		/* Smaller */
		else if (node.getK().sortAgainst(y.getK()) < 0)
			y.setLeft((BinaryNode<T>) node);
		else
			y.setRight((BinaryNode<T>) node);
		node.setColor(Color.RED);
		node.setSize(1);
		((BinaryNode<T>) node).setLeft(this.nil);
		((BinaryNode<T>) node).setRight(this.nil);
		fixup(node);
		this.totalElements++;
	}

	/***
	 * Method that obtains the i-th element of the tree.
	 * 
	 * @param x
	 *            the node where we are going to start the search
	 * @param element
	 *            the i-th element to obtain
	 * @return null if no element is found or the i-th node
	 */
	public T os_select(OrderStatisticNode<T> x, int element) {
		if (x == null)
			x = (OrderStatisticNode<T>) this.root;

		if (element < 0)
			return null;

		int rank = ((OrderStatisticNode<T>) x.getLeft()).getSize() + 1;
		if (element == rank)
			return x.getK();
		else if (element < rank)
			return os_select((OrderStatisticNode<T>) x.getLeft(), element);
		else
			return os_select((OrderStatisticNode<T>) x.getRight(),
					(element - rank));
	}

	/***
	 * Method to obtain the rank of a node of the tree
	 * 
	 * @param x
	 *            the node to obtain the rank
	 * @return the rank of that node
	 */
	public int os_rank(OrderStatisticNode<T> x) {
		int r = ((OrderStatisticNode<T>) x.getLeft()).getSize() + 1;
		OrderStatisticNode<T> aux = x;
		while (!aux.equals(this.root)) {
			if (aux == aux.getParent().getRight())
				r += ((OrderStatisticNode<T>) aux.getParent().getLeft())
						.getSize() + 1;
			aux = (OrderStatisticNode<T>) aux.getParent();
		}
		return r;
	}

	/***
	 * Fix-up method it is called every time we insert a new node in the tree. A
	 * new node may violated the restriction of red-black trees, so we need to
	 * maintain the tree in every insertion
	 * 
	 * @param node
	 *            the node where we apply fixup
	 */
	protected void fixup(OrderStatisticNode<T> node) {
		OrderStatisticNode<T> aux = null;
		while (((OrderStatisticNode<T>) node.getParent()).getColor() == Color.RED) {
			if (node.getParent().equals(node.getParent().getParent().getLeft())) {
				aux = (OrderStatisticNode<T>) node.getParent().getParent()
						.getRight();

				if (aux.getColor() == Color.RED) {
					((OrderStatisticNode<T>) node.getParent())
							.setColor(Color.BLACK);
					aux.setColor(Color.BLACK);
					((OrderStatisticNode<T>) node.getParent().getParent())
							.setColor(Color.RED);
					node = (OrderStatisticNode<T>) node.getParent().getParent();
				} else {
					if (node.equals(node.getParent().getRight())) {
						node = (OrderStatisticNode<T>) node.getParent();
						leftRotation(node);
					}
					((OrderStatisticNode<T>) node.getParent())
							.setColor(Color.BLACK);
					((OrderStatisticNode<T>) node.getParent().getParent())
							.setColor(Color.RED);
					rightRotation((OrderStatisticNode<T>) node.getParent()
							.getParent());
				}
			} else {
				aux = (OrderStatisticNode<T>) node.getParent().getParent()
						.getLeft();
				if (aux.getColor() == Color.RED) {
					((OrderStatisticNode<T>) node.getParent())
							.setColor(Color.BLACK);
					aux.setColor(Color.BLACK);
					((OrderStatisticNode<T>) node.getParent().getParent())
							.setColor(Color.RED);
					node = (OrderStatisticNode<T>) node.getParent().getParent();
				} else {
					if (node.equals(node.getParent().getLeft())) {
						node = (OrderStatisticNode<T>) node.getParent();
						rightRotation(node);
					}
					((OrderStatisticNode<T>) node.getParent())
							.setColor(Color.BLACK);
					((OrderStatisticNode<T>) node.getParent().getParent())
							.setColor(Color.RED);
					leftRotation((OrderStatisticNode<T>) node.getParent()
							.getParent());
				}
			}
		}
		((OrderStatisticNode<T>) this.root).setColor(Color.BLACK);
	}

	/***
	 * Same right-rotation of RedBlackTree, but working with OrderStatisticNode
	 * <T>s
	 * 
	 * @param node
	 *            the node to rotate
	 */
	private void rightRotation(OrderStatisticNode<T> node) {
		int rightsize, leftsize;
		super.rightRotation(node);
		((OrderStatisticNode<T>) node.getParent()).setSize(node.getSize());
		if (node.getRight().equals(this.nil))
			rightsize = 0;
		else
			rightsize = ((OrderStatisticNode<T>) node.getRight()).getSize();

		if (node.getLeft().equals(this.nil))
			leftsize = 0;
		else
			leftsize = ((OrderStatisticNode<T>) node.getLeft()).getSize();

		node.setSize(rightsize + leftsize + 1);
	}

	/***
	 * Same left-rotation of RedBlackTree, but working with OrderStatisticNode
	 * <T>s
	 * 
	 * @param node
	 *            the node to rotate
	 */
	private void leftRotation(OrderStatisticNode<T> node) {
		int rightsize, leftsize;
		super.leftRotation(node);
		((OrderStatisticNode<T>) node.getParent()).setSize(node.getSize());

		if (node.getRight().equals(this.nil))
			rightsize = 0;
		else
			rightsize = ((OrderStatisticNode<T>) node.getRight()).getSize();

		if (node.getLeft().equals(this.nil))
			leftsize = 0;
		else
			leftsize = ((OrderStatisticNode<T>) node.getLeft()).getSize();

		node.setSize(rightsize + leftsize + 1);
	}
	


	/** {@inheritDoc} */
	@Override
	public void delete(T element) {
		RedBlackNode<T> z = (RedBlackNode<T>) super.nodeSearch(element);
		RedBlackNode<T> y = (RedBlackNode<T>) z;
		BinaryNode<T> x;
		Color y_color = y.getColor();
		
		//Decrement size of each node
		OrderStatisticNode<T> aux = (OrderStatisticNode<T>) z.getParent();
		while (aux != this.nil){
			aux.setSize(aux.getSize()-1);
			aux=(OrderStatisticNode<T>) aux.getParent();
			
		}
		
		
		if (((BinaryNode<T>) z).getLeft().equals(this.nil)) {
			x = ((BinaryNode<T>) z).getRight();
			transplant((BinaryNode<T>) z, ((BinaryNode<T>) z).getRight());
		} else if (((BinaryNode<T>) z).getRight().equals(this.nil)) {
			x = ((BinaryNode<T>) z).getLeft();
			transplant((BinaryNode<T>) z, ((BinaryNode<T>) z).getLeft());
		} else {
			y = (RedBlackNode<T>) minimum(((RedBlackNode<T>) z).getRight());
			y_color = y.getColor();
			x = y.getRight();
			if (y.getParent().equals(z))
				x.setParent(y);
			else {
				transplant(y, y.getRight());
				y.setRight(((BinaryNode<T>) z).getRight());
				y.getRight().setParent(y);
			}
			transplant((BinaryNode<T>) z, y);
			y.setLeft(((BinaryNode<T>) z).getLeft());
			y.getLeft().setParent(y);
			y.setColor(((RedBlackNode<T>) z).getColor());
		}
		
		
		if (y_color == Color.BLACK)
			this.deleteFixup((RedBlackNode<T>) x);
		this.totalElements--;
	}
	
	@Override
	public String toString() {
		return toStringRevOS((OrderStatisticNode<T>) this.root);
	}

	private String toStringRevOS(OrderStatisticNode<T> node) {
		String result = "";
		/*
		 * As the implementation of red Black node uses the sentinel nil, we
		 * need to check if the node's key is null
		 */
		if (node == null || node.getK() == null)
			return result;
		result += node.toString();
		if (node.getColor()==Color.BLACK)
			result+="b";
		else
			result+="r";
		result+=node.getSize();
		result += " (" + toStringRevOS((OrderStatisticNode<T>) node.getLeft()) + ") ("
				+ toStringRevOS((OrderStatisticNode<T>) node.getRight()) + ")";
		return result;
	}


	/**
	 * Method that is called after delete. This method maintains the red black
	 * property after the deletion by making rotations. (In a similar way that
	 * it is used in insertFixup)
	 * 
	 * @param x
	 *            the node which we are going to fix
	 */
	private void deleteFixup(RedBlackNode<T> x) {
		while (x != this.root && x.getColor() == Color.BLACK) {
			if (x.equals(x.getParent().getLeft())) {
				RedBlackNode<T> w = (RedBlackNode<T>) x.getParent().getRight();
				if (w.getColor() == Color.RED) {
					w.setColor(Color.BLACK);
					((RedBlackNode<T>) x.getParent()).setColor(Color.RED);
					this.leftRotation((OrderStatisticNode<T>) x.getParent());
					w = (RedBlackNode<T>) x.getParent().getRight();
				}
				if (((RedBlackNode<T>) w.getLeft()).getColor() == Color.BLACK
						&& ((RedBlackNode<T>) w.getRight()).getColor() == Color.BLACK) {
					w.setColor(Color.RED);
					x = (RedBlackNode<T>) x.getParent();
				} else {
					if (((RedBlackNode<T>) w.getRight()).getColor() == Color.BLACK) {
						((RedBlackNode<T>) w.getLeft()).setColor(Color.BLACK);
						w.setColor(Color.RED);
						this.rightRotation((OrderStatisticNode<T>)w);
						w = (RedBlackNode<T>) x.getParent().getRight();
					}
					w.setColor(((RedBlackNode<T>) x.getParent()).getColor());
					((RedBlackNode<T>) x.getParent()).setColor(Color.BLACK);
					((RedBlackNode<T>) w.getRight()).setColor(Color.BLACK);
					this.leftRotation((OrderStatisticNode<T>) x.getParent());
					x = (RedBlackNode<T>) this.root;
				}
				

			} else {
				RedBlackNode<T> w = (RedBlackNode<T>) x.getParent().getLeft();
				if (w != null) {
					if (w.getColor() == Color.RED) {
						w.setColor(Color.BLACK);
						((RedBlackNode<T>) x.getParent()).setColor(Color.RED);
						this.rightRotation((OrderStatisticNode<T>) x.getParent());
						w = (RedBlackNode<T>) x.getParent().getLeft();
					}
					if (((RedBlackNode<T>) w.getRight()).getColor() == Color.BLACK
							&& ((RedBlackNode<T>) w.getLeft()).getColor() == Color.BLACK) {
						w.setColor(Color.RED);
						x = (RedBlackNode<T>) x.getParent();
					} else {
						if (((RedBlackNode<T>) w.getLeft()).getColor() == Color.BLACK) {
							((RedBlackNode<T>) w.getRight()).setColor(Color.BLACK);
							w.setColor(Color.RED);
							this.leftRotation((OrderStatisticNode<T>)w);
							w = (RedBlackNode<T>) x.getParent().getLeft();
						}
						w.setColor(((RedBlackNode<T>) x.getParent()).getColor());
						((RedBlackNode<T>) x.getParent()).setColor(Color.BLACK);
						((RedBlackNode<T>) w.getLeft()).setColor(Color.BLACK);
						this.rightRotation((OrderStatisticNode<T>) x.getParent());
						x = (RedBlackNode<T>) this.root;
					}
					
				}
			}
		}
		x.setColor(Color.BLACK);
	}
	

	@Override
	public String getStructure() {
		return "OrderStatisticTree";
	}

}
