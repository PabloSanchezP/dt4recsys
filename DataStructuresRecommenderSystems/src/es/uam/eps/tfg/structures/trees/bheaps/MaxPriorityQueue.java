package es.uam.eps.tfg.structures.trees.bheaps;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.HeapNode;

/**
 * MaxPriorityQueue implementation. It extends from the MaxHeap class as it can
 * be implemented using most of the MaxHeap methods
 * 
 * @author Pablo
 * 
 * @param <T>
 *            the element to insert
 */
public class MaxPriorityQueue<T extends Sorteable<T>> extends MaxHeap<T> {

	public MaxPriorityQueue(int maximum_lenght) {
		super(maximum_lenght);
	}

	public T maximun() {
		return this.nodes[0].getK();
	}

	/***
	 * Insertion method. It must call heapIncrease key in order to maintain the
	 * queue ordered
	 */
	@Override
	public void insert(T element) {
		if (this.lenght <= this.size + 1)
			return;
		size++;
		this.nodes[size] = null;
		heapIncreaseKey(size, element);
	}

	public T extractMax() {
		if (this.size == -1)
			return null;
		T result = this.nodes[0].getK();
		this.nodes[0] = this.nodes[size];
		this.nodes[size] = null;
		this.size--;
		this.maxHeapify(0);
		return result;
	}

	/**
	 * This method maintains the condition of
	 * 
	 * @param index
	 *            the index to start checking
	 * @param key
	 *            the key to be inserted
	 */
	private void heapIncreaseKey(int index, T key) {
		this.nodes[index] = new HeapNode<T>(key);
		while (index > 0
				&& this.nodes[this.parent(index)].getK().sortAgainst(
						this.nodes[index].getK()) < 0) {

			/* Swap elements nodes[index] nodes[parent(index)] */
			HeapNode<T> aux = this.nodes[index];
			this.nodes[index] = this.nodes[parent(index)];
			this.nodes[parent(index)] = aux;
			index = this.parent(index);
		}
	}

	@Override
	public String getStructure() {
		return "MaxPriorityQueue";
	}
}
