package es.uam.eps.tfg.structures.nodes.bsearchnodes;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.Node;

/**
 * Binary node class used in all the binary structures. It has 
 * three attributes (another Binary nodes), the parent of the node and the left and right child.
 * The implemented method are made to access this fields. 
 * 
 * @author Pablo
 * 
 * @param <T>
 *            the element to be inserted
 */
public abstract class BinaryNode<T extends Sorteable<T>> extends Node<T> {
	protected BinaryNode<T> right;
	protected BinaryNode<T> left;
	protected BinaryNode<T> parent;

	public BinaryNode(T k) {
		super(k);
		right = null;
		left = null;
		parent = null;
	}

	public void setLeft(BinaryNode<T> newNode) {
		left = newNode;
	}

	public BinaryNode<T> getLeft() {
		return left;
	}

	public void setRight(BinaryNode<T> newNode) {
		right = newNode;
	}

	public BinaryNode<T> getRight() {
		return right;
	}

	public void setParent(BinaryNode<T> newNode) {
		parent = newNode;
	}

	public BinaryNode<T> getParent() {
		return parent;
	}

	public boolean isRoot() {
		return this.parent == null ? true : false;
	}

	public boolean isEmptyRight() {
		return this.right == null ? true : false;
	}

	public boolean isEmptyLeft() {
		return this.left == null ? true : false;
	}

	public String toString() {
		return this.getK().toString();
	}
}
