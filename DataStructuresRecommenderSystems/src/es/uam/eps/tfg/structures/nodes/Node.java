package es.uam.eps.tfg.structures.nodes;

import java.io.PrintStream;

import es.uam.eps.tfg.structures.Sorteable;

/***
 * General node class. It only has the attribute K which is the element of the
 * node.
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 */
public abstract class Node<T extends Sorteable<T>> {
	protected T k;

	public Node(T k) {
		this.k = k;
	}

	public Node() {

	}

	public T getK() {
		return k;
	}

	public void setK(T k) {
		this.k = k;
	}

	public void printNode(PrintStream out) {
		out.println(this.k.toString());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Node))
			return false;

		@SuppressWarnings("unchecked")
		Node<T> n = (Node<T>) obj;
		if (n.getK() == null && this.getK() == null)
			return true;
		if (n.getK() == null && this.getK() != null)
			return false;
		if (this.getK().sortAgainst(n.getK()) == 0)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return k.toString();
	}

}
