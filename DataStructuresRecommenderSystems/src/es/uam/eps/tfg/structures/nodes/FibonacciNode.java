package es.uam.eps.tfg.structures.nodes;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.Sorteable;

public class FibonacciNode<T extends Sorteable<T>> extends Node<T> implements
		Sorteable<FibonacciNode<T>> {
	private int degree;
	private boolean mark;
	private int children;
	private FibonacciNode<T> parent;
	private FibonacciNode<T> left;
	private FibonacciNode<T> right;
	private List<FibonacciNode<T>> child;

	public FibonacciNode(T k) {
		super(k);
		parent = null;
		child = null;
		degree = 0;
		mark = false;
		children = 0;
		left = this;
		right = this;
	}

	public FibonacciNode<T> getLeft() {
		return left;
	}

	public void setLeft(FibonacciNode<T> left) {
		this.left = left;
	}

	public FibonacciNode<T> getRight() {
		return right;
	}

	public void setRight(FibonacciNode<T> right) {
		this.right = right;
	}

	public void addChild(FibonacciNode<T> node) {
		if (children == 0)
			child = new ArrayList<FibonacciNode<T>>();
		child.add(node);
	}

	/* Getters and setters */

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public boolean getMark() {
		return mark;
	}

	public void setMark(boolean mark) {
		this.mark = mark;
	}

	public int getChildren() {
		return children;
	}

	public void setChildren(int children) {
		this.children = children;
	}

	public FibonacciNode<T> getParent() {
		return parent;
	}

	public void setParent(FibonacciNode<T> parent) {
		this.parent = parent;
	}

	public List<FibonacciNode<T>> getChild() {
		return child;
	}

	public void setChild(List<FibonacciNode<T>> child) {
		this.child = child;
	}

	public T search(T element) {
		T aux = null;
		if (element.sortAgainst(this.k) == 0)
			return this.k;
		if (child == null)
			return null;
		int limit = child.size();
		for (int i = 0; i < limit; i++) {
			aux = child.get(i).search(element);
			if (aux != null)
				return aux;
		}
		return null;

	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < this.children; i++)
			result += this.child.get(i).toString();
		return result;
	}

	@Override
	public int sortAgainst(FibonacciNode<T> o) {
		if (this.getK().sortAgainst(o.getK()) < 0)
			return -1;
		else if (this.getK().sortAgainst(o.getK()) > 0)
			return 1;
		else
			return 0;
	}

}
