package es.uam.eps.tfg.structures.nodes;

import es.uam.eps.tfg.structures.Sorteable;

/**
 * Class that extends the Node class
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 */
public class HeapNode<T extends Sorteable<T>> extends Node<T> {
	public HeapNode(T k) {
		super(k);
	}
}
