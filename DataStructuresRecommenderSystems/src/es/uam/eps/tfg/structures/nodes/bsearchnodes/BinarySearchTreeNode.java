package es.uam.eps.tfg.structures.nodes.bsearchnodes;

import es.uam.eps.tfg.structures.Sorteable;

/***
 * Binary Search Tree Node. It has the same fields as it parents (binary node class)
 * @author Pablo
 *
 * @param <T>
 */
public class BinarySearchTreeNode<T extends Sorteable<T>> extends BinaryNode<T> {

	public BinarySearchTreeNode(T k) {
		super(k);
	}
}
