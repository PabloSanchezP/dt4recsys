package es.uam.eps.tfg.structures.nodes.bsearchnodes;

import es.uam.eps.tfg.structures.Sorteable;

/***
 * Red Black node class. Besides the other attributes (parent, lefht and right side) it has
 * a color (red or black)
 * @author Pablo
 *
 * @param <T>
 */
public class RedBlackNode<T extends Sorteable<T>> extends BinaryNode<T> {
	public enum Color {
		RED, BLACK
	};

	private Color color;

	public RedBlackNode(T k) {
		super(k);
		color = Color.BLACK;
	}

	/* Getters and Setters */

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}
