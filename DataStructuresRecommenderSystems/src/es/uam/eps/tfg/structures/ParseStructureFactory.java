package es.uam.eps.tfg.structures;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.structures.basic.DoubleLinkedList;
import es.uam.eps.tfg.structures.basic.HashTable;
import es.uam.eps.tfg.structures.trees.BTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.BinarySearchTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.OrderStatisticTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.RedBlackTree;
import es.uam.eps.tfg.system.test.alt.General.Element;
import es.uam.eps.tfg.system.test.alt.General.Structures;

public final class ParseStructureFactory {

	/**
	 * Parse structure method. It receives a string indicating a structure and
	 * it returns an element of the Structure enumeration.
	 * 
	 * @param structure
	 * @return an element of the structures enumeration.
	 */
	public static Structures parseStructure(String structure) {
		if (structure.equals("hashtable_tree")) {
			return Structures.HashTable_RedBlack;
		} else if (structure.equals("hashtable_list")) {
			return Structures.HashTable_List;
		} else if (structure.equals("redblacktree")) {
			return Structures.RedBlackTree;
		} else if (structure.equals("orderstatistictree")) {
			return Structures.OrderStatisticTree;
		} else if (structure.equals("binarysearchtree")) {
			return Structures.BinarySearchTree;
		} else if (structure.equals("doublelinkedlist")) {
			return Structures.DoubleLinkedList;
		} else if (structure.equals("btree")) {
			return Structures.BTree;
		}
		return null;
	}

	public static <V extends Sorteable<V>> GeneralStructure<V> getStructure(Structures s, int n) {
		GeneralStructure<V> structure = null;
		switch (s) {
		case HashTable_RedBlack:
			structure = new HashTable<V>(Structures.RedBlackTree, n);
			break;
		case HashTable_List:
			structure = new HashTable<V>(Structures.DoubleLinkedList, n);
			break;
		case RedBlackTree:
			structure = new RedBlackTree<V>();
			break;
		case OrderStatisticTree:
			structure = new OrderStatisticTree<V>();
			break;
		case BinarySearchTree:
			structure = new BinarySearchTree<V>();
			break;
		case DoubleLinkedList:
			structure = new DoubleLinkedList<V>();
			break;
		case BTree:
			structure = new BTree<V>(n);
			break;
		default:
			// do nothing
		}
		return structure;
	}
}
