package es.uam.eps.tfg.structures.advanced;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;

/***
 * DisjointSetForest class.
 * 
 * @author Pablo
 * 
 * @param <T>
 *            the element
 */
public class DisjointSetForest<T extends Sorteable<T>> implements
		GeneralStructure<T> {
	private int totalElements = 0;
	private final HashMap<T, Node<T>> nodes = new HashMap<>();
	protected boolean duplicatesAllowed = false;

	/***
	 * Static class of the nodes of the disjoint set
	 * 
	 * @author Pablo
	 * @param <T>
	 *            the object that represents the node
	 */
	private static class Node<T> {
		/* rank */
		int rank;
		/* All nodes have a pointer to its parent */
		T parent;

		public Node(T parent) {
			this.parent = parent;
			this.rank = 0;
		}
	}

	/** {@inheritDoc} */
	@Override
	/* Equivalent to make-Set */
	public void insert(T element) {
		this.totalElements++;
		Node<T> in = new Node<T>(element);
		nodes.put(element, in);
	}

	public void union(T x, T y) {
		link(findSet(x), findSet(y));
	}

	private void link(T findSet, T findSet2) {
		Node<T> x = nodes.get(findSet);
		Node<T> y = nodes.get(findSet2);
		if (x.rank > y.rank)
			y.parent = findSet;
		else {
			x.parent = findSet2;
			if (x.rank == y.rank)
				y.rank += 1;
		}
	}

	public T findSet(T o) {
		Node<T> node = nodes.get(o);
		if (node == null)
			return null;
		if (!o.equals(node.parent))
			node.parent = this.findSet(node.parent);
		return node.parent;
	}

	@Override
	public String toString() {
		String result = "";
		for (T node : this.nodes.keySet()) {

			String key = node.toString();
			result += key;
		}
		return result;
	}

	@Override
	public void delete(T element) {
		// TODO WRONG
		T searched = search(element);
		if (searched == null)
			return;
		nodes.remove(searched);

		this.totalElements--;
	}

	@Override
	public String getStructure() {
		return "DisjointSetForest";
	}

	/** {@inheritDoc} */
	@Override
	public int getTotalElements() {
		return totalElements;
	}

	@Override
	public T search(T k) {
		Node<T> node = nodes.get(k);
		if (node == null)
			return null;
		return node.parent;
	}

	/**
	 * Removes all elements belonging to the set of the given object.
	 * 
	 * @param o
	 *            the object to remove
	 */
	public void removeSet(T o) {
		T set = findSet(o);
		if (set == null)
			return;
		for (Iterator<T> it = nodes.keySet().iterator(); it.hasNext();) {
			T next = it.next();
			if (next != set && findSet(next) == set) {
				it.remove();
				totalElements--;
			}
		}
		totalElements--;
		nodes.remove(set);
	}

	@Override
	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		Set<T> keys = nodes.keySet();
		result.addAll(keys);
		return result;
	}

}
