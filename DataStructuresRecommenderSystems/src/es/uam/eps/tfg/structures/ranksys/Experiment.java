package es.uam.eps.tfg.structures.ranksys;

import static es.saulvargas.recsys2015.Conventions.getCodec;
import static es.saulvargas.recsys2015.Conventions.getFixedLength;
import static es.saulvargas.recsys2015.Conventions.getPath;
import es.saulvargas.recsys2015.Utils;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.tfg.structures.ParseStructureFactory;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import it.unimi.dsi.fastutil.ints.IntArrays;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import java.io.IOException;
import java.io.File;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import org.jooq.lambda.Unchecked;
import java.util.stream.IntStream;
import static java.util.stream.DoubleStream.of;
import org.ranksys.compression.codecs.CODEC;
import org.ranksys.compression.preferences.BinaryCODECPreferenceData;
import org.ranksys.compression.preferences.RatingCODECPreferenceData;
import org.ranksys.core.util.tuples.Tuple2io;
import org.ranksys.formats.index.ItemsReader;
import org.ranksys.formats.index.UsersReader;
import org.ranksys.formats.parsing.Parser;
import static org.ranksys.formats.parsing.Parsers.ip;
import static org.ranksys.formats.parsing.Parsers.sp;
import org.ranksys.formats.preference.CompressibleBinaryPreferencesFormat;
import org.ranksys.formats.preference.SimpleBinaryPreferencesReader;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.preference.CompressibleRatingPreferencesFormat;

public class Experiment {
	public static void main(String[] args) throws Exception {

		// step structure nusers nitems auxn path dataset idxcodec vcodec [n funname seed]

		int step = Integer.parseInt(args[0]);

		Structures structure = ParseStructureFactory.parseStructure(args[1]);
		int nusers = Integer.parseInt(args[2]);
		int nitems = Integer.parseInt(args[3]);
		int auxn = Integer.parseInt(args[4]);

		String path = args[5];
		String dataset = args[6];
		String idxCodec = args[7];
		String vCodec = args[8];

		switch (step) {
		case 1: {
			// es.saulvargas.recsys2015.Generate
			if (dataset.equals("msd") && !vCodec.equals("null")) {
				System.err.println("does not apply here: implicit data");
				return;
			}
			if (vCodec.startsWith("i")) {
				System.err.println("integrated codec only for ids");
				return;
			}

			switch (dataset) {
			case "ml1M":
			case "ml10M":
			case "ml20M":
			case "netflix":
			case "ymusic":
				store(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, ip, ip);
				break;
			case "msd":
			default:
				store(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, sp, sp);
				break;
			}
		}
			break;
			
		case 2: {
			// es.saulvargas.recsys2015.Benchmark
			int n = parseInt(args[9]);
			String funName = args[10];
			long seed = parseLong(args[11]);

			if (dataset.equals("msd") && !vCodec.equals("null")) {
				System.err.println("does not apply here: implicit data");
				return;
			}
			if (vCodec.startsWith("i")) {
				System.err.println("integrated codec only for ids");
				return;
			}

			test(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, n, funName, seed);
		}
			break;
		}
	}
	
	private static String getStructurePrefix(Structures structure){
		return (structure == null ? "" : structure.toString());
	}

	private static <U, I> void store(Structures structure, int nusers, int nitems, int auxn, String path,
			String dataset, String idxCodec, String vCodec, Parser<U> up, Parser<I> ip) throws IOException {

		FastUserIndex<U> users = SimpleFastUserIndex.load(UsersReader.read(path + "/users.txt", up));
		FastItemIndex<I> items = SimpleFastItemIndex.load(ItemsReader.read(path + "/items.txt", ip));

		String dataPath = path + "/ratings.data";
		String uDataPath = path + "/ratings" + getStructurePrefix(structure) + ".u";
		String iDataPath = path + "/ratings" + getStructurePrefix(structure) + ".i";

		Function<CODEC<?>[], FastPreferenceData<U, I>> cdf = Unchecked.function(cds -> {
			switch (dataset) {
			case "msd":
				CompressibleBinaryPreferencesFormat binaryFormat = CompressibleBinaryPreferencesFormat.get();

				if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
					FastPreferenceData<U, I> data = null;
					if (structure == null) {
						data = SimpleFastPreferenceData.load(SimpleBinaryPreferencesReader.get().read(dataPath, up, ip),
								users, items);
					} else {
						data = GeneralPreferenceData.load(SimpleBinaryPreferencesReader.get().read(dataPath, up, ip),
								users, items, structure, nusers, nitems, auxn);
					}
					binaryFormat.write(data, uDataPath, iDataPath);
				}

				Stream<Tuple2io<int[]>> ulb = binaryFormat.read(uDataPath);
				Stream<Tuple2io<int[]>> ilb = binaryFormat.read(iDataPath);

				return new BinaryCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1]);
			case "ml1M":
			case "ml10M":
			case "ml20M":
			case "netflix":
			case "ymusic":
			default:
				CompressibleRatingPreferencesFormat ratingFormat = CompressibleRatingPreferencesFormat.get();

				if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
					FastPreferenceData<U, I> data = null;
					if (structure == null) {
						data = SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(dataPath, up, ip),
								users, items);
					} else {
						data = GeneralPreferenceData.load(SimpleRatingPreferencesReader.get().read(dataPath, up, ip),
								users, items, structure, nusers, nitems, auxn);
					}
					ratingFormat.write(data, uDataPath, iDataPath);
				}

				Stream<Tuple2io<int[][]>> ulr = ratingFormat.read(uDataPath);
				Stream<Tuple2io<int[][]>> ilr = ratingFormat.read(iDataPath);

				return new RatingCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2]);
			}
		});

		long time0 = System.nanoTime();
		int[] lens = getFixedLength(path, dataset);
		CODEC cd_uidxs = getCodec(idxCodec, lens[0]);
		CODEC cd_iidxs = getCodec(idxCodec, lens[1]);
		CODEC cd_vs = getCodec(vCodec, lens[2]);
		FastPreferenceData<U, I> preferences = cdf.apply(new CODEC[] { cd_uidxs, cd_iidxs, cd_vs });
		double loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
		idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
		System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);

		String fields = dataset + "\t" + idxCodec + "\t" + vCodec;
		System.out.println(fields + "\tus\t" + cd_uidxs.stats()[1]);
		System.out.println(fields + "\tis\t" + cd_iidxs.stats()[1]);
		System.out.println(fields + "\tvs\t" + cd_vs.stats()[1]);

		Utils.serialize(preferences, getPath(path, dataset, idxCodec, vCodec));
	}

	public static <U, I> void test(Structures structure, int nusers, int nitems, int auxn, String path, String dataset,
			String idxCodec, String vCodec, int n, String funName, long seed) throws Exception {
		long time0 = System.nanoTime();
		idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
		FastPreferenceData<U, I> preferences = Utils.deserialize(getPath(path, dataset, idxCodec, vCodec));
		double loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
		System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);

		Random rnd = new Random(seed);
		int[] targetUsers;
		if (funName.contains("_")) {
			int N = parseInt(funName.split("_")[1]);
			funName = funName.split("_")[0];
			targetUsers = rnd.ints(0, preferences.numUsers()).distinct().limit(N).toArray();
		} else {
			targetUsers = preferences.getAllUidx().toArray();
		}
		IntArrays.shuffle(targetUsers, rnd);

		Consumer<FastPreferenceData<U, I>> fun;
		switch (funName) {
		case "urv":
			fun = prefs -> {
				UserSimilarity<U> us = new VectorCosineUserSimilarity<>(prefs, 0.5, true);
				UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
				UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
				IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
			};
			break;
		case "urs":
			fun = prefs -> {
				UserSimilarity<U> us = new SetCosineUserSimilarity<>(prefs, 0.5, true);
				UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
				UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
				IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
			};
			break;
		case "irv":
			fun = prefs -> {
				ItemSimilarity<I> is = new VectorCosineItemSimilarity<>(prefs, 0.5, true);
				ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
				ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
				IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
			};
			break;
		case "irs":
			fun = prefs -> {
				ItemSimilarity<I> is = new SetCosineItemSimilarity<>(prefs, 0.5, true);
				ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
				ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
				IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
			};
			break;
		default:
			fun = null;
			break;
		}

		double[] times = tiktok(fun, preferences, n);

		String fields = dataset + "\t" + idxCodec + "\t" + vCodec + "\t" + funName;
		for (double t : times) {
			System.out.println(fields + "\tt\t" + t);
		}
		System.out.println(fields + "\tlt\t" + times[times.length - 1]);
		System.out.println(fields + "\tat\t" + of(times).average().getAsDouble());
		System.out.println(fields + "\tmt\t" + of(times).min().getAsDouble());
	}

	private static <T> double[] tiktok(Consumer<T> fun, T t, int n) {
		double[] times = new double[n];

		for (int i = 0; i < n; i++) {
			long time0 = System.nanoTime();
			fun.accept(t);
			times[i] = (System.nanoTime() - time0) / 1_000_000_000.0;
		}

		return times;
	}
}
