package es.uam.eps.tfg.structures.ranksys;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.ranksys.core.util.iterators.StreamDoubleIterator;
import org.ranksys.core.util.iterators.StreamIntIterator;

import org.jooq.lambda.tuple.Tuple3;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.AbstractFastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.IdxPref;
import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.ParseStructureFactory;
import es.uam.eps.tfg.system.test.alt.General.Element;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.ints.IntIterator;
import static java.util.stream.IntStream.range;
import static java.util.stream.Stream.empty;

public class GeneralPreferenceData<U, I> extends AbstractFastPreferenceData<U, I>
		implements FastPreferenceData<U, I>, Serializable {

	private static final long serialVersionUID = 8206085997786539541L;

	private final int numPreferences;
	protected final GeneralStructure<Element> uidxs;
	protected final GeneralStructure<Element> iidxs;
	protected final List<Element> uidxList;
	protected final List<Element> iidxList;

	protected GeneralPreferenceData(int numPreferences, GeneralStructure<Element> uidx,
			GeneralStructure<Element> iidx, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
		super(uIndex, iIndex);
		this.numPreferences = numPreferences;
		this.uidxs = uidx;
		this.iidxs = iidx;
		this.uidxList = uidx.toList();
		this.iidxList = iidx.toList();
	}
	
	private static Element toElement(int id){
		Long idLong = new Long(id);
		Element e = new Element(idLong);
		return e;
	}

	@Override
	public int numUsers(int iidx) {
		Element itemsearched = iidxs.search(toElement(iidx));
		if (itemsearched == null) {
			return 0;
		}
		return itemsearched.getTotalRatings();
	}

	@Override
	public int numItems(int uidx) {
		Element u = uidxs.search(toElement(uidx));
		if (u == null) {
			return 0;
		}
		return u.getTotalRatings();
	}

	@Override
	public Stream<IdxPref> getUidxPreferences(int uidx) {
		Element u = uidxs.search(toElement(uidx));
		if (u == null) {
			return Stream.empty();
		}
		GeneralStructure<SorteableLong> e = u.getElementsRated();
		int len = e.getTotalElements();
        if (len == 0) {
            return empty();
        }
        int[] idxs = new int[len];
		double[] vs = new double[len];
		int i=0;
		for (SorteableLong sl: e.toList()){
			idxs[i] = sl.getWrapped().intValue();
			vs[i] = sl.getValue();
			i++;
		}
        return range(0, len).mapToObj(j -> new IdxPref(idxs[j], vs[j]));
	}

	@Override
	public Stream<IdxPref> getIidxPreferences(int iidx) {
		Element ii = iidxs.search(toElement(iidx));
		if (ii == null) {
			return Stream.empty();
		}
		GeneralStructure<SorteableLong> e = ii.getElementsRated();
		int len = e.getTotalElements();
        if (len == 0) {
            return empty();
        }
        int[] idxs = new int[len];
		double[] vs = new double[len];
		int i=0;
		for (SorteableLong sl: e.toList()){
			idxs[i] = sl.getWrapped().intValue();
			vs[i] = sl.getValue();
			i++;
		}
        return range(0, len).mapToObj(j -> new IdxPref(idxs[j], vs[j]));
	}

	@Override
	public int numPreferences() {
		return numPreferences;
	}

	@Override
	public IntStream getUidxWithPreferences() {
		return IntStream.range(0, numUsers()).filter(uidx -> uidxs.search(toElement(uidx)) != null);
	}

	@Override
	public IntStream getIidxWithPreferences() {
		return IntStream.range(0, numItems()).filter(iidx -> iidxs.search(toElement(iidx)) != null);
	}

	@Override
	public int numUsersWithPreferences() {
		return (int) uidxList.stream().filter(iv -> iv != null).count();
	}

	@Override
	public int numItemsWithPreferences() {
		return (int) iidxList.stream().filter(iv -> iv != null).count();
	}

	@Override
	public IntIterator getUidxIidxs(final int uidx) {
		return new StreamIntIterator(getUidxPreferences(uidx).mapToInt(IdxPref::v1));
	}

	@Override
	public DoubleIterator getUidxVs(final int uidx) {
		return new StreamDoubleIterator(getUidxPreferences(uidx).mapToDouble(IdxPref::v2));
	}

	@Override
	public IntIterator getIidxUidxs(final int iidx) {
		return new StreamIntIterator(getIidxPreferences(iidx).mapToInt(IdxPref::v1));
	}

	@Override
	public DoubleIterator getIidxVs(final int iidx) {
		return new StreamDoubleIterator(getIidxPreferences(iidx).mapToDouble(IdxPref::v2));
	}

	@Override
	public boolean useIteratorsPreferentially() {
		return false;
	}

	public static <U, I> GeneralPreferenceData<U, I> load(Stream<Tuple3<U, I, Double>> tuples, FastUserIndex<U> uIndex,
			FastItemIndex<I> iIndex, Structures s, int nu, int ni, int auxSize) {
		AtomicInteger numPreferences = new AtomicInteger();

		// initialize the structures to be used
		GeneralStructure<Element> uidxs = ParseStructureFactory.getStructure(s, nu);
		GeneralStructure<Element> iidxs = ParseStructureFactory.getStructure(s, ni);

		tuples.forEach(t -> {
			int uidx = uIndex.user2uidx(t.v1);
			int iidx = iIndex.item2iidx(t.v2);
			Long user = new Long(uidx);
			Long item = new Long(iidx);
			Double interaction = t.v3;

			numPreferences.incrementAndGet();

			// insert user (if it is new)
			Element newUser = new Element(user);
			Element u = uidxs.search(newUser);
			if (u == null) {
				newUser.FinishAlloc(s, auxSize);
				uidxs.insert(newUser);
				u = newUser;
			}
			// insert interaction
			u.addInteraction(item, interaction);

			// insert item (if it is new)
			Element newItem = new Element(item);
			Element itemsearched = iidxs.search(newItem);
			if (itemsearched == null) {
				newItem.FinishAlloc(s, auxSize);
				iidxs.insert(newItem);
				itemsearched = newItem;
			}
			itemsearched.addInteraction(user, interaction);
		});

		return new GeneralPreferenceData<>(numPreferences.intValue(), uidxs, iidxs, uIndex, iIndex);
	}
}
