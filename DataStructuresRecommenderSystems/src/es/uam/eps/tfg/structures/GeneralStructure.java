package es.uam.eps.tfg.structures;

import java.util.List;

/***
 * General interface of data structures. All data structures implements this
 * interface
 * 
 * @author Pablo
 * 
 * @param <T>
 *            the element which will be used in the data structure.
 */
public interface GeneralStructure<T extends Sorteable<T>> {

	/**
	 * Method to search an element in the structure.
	 * 
	 * @param k
	 *            the element to search
	 * @return that element if is it in the structure or null if is it not
	 */
	public T search(T k);

	/***
	 * Method to insert an element in the structure.
	 * 
	 * @param element
	 *            the element to insert
	 */
	public void insert(T element);

	/***
	 * Method to delete an element of the structure.
	 * 
	 * @param element
	 *            the element to delete
	 */
	public void delete(T element);

	/***
	 * Method to obtain the name of the structure that it is being used at the
	 * moment
	 * 
	 * @return the name of the structure (a String)
	 */
	public String getStructure();

	/***
	 * Method to obtain the total elements stored in the structure
	 * 
	 * @return the elements that are stored.
	 */
	public int getTotalElements();

	/***
	 * Method to obtain all elements a Java List
	 * 
	 * @return the list of elements
	 */
	public List<T> toList();

}
