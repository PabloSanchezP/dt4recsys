package es.uam.eps.tfg.algorithms;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.system.eval.UserTest;

/**
 * Class that implements some algorithms to compute the similitude between 2
 * arguments.
 * 
 * @author Pablo
 * 
 */
public final class Similarity {

	/**
	 * Classic algorithm implementation to compute the similitude. It returns
	 * the percentage of the string that is equal to both of them
	 * 
	 * @param a
	 *            the first string
	 * @param b
	 *            the second string
	 * @return the percentage of elements that are the same
	 */
	@Deprecated
	public static double classic(String a, String b) {
		double result = 0;
		int i;
		long maxlenght = a.length();
		long minlenght = b.length();
		if (maxlenght < minlenght) {
			long aux = maxlenght;
			maxlenght = minlenght;
			minlenght = aux;
		}
		for (i = 0; i < minlenght; i++) {
			if (a.charAt(i) == b.charAt(i))
				result++;
		}
		if (result == 0)
			return 0;
		return result / (double) maxlenght;
	}

	/**
	 * Classic algorithm implementation to compute the similitude. It returns
	 * the percentage of the string that is equal to both of them
	 * 
	 * @param a
	 *            the first string
	 * @param b
	 *            the second string
	 * @return the percentage of elements that are the same
	 */
	@Deprecated
	public static double classic(Integer[] a, Integer[] b) {
		double result = 0;
		int i;
		long maxlenght = a.length;
		long minlenght = b.length;
		if (maxlenght < minlenght) {
			long aux = maxlenght;
			maxlenght = minlenght;
			minlenght = aux;
		}
		for (i = 0; i < minlenght; i++) {
			if (a[i] == b[i])
				result++;
		}
		if (result == 0)
			return 0;
		return result / (double) maxlenght;
	}

	/**
	 * It computes the similitude between the strings applying the cosine Vector
	 * similitude. It computes the scalar product of both vectors and divide
	 * them by the product of the modules.
	 * 
	 * @param a
	 *            the first string
	 * @param b
	 *            the second string
	 * @return the cosine vector
	 */
	@Deprecated
	public static double cosineVector(String a, String b) {
		Integer[] aVector = stringToArrayInt(a);
		Integer[] bVector = stringToArrayInt(b);
		return cosineVector(aVector, bVector);
	}

	/**
	 * It computes the similitude between two vectors applying the cosine Vector
	 * similitude. It computes the scalar product of both vectors and divide
	 * them by the product of the modules.
	 * 
	 * @param a
	 *            the first vector
	 * @param b
	 *            the second vector
	 * @return the cosine vector
	 */
	@Deprecated
	public static double cosineVector(Integer[] aVector, Integer[] bVector) {
		double result;
		int num = escalarProduct(aVector, bVector);

		result = ((double) num / (module(aVector) * module(bVector)));

		return result;
	}

	/***
	 * It computes the CosineVector between 2 vectors. Here, the denominator and
	 * the numerator are computed only with the elements rated by both users
	 * 
	 * @param u
	 *            the items of the first user
	 * @param v
	 *            the items of the second user
	 * @return the cosine vector
	 */
	public static double cosineVectorCommon(GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v) {
		double itemVValue = 0;
		double escalar = 0;
		long uV = 0;
		long vV = 0;
		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			// we ignore items not rated by both users
			if (itemV != null) {
				itemVValue = itemV.value;
				escalar += l.getValue() * itemVValue;
				vV += Math.pow(itemVValue, 2);
				uV += Math.pow(l.getValue(), 2);
			}
		}
		double denominator = (Math.sqrt(uV) * Math.sqrt(vV));
		if (denominator == 0)
			return 0;

		return escalar / denominator;
	}

	/***
	 * Cosine vector without between 2 vectors. In the numerator, it considers
	 * the intersection and in the denominator it considers items of u and v
	 * separately
	 * 
	 * @param u
	 *            the user u
	 * @param v
	 *            the user v
	 * @return the cosine Vector
	 */
	public static double cosineVector(GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v) {
		double itemVValue = 0;
		double escalar = 0;
		long uV = 0;
		long vV = 0;

		// Compute the sum of v values
		for (SorteableLong l : v.toList())
			vV += Math.pow(l.getValue(), 2);

		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			// we ignore items not rated by both users (numerator)
			if (itemV != null) {
				itemVValue = itemV.value;
				escalar += l.getValue() * itemVValue;
			}
			uV += Math.pow(l.getValue(), 2);
		}

		double denominator = (Math.sqrt(uV) * Math.sqrt(vV));
		if (denominator == 0)
			return 0;
		return escalar / denominator;
	}

	/***
	 * It computes the cosine vector between 2 two users. It uses all the items
	 * rated by u. If v has not rated that item, it computes like the default
	 * value.
	 * 
	 * @param u
	 *            the user u
	 * @param v
	 *            the user v
	 * @param defaultValue
	 *            the default value for the items that have not been rated by v
	 * @return the cosine vector
	 */
	public static double cosineVectorFirstUser(
			GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v, long defaultValue) {
		double itemVValue = 0;
		double itemUValue = 0;
		double escalar = 0;
		long uV = 0;
		long vV = 0;
		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			if (itemV != null) {
				itemVValue = itemV.value;
			} else {
				itemVValue = defaultValue;
			}
			itemUValue = l.getValue();
			escalar += itemUValue * itemVValue;
			vV += Math.pow(itemVValue, 2);
			uV += Math.pow(itemUValue, 2);
		}

		double denominator = (Math.sqrt(uV) * Math.sqrt(vV));
		if (denominator == 0)
			return 0;
		return escalar / denominator;
	}

	/***
	 * It computes the cosine vector between 2 two users. It uses the union of
	 * the users u and v
	 * 
	 * @param u
	 *            the user u
	 * @param v
	 *            the user v
	 * @param defaultValue
	 *            the default value for the items that have not been rated by v
	 *            or u
	 * @return the cosine vector
	 */
	public static double cosineVector(GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v, long defaultValue) {
		double itemVValue = 0;
		double itemUValue = 0;
		double escalar = 0;
		long uV = 0;
		long vV = 0;
		// First loop, it considers the u items and the intersection between u
		// and v
		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			if (itemV != null) {
				itemVValue = itemV.value;
			} else {
				itemVValue = defaultValue;
			}
			itemUValue = l.getValue();
			escalar += itemUValue * itemVValue;
			vV += Math.pow(itemVValue, 2);
			uV += Math.pow(itemUValue, 2);
		}
		// Second loop, it considers v values that have not been consider by u
		for (SorteableLong l : v.toList()) {
			SorteableLong itemU = u.search(l);
			if (itemU != null)
				continue;
			itemUValue = defaultValue;
			itemVValue = l.getValue();
			escalar += itemUValue * itemVValue;
			vV += Math.pow(itemVValue, 2);
			uV += Math.pow(itemUValue, 2);
		}

		double denominator = (Math.sqrt(uV) * Math.sqrt(vV));
		if (denominator == 0)
			return 0;
		return escalar / denominator;
	}

	/**
	 * It computes the similitude between the strings applying the Pearson
	 * Correlation.
	 * 
	 * @param a
	 *            the first string
	 * @param b
	 *            the second string
	 * @return the pearson Correlation
	 */
	@Deprecated
	public static double pearsonCorrelation(String a, String b) {
		Integer[] aVector = stringToArrayInt(a);
		Integer[] bVector = stringToArrayInt(b);
		return pearsonCorrelation(aVector, bVector);
	}

	/**
	 * It computes the Pearson correlation between 2 vectors
	 * 
	 * @param aVector
	 *            the first vector
	 * @param bVector
	 *            the second vector
	 * @return the pearson Correlation
	 */
	@Deprecated
	public static double pearsonCorrelation(Integer[] aVector, Integer[] bVector) {
		double numerator = 0;
		double aDenominator = 0;
		double bDenominator = 0;
		double aAvg = avg(aVector);
		double bAvg = avg(bVector);
		int minlenght = aVector.length;
		if (minlenght > bVector.length)
			minlenght = bVector.length;

		/* Numerator */
		for (int i = 0; i < minlenght; i++)
			numerator += (aVector[i] - aAvg) * (bVector[i] - bAvg);

		/* Denominator */
		/* First vector */
		for (int i = 0; i < aVector.length; i++)
			aDenominator += Math.pow((aVector[i] - aAvg), 2);

		/* Second vector */
		for (int i = 0; i < bVector.length; i++)
			bDenominator += Math.pow((bVector[i] - bAvg), 2);
		return numerator / (Math.sqrt(aDenominator * bDenominator));
	}

	/***
	 * Pearson Correlation method that only uses the pearsonCorrelation between
	 * two users using only the items that they have in common (items only in
	 * the intersection)
	 * 
	 * @param u
	 *            the first user
	 * @param v
	 *            the second user
	 * @return the pearsonCorrelation
	 */
	public static double pearsonCorrelation(GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v) {
		double cont;
		double itemVValue = 0;
		double uAvg = 0;
		double vAvg = 0;
		double numerator = 0;
		double aDenominator = 0;
		double bDenominator = 0;

		/* Compute averages */
		cont = 0;
		for (SorteableLong l : u.toList()) {
			uAvg += l.value;
			cont++;
		}
		uAvg /= cont;
		cont = 0;
		for (SorteableLong l : v.toList()) {
			vAvg += l.value;
			cont++;
		}
		vAvg /= cont;

		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			// we ignore items not rated by both users
			if (itemV == null)
				continue;
			itemVValue = itemV.value;
			numerator += (l.getValue() - uAvg) * (itemVValue - vAvg);
			aDenominator += Math.pow((l.value - uAvg), 2);
			bDenominator += Math.pow((itemVValue - vAvg), 2);
		}
		double denominator = (Math.sqrt(aDenominator) * Math.sqrt(bDenominator));
		if (denominator == 0)
			return 0;
		return numerator / denominator;
	}

	/**
	 * Pearon Correlation between 2 users. It uses all the items rated by u. If
	 * an item rated by u is not rated by v, it put the default value
	 * 
	 * @param u
	 *            the user u
	 * @param v
	 *            the user v
	 * @param defaultValue
	 *            the default value
	 * @return the Pearson Correlation
	 */
	public static double pearsonCorrelationFirstUser(
			GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v, double defaultValue) {

		double itemVValue = 0;
		double itemUValue = 0;
		double uAvg = 0;
		double vAvg = 0;
		double numerator = 0;
		double aDenominator = 0;
		double bDenominator = 0;

		/* Compute averages */
		int cont = 0;
		for (SorteableLong l : u.toList()) {
			uAvg += l.value;
			cont++;
		}
		uAvg /= cont;
		cont = 0;
		for (SorteableLong l : v.toList()) {
			vAvg += l.value;
			cont++;
		}
		vAvg /= cont;

		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			if (itemV != null) {
				itemVValue = itemV.value;
			} else {
				itemVValue = defaultValue;
			}
			itemUValue = l.getValue();
			numerator += (itemUValue - uAvg) * (itemVValue - vAvg);
			aDenominator += Math.pow((itemUValue - uAvg), 2);
			bDenominator += Math.pow((itemVValue - vAvg), 2);
		}
		double denominator = (Math.sqrt(aDenominator) * Math.sqrt(bDenominator));
		if (denominator == 0)
			return 0;

		return numerator / denominator;
	}

	/**
	 * Pearon Correlation between 2 users. It uses the union of the items rated
	 * by u and by v
	 * 
	 * @param u
	 *            the user u
	 * @param v
	 *            the user v
	 * @param defaultValue
	 *            the default value
	 * @return the Pearson Correlation
	 */
	public static double pearsonCorrelation(GeneralStructure<SorteableLong> u,
			GeneralStructure<SorteableLong> v, double defaultValue) {

		double itemVValue = 0;
		double itemUValue = 0;
		double uAvg = 0;
		double vAvg = 0;
		double numerator = 0;
		double aDenominator = 0;
		double bDenominator = 0;

		/* Compute averages */
		int cont = 0;
		for (SorteableLong l : u.toList()) {
			uAvg += l.value;
			cont++;
		}
		uAvg /= cont;
		cont = 0;
		for (SorteableLong l : v.toList()) {
			vAvg += l.value;
			cont++;
		}
		vAvg /= cont;

		// First loop u items and intersection
		for (SorteableLong l : u.toList()) {
			SorteableLong itemV = v.search(l);
			if (itemV != null) {
				itemVValue = itemV.value;
			} else {
				itemVValue = defaultValue;
			}
			itemUValue = l.getValue();
			numerator += (itemUValue - uAvg) * (itemVValue - vAvg);
			aDenominator += Math.pow((itemUValue - uAvg), 2);
			bDenominator += Math.pow((itemVValue - vAvg), 2);
		}
		// Second loop
		for (SorteableLong l : v.toList()) {
			SorteableLong itemU = u.search(l);
			if (itemU != null)
				continue;
			itemVValue = l.getValue();
			itemUValue = defaultValue;
			numerator += (itemUValue - uAvg) * (itemVValue - vAvg);
			aDenominator += Math.pow((itemUValue - uAvg), 2);
			bDenominator += Math.pow((itemVValue - vAvg), 2);
		}

		double denominator = (Math.sqrt(aDenominator) * Math.sqrt(bDenominator));
		if (denominator == 0)
			return 0;

		return numerator / denominator;
	}

	/***
	 * Method to obtain the euclidean Distance between 2 strings. It parses
	 * between 2 strings.
	 * 
	 * @param a
	 *            the first string
	 * @param b
	 *            the second string
	 * @return the euclidean distance
	 */
	@Deprecated
	public static double euclideanDistance(String a, String b) {
		Integer[] aVector = stringToArrayInt(a);
		Integer[] bVector = stringToArrayInt(b);
		return euclideanDistance(aVector, bVector);
	}

	/**
	 * Method to obtain the euclidean Distance between 2 array of integers.
	 * 
	 * @param aVector
	 *            the first vector
	 * @param bVector
	 *            the second vector
	 * @return the euclidean distance
	 */
	@Deprecated
	public static double euclideanDistance(Integer[] aVector, Integer[] bVector) {
		int result = 0;
		int minlenght = aVector.length;
		if (minlenght > bVector.length)
			minlenght = bVector.length;
		for (int i = 0; i < minlenght; i++)
			result += Math.pow((aVector[i] - bVector[i]), 2);

		/* If one vector has more length than the other one */
		if (minlenght == aVector.length) {
			for (int i = minlenght; i < bVector.length; i++)
				result += Math.pow(bVector[i], 2);
		} else {
			for (int i = minlenght; i < aVector.length; i++)
				result += Math.pow(aVector[i], 2);
		}
		return Math.sqrt(result);
	}

	/**
	 * Method to compute the average of an array
	 * 
	 * @param a
	 *            the array of integers
	 * @return the average
	 */
	private static double avg(Integer[] a) {
		long result = 0;
		for (int i = 0; i < a.length; i++)
			result += a[i];
		return ((double) result / a.length);

	}

	/**
	 * Method to compute the scalar product between two arrays of integers
	 * 
	 * @param a
	 *            the first array
	 * @param b
	 *            the second array
	 * @return the integer of the escalar product
	 */
	@Deprecated
	private static int escalarProduct(Integer[] a, Integer[] b) {
		int result = 0;
		int minlenght = a.length;
		if (minlenght > b.length)
			minlenght = b.length;

		for (int i = 0; i < minlenght; i++) {
			int aux = a[i] * b[i];
			result += aux;
		}
		return result;
	}

	/**
	 * Method to compute the module of a vector
	 * 
	 * @param a
	 *            the integer vector
	 * @return the module
	 */
	private static double module(Integer[] a) {
		double result = 0;
		for (int i = 0; i < a.length; i++)
			result += Math.pow(a[i], 2);

		return Math.sqrt(result);
	}

	/**
	 * Method to transform a string array to an integer array
	 * 
	 * @param a
	 *            the string
	 * @return the new array of integers
	 */
	private static Integer[] stringToArrayInt(String a) {
		int total = a.length();
		Integer[] result = new Integer[total];
		for (int i = 0; i < total; i++) {
			if (a.charAt(i) == ' ')
				result[i] = 0;
			else
				result[i] = (int) a.charAt(i);
		}
		return result;
	}

	/**
	 * Sorteable Long Class used to represent the items or users, depending on the strategy.
	 * 
	 * -wrapped is the attribute that represents the id 
	 * -value is the rating of that element made by users or items 
	 * -totalRatings is the total ratings of an item or a user 
	 * -totalRated is the sum of the total values of the items or the users
	 * 
	 * This class will be used in 2 ways. If it has value = -1 then it will
	 * store all the information of an item in the application (total Ratings of
	 * that item and total Values) but if value /= -1 the it be an item rated by
	 * an user
	 * 
	 * @author Pablo
	 * 
	 */
	public static class SorteableLong implements Sorteable<SorteableLong>,
			Comparable<SorteableLong>, UserTest<SorteableLong> {
		private Long wrapped;
		private Double value;

		private int totalRatings;
		private double totalRated;

		private List<SorteableLong> test;
		private List<SorteableLong> reccomended;

		public SorteableLong(Long l) {
			this(l, -1.0);
			totalRatings = 0;
			totalRated = 0.0;
			test = new ArrayList<SorteableLong>();
			reccomended = new ArrayList<SorteableLong>();
		}

		@Override
		public List<SorteableLong> getTest() {
			return test;
		}

		public boolean hasRecc(SorteableLong l) {
			for (SorteableLong l2 : reccomended) {
				if (l2.wrapped == l.wrapped)
					return true;
			}
			return false;
		}

		@Override
		public List<SorteableLong> getRecommended() {
			return reccomended;
		}

		public void setTest(List<SorteableLong> newTest) {
			test = newTest;

		}

		@Override
		public void setRecommended(List<SorteableLong> newRec) {
			reccomended = newRec;

		}

		public void addRate(double rate) {
			totalRated += rate;
			totalRatings++;
		}

		public double avg() {
			return totalRated / totalRatings;
		}
		
		public int getTotalRatings() {
			return totalRatings;
		}

		public SorteableLong(Long l, Double v) {
			wrapped = l;
			value = v;
		}

		public Double getValue() {
			return value;
		}

		public Long getWrapped() {
			return wrapped;
		}

		public void setValue(Double v) {
			value = v;
		}

		@Override
		public int sortAgainst(SorteableLong o) {

			return wrapped.compareTo(o.wrapped);
		}

		@Override
		public int compareTo(SorteableLong o) {
			if (o == null)
				return -1;
			int c = value.compareTo(o.value);
			if(c==0){
				c = wrapped.compareTo(o.wrapped);
			}
			return c;
		}

		@Override
		public String toString() {
			return String.valueOf(wrapped);
		}
	}

}
