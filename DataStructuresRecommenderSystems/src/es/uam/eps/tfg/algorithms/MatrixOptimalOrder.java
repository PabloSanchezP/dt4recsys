package es.uam.eps.tfg.algorithms;

/***
 * Class of MatrixOptimalOrder. With this class you can obtain the best order to
 * multiply matrices. The array in which the matrices are inserted is this:
 * -rows columns of the first matrix -columns of the other matrix.
 * 
 * @author Pablo
 * @version 1.0
 */
public class MatrixOptimalOrder {

	private int[][] s;
	private int[][] m;

	/**
	 * Method to obtain the 2 tables s and m. m will contain the minimum number
	 * of scalar multiplications to multiply the matrices and s contains the
	 * optimal value of the parenthesization.
	 * 
	 * @param p
	 *            the array of the matrices
	 */
	public void matrixChainOrder(int[] p) {
		int n = p.length - 1;
		m = new int[n][n];
		s = new int[n][n];
		for (int i = 0; i < n; i++)
			m[i][i] = 0;

		for (int l = 1; l < n; l++) {
			for (int i = 0; i < n - l; i++) {
				int j = i + l;
				m[i][j] = Integer.MAX_VALUE;
				for (int k = i; k < j; k++) {
					int q = m[i][k] + m[k + 1][j] + p[i] * p[k + 1] * p[j + 1];
					if (q < m[i][j]) {
						m[i][j] = q;
						s[i][j] = k;
					}
				}
			}
		}
	}

	public void printOptimalParenthesis() {
		printOptimalParenthesisRec(0, (s.length - 1));
	}

	private void printOptimalParenthesisRec(int i, int j) {
		if (i == j)
			System.out.print(" A" + i + " ");
		else {
			System.out.print("(");
			printOptimalParenthesisRec(i, s[i][j]);
			printOptimalParenthesisRec(s[i][j] + 1, j);
			System.out.print(")");
		}
	}
}
