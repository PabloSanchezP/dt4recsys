package es.uam.eps.tfg.algorithms;

import java.util.ArrayList;
import java.util.List;

/***
 * Class made to obtain the longest common subsequence.
 * It has many implementations depending on the algorithm.
 * 
 * @author Pablo
 * @version 1.0
 */
public final class LongestCommonSubsequence {

	/***
	 * First method to obtain the largest common subsequence of two strings
	 * 
	 * @param x
	 *            the first string
	 * @param y
	 *            the second string
	 * @return a String which is the longest-commonSubsequence of x and y
	 */
	public static String longestCommonSubsequence(String x, String y) {

		int m = x.length();
		int n = y.length();
		int[][] c = new int[m + 1][n + 1];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (x.toCharArray()[i] == y.toCharArray()[j])
					c[i + 1][j + 1] = c[i][j] + 1;
				else
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
			}
		}
		/*
		 * for (int i = 0;i < m+1;i++){ for (int j = 0; j < n+1; j++){
		 * System.out.print(c[i][j] +" "); } System.out.println(); }
		 */

		StringBuffer result = new StringBuffer();
		for (int xlen = m, ylen = n; xlen != 0 && ylen != 0;) {
			if (c[xlen][ylen] == c[xlen - 1][ylen])
				xlen--;
			else if (c[xlen][ylen] == c[xlen][ylen - 1])
				ylen--;
			else {
				result.append(x.charAt(xlen - 1));
				xlen--;
				ylen--;
			}
		}
		return result.reverse().toString();
	}

	/**
	 * Second method to obtain the longest common Subsequence. In this case, we
	 * compare two arrays of strings and we obtain a list with are the longest
	 * commons subsequences of the two arrays
	 * 
	 * @param x
	 *            the first array of strings
	 * @param y
	 *            the second array of strings
	 * @return the list of strings
	 */
	public static List<String> longestCommonSubsequence(String[] x, String[] y) {

		int m = x.length;
		int n = y.length;
		int[][] c = new int[m + 1][n + 1];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (x[i].equals(y[j]))
					c[i + 1][j + 1] = c[i][j] + 1;
				else
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
			}
		}

		List<String> result = new ArrayList<String>();
		for (int xlen = m, ylen = n; xlen != 0 && ylen != 0;) {
			if (c[xlen][ylen] == c[xlen - 1][ylen])
				xlen--;
			else if (c[xlen][ylen] == c[xlen][ylen - 1])
				ylen--;
			else {
				result.add(x[xlen - 1]);
				xlen--;
				ylen--;
			}
		}

		return result;
	}

	/**
	 * Method to compute an approximation of the longestCommonSubsequence. This
	 * method do not returns the elements that are in common, it returns the
	 * number of common elements
	 * 
	 * @param x
	 *            the first array of integers
	 * @param y
	 *            the second array of integer
	 * @return the number of common elements
	 */
	public static int longestCommonSubsequence(int[] x, int[] y, int threshold) {
		int sim = 0;
		int m = x.length;
		int n = y.length;
		int[][] c = new int[m + 1][n + 1];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (Math.abs(x[i] - y[j]) <= threshold)
					c[i + 1][j + 1] = c[i][j] + 1;
				else
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
			}
		}

		for (int xlen = m, ylen = n; xlen != 0 && ylen != 0;) {
			if (c[xlen][ylen] == c[xlen - 1][ylen])
				xlen--;
			else if (c[xlen][ylen] == c[xlen][ylen - 1])
				ylen--;
			else {
				sim++;
				xlen--;
				ylen--;
			}
		}

		return sim;
	}

}
