package es.uam.eps.tfg.transform;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

/***
 * General class that transform a implicit datafile to an explicit one. The
 * implicit datafile must be a file with userid, itemid, and
 * totalwatches/listenings
 * 
 * It can be configured by it getters and setters
 * 
 * @author Pablo
 * 
 */
public class GeneralNormalization {
	private int columnIDUser;
	private int columnIDItem;
	private int columnwatches;
	private boolean firstLineData;

	private int from;
	private int to;

	private TreeMap<String, TreeMap<Integer, ArrayList<String>>> usersRatings;
	private TreeMap<String, Integer> userWatches;

	private String separator;

	public GeneralNormalization() {
		firstLineData = true;
		from = 1;
		to = 5;
		usersRatings = new TreeMap<String, TreeMap<Integer, ArrayList<String>>>();
		userWatches = new TreeMap<String, Integer>();
		columnIDUser = 0;
		columnIDItem = 1;
		columnwatches = 2;

		separator = "\\t";
	}

	public void setSeparator(String n) {
		separator = n;
	}

	public void setColumnIDUser(int column) {
		columnIDUser = column;
	}

	public void setColumnIDItem(int column) {
		columnIDItem = column;
	}

	public void setWatches(int column) {
		columnwatches = column;
	}

	public void transfer2(String origin, String result) {
		BufferedReader br = null;

		FileWriter fw_result = null;
		PrintWriter pw_result = null;
		try {
			String sCurrentLine = null;
			br = new BufferedReader(new FileReader(origin));
			/* First line are the titles */
			if (this.firstLineData)
				sCurrentLine = br.readLine();

			fw_result = new FileWriter(result);
			pw_result = new PrintWriter(fw_result);
			while ((sCurrentLine = br.readLine()) != null) {
				String[] data = sCurrentLine.split(separator);
				String user = data[columnIDUser];
				int w = Integer.parseInt(data[columnwatches]);
				if (userWatches.get(user) == null) {
					userWatches.put(user, w);
				} else {
					// Get the maximun
					if (userWatches.get(user) < w)
						userWatches.put(user, w);
				}
			}
			br.close();
			br = new BufferedReader(new FileReader(origin));
			if (this.firstLineData) // FIrst line
				sCurrentLine = br.readLine();
			while ((sCurrentLine = br.readLine()) != null) {
				String[] data = sCurrentLine.split(separator);
				String user = data[columnIDUser];
				String item = data[columnIDItem];
				int w = Integer.parseInt(data[columnwatches]);
				pw_result.write(user);
				pw_result.write("	");
				pw_result.write(item);
				pw_result.write("	");
				double r = (double) (w * 5) / (double) userWatches.get(user);
				pw_result.write(String.valueOf((int) Math.ceil(r)));
				pw_result.write("\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				pw_result.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void transfer(String origin, String result) {
		BufferedReader br = null;

		FileWriter fw_result = null;
		PrintWriter pw_result = null;
		try {
			String sCurrentLine = null;
			br = new BufferedReader(new FileReader(origin));
			/* First line are the titles */
			if (this.firstLineData)
				sCurrentLine = br.readLine();

			fw_result = new FileWriter(result);
			pw_result = new PrintWriter(fw_result);
			while ((sCurrentLine = br.readLine()) != null) {

				String[] data = sCurrentLine.split(separator);
				String user = data[columnIDUser];
				String item = data[columnIDItem];
				int w = Integer.parseInt(data[columnwatches]);
				if (userWatches.get(user) == null) {
					userWatches.put(user, 1);
				} else
					userWatches.put(user, userWatches.get(user) + 1);
				if (!usersRatings.containsKey(user)) {
					ArrayList<String> it = new ArrayList<String>();
					usersRatings.put(
							user,
							new TreeMap<Integer, ArrayList<String>>(Collections
									.reverseOrder()));
					usersRatings.get(user).put(w, it);
					usersRatings.get(user).get(w).add(item);

				} else if (!usersRatings.get(user).containsKey(w)) {
					ArrayList<String> it = new ArrayList<String>();
					usersRatings.get(user).put(w, it);
					usersRatings.get(user).get(w).add(item);
				} else {
					usersRatings.get(user).get(w).add(item);
				}
			}

			for (String user : usersRatings.keySet()) {
				int cont = 0;
				int first = this.to;
				int totaluser = userWatches.get(user);
				int value = this.to - this.from;
				int cut = totaluser / value;

				for (Integer w : usersRatings.get(user).keySet()) {
					for (String item : usersRatings.get(user).get(w)) {
						cont++;
						if (cont == cut) {
							cont = 0;
							first--;
						}
						pw_result.write(user);
						pw_result.write("	");
						pw_result.write(item);
						pw_result.write("	");
						pw_result.write(String.valueOf(first));
						pw_result.write("\n");
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				pw_result.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	public void setFirstLineData(boolean fl) {
		firstLineData = fl;
	}

	public void setMeasure(int from, int to) {
		this.from = from;
		this.to = to;
	}

}
