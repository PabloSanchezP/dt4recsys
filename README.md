# dt4recsys

##Description
A neighbourhood-based collaborative filtering recommender system implemented in Java. It allows executions   
with different data structures such as: binary search trees, double linked lists, red-black trees, order-statistic trees,   
BTrees, and hash tables.

## Project Setup
The application is an eclipse proyect that can be directly imported using Egit.  

Steps to load directly to eclipse (Egit plugin needed)  
* 1. File->Import->Proyects from git.   
* 2. Clone URI.  
* 3. Put the Uri and put the information to log in.  
* 4. Next and Finish.  

## Testing

The most important main is ConsoleComparison.java (package es.uam.eps.tfg.system.test.alt). Most of the programs work with  
parameters sent by arguments. For more information, there is a documentation directory where you can find the class   
diagrams of the application and the javadoc.

### Unit Tests
There are some unit tests made in JUnit. The location is es.uam.eps.tfg.system.test

## Aditional Info