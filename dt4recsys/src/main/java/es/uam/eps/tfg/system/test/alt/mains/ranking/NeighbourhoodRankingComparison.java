package es.uam.eps.tfg.system.test.alt.mains.ranking;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;

/**
 * Compares ranking results between algorithms from 1 train and one test file
 * @author Pablo
 *
 */
public class NeighbourhoodRankingComparison {
	private static final String TRAINING_FILE = "./datasets/Movielens/u1.base";
	private static final String TEST_FILE = "./datasets/Movielens/u1.test";
	private static final String RESULT_FILE_PRECISION = "./src/es/uam/eps/tfg/system/test/alt/Prediction.txt";
	private static final String RESULT_FILE_RECALL = "./src/es/uam/eps/tfg/system/test/alt/Recall.txt";
	private static final String RESULT_FILE_MRR = "./src/es/uam/eps/tfg/system/test/alt/MRR.txt";
	private static final String RESULT_FILE_NDCG = "./src/es/uam/eps/tfg/system/test/alt/Ndcg.txt";

	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	// Variable that indicates the threshold for LCS to consider in its
	// similarity
	protected static final int LCSTHRESHOLD = 1;

	private static final int MIN_NEIGHBOURS = 10; // Minimun of MIN_NEIGHBOURS
	// to start the simulation
	private static final int MAX_NEIGHBOURS = 10; // Maximun of MIN_NEIGHBOURS
	// to end the simulation

	private static final double HEIGHT = 4;

	public static void main(String[] args) throws Exception {

		// Array of algorithms to be tested
		String[] algorithms = new String[] {
				//"baseline_random",
				//"baseline_user_bias",
				//"baseline_item_bias",
				//"baseline_system_bias",
				//"baseline_bias",
				//"Pearson_Union0_UB_Std",
				//"Pearson_Union3_UB_Std",
				//"Pearson_Union0_UB_MC",
				//"Pearson_Union3_UB_MC",
				//"Pearson_Normal_UB_Std",
				//"Pearson_Normal_UB_MC",
				//"PearsonPos_Union0_UB_Std",
				//"PearsonPos_Union3_UB_Std",
				//"PearsonPos_Union0_UB_MC",
				//"PearsonPos_Union3_UB_MC",
				//"PearsonPos_Normal_UB_Std",
				//"PearsonPos_Normal_UB_MC",
				//"Pearson_Union0_IB_Std",
				//"Pearson_Union0_IB_MC",
				//"Pearson_Union3_IB_Std",
				//"Pearson_Union3_IB_MC",
				//"Pearson_Normal_IB_Std",
				//"Pearson_Normal_IB_MC",
				//"PearsonPos_Union0_IB_Std",
				//"PearsonPos_Union0_IB_MC",
				//"PearsonPos_Union3_IB_Std",
				//"PearsonPos_Union3_IB_MC",
				//"PearsonPos_Normal_IB_Std",
				//"PearsonPos_Normal_IB_MC",
				//"Cosine_Normal_UB_Std",
				//"Cosine_Normal_UB_MC",
				//"Cosine_Union3_UB_MC",
				//"Cosine_Union3_UB_Std",
				//"CosinePos_Normal_UB_Std",
				//"CosinePos_Normal_UB_MC",
				//"CosinePos_Union3_UB_MC",
				//"CosinePos_Union3_UB_Std",
				//"Cosine_Union3_IB_Std",
				//"Cosine_Union3_IB_MC",
				//"Cosine_Normal_IB_Std",
				//"Cosine_Normal_IB_MC",
				//"CosinePos_Union3_IB_Std",
				//"CosinePos_Union3_IB_MC",
				//"CosinePos_Normal_IB_Std",
				//"CosinePos_Normal_IB_MC",
				//"LCS_-1_0_UB_Std",
				//"LCS_30_0_UB_Std",
				//"LCS_30_0_UB_MC",
				//"LCS_-1_0_UB_MC",
				//"LCS_-1_1_UB_Std",
				//"LCS_30_1_UB_Std",
				//"LCS_30_1_UB_MC",
				//"LCS_-1_1_UB_MC",
				//"LCS_-1_0_IB_Std",
				//"LCS_30_0_IB_Std",
				//"LCS_30_0_IB_MC",
				//"LCS_-1_0_IB_MC",
				//"LCS_-1_1_IB_Std",
				//"LCS_30_1_IB_Std",
				//"LCS_30_1_IB_MC",
				//"LCS_-1_1_IB_MC"
		};

		@SuppressWarnings("unchecked")
		RecommenderSystemIF<Long, Long>[] recommender = new RecommenderSystemIF[algorithms.length];

		for (int i = 0; i < algorithms.length; i++)
			recommender[i] = BuildDatasetRanking.buildDataset(algorithms[i], Structures.HashTable_RedBlack);

		for (int i = 0; i < algorithms.length; i++) {
			// load data
			BufferedReader in = new BufferedReader(
					new FileReader(TRAINING_FILE));
			String line = null;
			while ((line = in.readLine()) != null) {
				String[] toks = line.split("\t");
				recommender[i].addInteraction(
						Long.parseLong(toks[COLUMN_USER]),
						Long.parseLong(toks[COLUMN_ITEM]),
						Double.parseDouble(toks[COLUMN_RATING]));
			}
			in.close();
		}

		FileWriter result_file_precision = new FileWriter(RESULT_FILE_PRECISION);
		result_file_precision.write("NEIGHBOURS;");

		FileWriter result_file_mrr = new FileWriter(RESULT_FILE_MRR);
		result_file_mrr.write("NEIGHBOURS;");

		FileWriter result_file_ndcg = new FileWriter(RESULT_FILE_NDCG);
		result_file_ndcg.write("NEIGHBOURS;");

		FileWriter result_file_recall = new FileWriter(RESULT_FILE_RECALL);
		result_file_recall.write("NEIGHBOURS;");

		for (int i = 0; i < algorithms.length; i++) {
			result_file_precision.write(recommender[i].info() + ";");
			result_file_mrr.write(recommender[i].info() + ";");
			result_file_ndcg.write(recommender[i].info() + ";");
			result_file_recall.write(recommender[i].info() + ";");
			recommender[i].train(false);
			// Read all the test iteractions
			try {
				BufferedReader in = new BufferedReader(
						new FileReader(TEST_FILE));
				String line = null;
				while ((line = in.readLine()) != null) {
					String[] toks = line.split("\t");
					recommender[i].addTestInteraction(
							Long.parseLong(toks[COLUMN_USER]),
							Long.parseLong(toks[COLUMN_ITEM]),
							(double) Long.parseLong(toks[COLUMN_RATING]),
							HEIGHT);
				}
				in.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {

				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		result_file_precision.write("\n");
		result_file_mrr.write("\n");
		result_file_ndcg.write("\n");
		result_file_recall.write("\n");

		for (int j = MIN_NEIGHBOURS; j <= MAX_NEIGHBOURS; j += 10) {

			result_file_precision.write(j + ";");
			result_file_mrr.write(j + ";");
			result_file_recall.write(j + ";");
			result_file_ndcg.write(j + ";");
			for (int i = 0; i < algorithms.length; i++) {
				recommender[i].setNeighbours(j);
				recommender[i].test(0.0); // make the test and change neighbours
				String r = ((NeighbourhoodRanking) recommender[i]).evaluateV2();
				// precision / cont+";"+mrr / cont+";"+ndcg / cont+";"+recall /
				// cont;
				String[] splited = r.split(";");
				result_file_precision.write(splited[0] + ";");
				result_file_mrr.write(splited[1] + ";");
				result_file_ndcg.write(splited[2] + ";");
				result_file_recall.write(splited[3] + ";");
				System.out.println("Result:  " + recommender[i].info() + ";"
						+ r);

			}
			result_file_precision.write(j + "\n");
			result_file_mrr.write(j + "\n");
			result_file_ndcg.write(j + "\n");
			result_file_recall.write(j + "\n");
		}

		result_file_precision.close();
		result_file_recall.close();
		result_file_mrr.close();
		result_file_ndcg.close();

	}
}
