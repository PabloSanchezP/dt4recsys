package es.uam.eps.tfg.test.structures;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import es.uam.eps.tfg.datatest.Key;
import es.uam.eps.tfg.structures.advanced.FibonacciHeap;
import es.uam.eps.tfg.structures.basic.DoubleLinkedList;
import es.uam.eps.tfg.structures.basic.HashTable;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.OrderStatisticNode;
import es.uam.eps.tfg.structures.trees.BTree;
import es.uam.eps.tfg.structures.trees.bheaps.MaxHeap;
import es.uam.eps.tfg.structures.trees.bheaps.MaxPriorityQueue;
import es.uam.eps.tfg.structures.trees.bheaps.MinHeap;
import es.uam.eps.tfg.structures.trees.bheaps.MinPriorityQueue;
import es.uam.eps.tfg.structures.trees.bsearchtrees.BinarySearchTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.OrderStatisticTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.RedBlackTree;

/***
 * Structures test
 * 
 * @author Pablo
 * 
 */
public class StructuresManipulationTest {

	@Test
	public void testMinProrityQueueSimple() {
		int elements = 100;
		MinPriorityQueue<Key> queue = new MinPriorityQueue<Key>(elements);
		Integer[] arr = new Integer[elements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		for (int i = 0; i < arr.length; i++) {
			queue.insert(new Key(arr[i]));
		}
		for (int i = 0; i < arr.length; i++)
			assertEquals(i, Integer.parseInt(queue.extractMin().toString()),
					0.1);
	}
	
	@Test
	public void testRedBlack() {
		int elements = 18;
		RedBlackTree<Key> rb = new RedBlackTree<Key>();
		Integer[] arr = new Integer[elements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		for (int i = 0; i < arr.length; i++) {
			rb.insert(new Key(arr[i]));
		}
		
		assertNull(rb.search(new Key(18)));
		assertNotNull(rb.search(new Key(1)));
	}
	
	@Test
	public void OrderStatisticTest() {
		int elements = 18;
		OrderStatisticTree<Key> oe = new OrderStatisticTree<Key>();
		Integer[] arr = new Integer[elements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		for (int i = 0; i < arr.length; i++) {
			oe.insert(new Key(arr[i]));
		}
		
		for (int i = 0; i < arr.length; i++)
			assertEquals(i,Integer.parseInt(oe.os_select((OrderStatisticNode<Key>) oe.getRoot(), i+1).toString()),0.01);
		
	}


	@Test
	public void testMaxProrityQueueSimple() {
		int elements = 100;
		MaxPriorityQueue<Key> queue = new MaxPriorityQueue<Key>(elements);
		Integer[] arr = new Integer[elements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		for (int i = 0; i < arr.length; i++) {
			queue.insert(new Key(arr[i]));
		}

		for (int i = 0; i < arr.length; i++)
			assertEquals(elements - i - 1,
					Integer.parseInt(queue.extractMax().toString()), 0.1);
	}

	@Test
	public void testBTreeSimple3() {
		BTree<Key> b = new BTree<Key>(3);
	
		b.insert(new Key(33));
		b.insert(new Key(20));
		b.insert(new Key(49));
		b.insert(new Key(57));
		b.insert(new Key(90));
		// Split
		b.insert(new Key(30));
		b.insert(new Key(17));
		b.insert(new Key(35));
		b.insert(new Key(10));
		// 3 children
		b.insert(new Key(21));
		b.insert(new Key(40));
		b.insert(new Key(80));
		b.insert(new Key(56));
		b.insert(new Key(8));
		b.insert(new Key(39));
		b.insert(new Key(43));
		b.insert(new Key(60));
		//3 children
		b.insert(new Key(4));
		b.insert(new Key(31));
		b.insert(new Key(99));
		//5 children
		b.insert(new Key(1));
		b.insert(new Key(2));
		b.insert(new Key(11));
		
		assertNull(b.search(new Key(18)));
		assertEquals(Integer.parseInt(new Key(90).toString()),
				Integer.parseInt(b.search(new Key(90)).toString()), 0.01);
		

	}
	
	@Test
	public void testBTreeComplex4() {
		BTree<Key> b = new BTree<Key>(4);

		for (int i = 0 ;i<1000;i++)
			b.insert(new Key(i));
		
		for (int i =1000-1 ; i>=0;i--)
			assertEquals(Integer.parseInt(new Key(i).toString()),
					Integer.parseInt(b.search(new Key(i)).toString()), 0.01);
		
		for (int i =1000-1 ; i>=0;i--)
			b.delete(new Key(i));
		
		for (int i =1000-1 ; i>=1;i--)
			assertNull(b.search(new Key(i)));
		

	}
	
	@Test
	public void testBTreeComplex() {
		BTree<Key> b = new BTree<Key>(3);

		for (int i = 0 ;i<1000;i++)
			b.insert(new Key(i));
		
		for (int i =1000-1 ; i>=0;i--)
			assertEquals(Integer.parseInt(new Key(i).toString()),
					Integer.parseInt(b.search(new Key(i)).toString()), 0.01);
		
		for (int i =1000 ; i<3000;i+=3)
			assertNull(b.search(new Key(i)));
		
	}

	@Test
	public void testOrderStatistic() {
		int max = 10;
		OrderStatisticTree<Key> t = new OrderStatisticTree<Key>();
		Key[] array;
		int i, j;
		array = new Key[max];
		for (i = 0, j = 0; i < max; i++, j += 4) {
			array[i] = new Key(j + 4);
			t.insert(array[i]);
		}

		for (i = 0, j = 0; i < max; i++, j += 4) {
			Key aux = t.os_select(null, i + 1);
			assertEquals(j + 4, Integer.parseInt(aux.toString()), 0.01);
		}

	}

	@Test
	public void DoubleLinkedTest() {
		DoubleLinkedList<Key> test = new DoubleLinkedList<Key>();
		test.insert(new Key(10));
		test.insert(new Key(12));
		test.insert(new Key(13));
		test.insert(new Key(14));
		DoubleLinkedList<Key> test2 = new DoubleLinkedList<Key>();
		test2.insert(new Key(1));
		test2.insert(new Key(2));
		test2.insert(new Key(3));
		test2.insert(new Key(4));
		assertEquals(10, Integer.parseInt(test.search(new Key(10)).toString()),
				0.1);
		assertNull(test2.search(new Key(10)));
		
		

	}

	@Test
	public void HashTableTest() {
		HashTable<Key> test = new HashTable<Key>();
		test.insert(new Key(10));
		test.insert(new Key(12));
		test.insert(new Key(13));
		test.insert(new Key(14));
		assertNull(test.search(new Key(100)));
		assertNotNull(test.search(new Key(10)));
	}

	@Test
	public void FibonacciHeapTest() {

		FibonacciHeap<Key> fheap = new FibonacciHeap<Key>();
		fheap.insert(new Key(10));
		fheap.insert(new Key(5));
		fheap.insert(new Key(4));
		fheap.insert(new Key(7));
		fheap.insert(new Key(1));
		assertNull(fheap.search(new Key(100)));
		assertNotNull(fheap.search(new Key(10)));
	}

	@Test
	public void MinHeapTest() {
		int numberElements = 10;
		MinHeap<Key> min = new MinHeap<Key>(numberElements);
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		for (int i = 0; i < arr.length; i++) {
			min.insert(new Key(arr[i]));
		}
		min.buildMinHeap();
		assertTrue(min.checkMinHeap(0));

	}

	@Test
	public void MaxHeapTest() {
		int numberElements = 10;
		MaxHeap<Key> max = new MaxHeap<Key>(numberElements);
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		for (int i = 0; i < arr.length; i++) {
			max.insert(new Key(arr[i]));
		}
		max.buildMaxHeap();
		assertTrue(max.checkMaxHeap(0));

	}
	
	@Test
	public void BinarySearchTreeDeleteSimple() {
		int numberElements = 10;
		BinarySearchTree<Key> bst = new BinarySearchTree<Key>();
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		for (int i = 0; i < arr.length; i++) {
			bst.insert(new Key(arr[i]));
		}
		assertNotNull(bst.search(new Key(4)));
		bst.delete(new Key(0));
		assertNull(bst.search(new Key(0)));

	}
	@Test
	public void BinarySearchTreeDeleteComplete() {
		int numberElements = 100;
		BinarySearchTree<Key> bst = new BinarySearchTree<Key>();
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		for (int i = 0; i < arr.length; i++) {
			bst.insert(new Key(arr[i]));
		}
		for (int i= 0;i< arr.length;i++)
			bst.delete(new Key(i));
		assertNull(bst.getRoot());
		assertEquals(bst.getTotalElements(),0,0);

	}
	
	@Test
	public void DoubleLinkedListDeleteComplete() {
		int numberElements = 10;
		DoubleLinkedList<Key> dll = new DoubleLinkedList<Key>();
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		for (int i = 0; i < arr.length; i++) {
			dll.insert(new Key(arr[i]));
		}
		for (int i= 0;i< arr.length;i++)
			dll.delete(new Key(i));
		assertNull(dll.getFirst());
		assertEquals(dll.getTotalElements(),0,0);

	}
	
	@Test
	public void RedBlackTreeDeleteComplete() {
		int numberElements = 100;
		RedBlackTree<Key> rdbt = new RedBlackTree<Key>();
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		for (int i = 0; i < arr.length; i++) {
			rdbt.insert(new Key(arr[i]));
		}
		for (int i= 0;i< arr.length;i++)
			rdbt.delete(new Key(i));
		assertNull(rdbt.getRoot().getK());
		assertEquals(rdbt.getTotalElements(),0,0);

	}	

	@Test
	public void OrderStatisticTreeDeleteComplete() {
		int numberElements = 100;
		OrderStatisticTree<Key> ost = new OrderStatisticTree<Key>();
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		for (int i = 0; i < arr.length; i++) {
			ost.insert(new Key(arr[i]));
		}
		for (int i= 0;i< arr.length;i++)
			ost.delete(new Key(i));
		assertNull(ost.getRoot().getK());
		assertEquals(ost.getTotalElements(),0,0);

	}	
	
	@Test
	public void BTreeDeleteSimple() {
		BTree<Key> b = new BTree<Key>(3);

		b.insert(new Key(33));
		b.insert(new Key(20));
		b.insert(new Key(49));
		b.insert(new Key(57));
		b.insert(new Key(90));
		// Split
		b.insert(new Key(30));
		b.insert(new Key(17));
		b.insert(new Key(35));
		b.insert(new Key(10));
		// 3 children
		b.insert(new Key(21));
		b.insert(new Key(40));
		b.insert(new Key(80));
		b.insert(new Key(56));
		b.insert(new Key(8));
		b.insert(new Key(39));
		b.insert(new Key(43));
		b.insert(new Key(60));
		//3 children
		b.insert(new Key(4));
		b.insert(new Key(31));
		b.insert(new Key(99));
		//5 children
		b.insert(new Key(1));
		b.insert(new Key(2));
		b.insert(new Key(11));
		
		assertNotNull(b.search(new Key(60)));
		b.delete(new Key(60));
		assertNull(b.search(new Key(60)));
		
		assertNotNull(b.search(new Key(30)));
		b.delete(new Key(30));
		assertNull(b.search(new Key(30)));
		
		assertNotNull(b.search(new Key(1)));
		b.delete(new Key(1));
		assertNull(b.search(new Key(1)));
		assertNotNull(b.search(new Key(2)));
		b.delete(new Key(2));
		assertNull(b.search(new Key(2)));
		
		assertNotNull(b.search(new Key(11)));
		b.delete(new Key(11));
		assertNull(b.search(new Key(11)));


	}	
	
	
	@Test
	public void BTreeDeleteComplete() {
		int numberElements = 100;
		BTree<Key> bt = new BTree<Key>(3);
		Integer[] arr = new Integer[numberElements];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		for (int i = 0; i < arr.length; i++) {
			bt.insert(new Key(arr[i]));
		}
		for (int i= 0;i< arr.length;i++)
			bt.delete(new Key(i));
		assertEquals(bt.getTotalElements(),0,0);

	}	
	
}
