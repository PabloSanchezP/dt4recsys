package es.uam.eps.tfg.test.algorithms;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uam.eps.tfg.algorithms.LongestCommonSubsequence;
import es.uam.eps.tfg.algorithms.Similarity;
import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.trees.bsearchtrees.OrderStatisticTree;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ratingprediction.neighbourhood.NeighBourhoodSimilarity;

/**
 * JUnit test of Similarity measures.
 * @author Pablo
 *
 */
public class SimilarityTest {

	RecommenderSystemIF<Long, Long> dataset, datasetP, datasetPIB, datasetLCSU;

	@Before
	public void initializeSimilarity() {
		dataset = new NeighBourhoodSimilarity(10, "Cosine Normal",
				Structures.RedBlackTree, 4);
		datasetP = new NeighBourhoodSimilarity(10, "Pearson Normal",
				Structures.RedBlackTree, 4);

		datasetPIB = new NeighBourhoodSimilarity(10, "Pearson Normal",
				Structures.RedBlackTree, 0, ReccomendationScheme.ItemBased);

		datasetLCSU = new NeighBourhoodSimilarity(10, "LCS",
				Structures.RedBlackTree, ReccomendationScheme.UserBased, 0, 1);

	}

	@Test
	public void testCosine() {
		GeneralStructure<SorteableLong> u = new OrderStatisticTree<SorteableLong>();

		u.insert(new SorteableLong((long) 1, 2.0));
		u.insert(new SorteableLong((long) 2, 1.0));
		u.insert(new SorteableLong((long) 3, 0.0));
		u.insert(new SorteableLong((long) 4, 2.0));
		u.insert(new SorteableLong((long) 5, 0.0));
		u.insert(new SorteableLong((long) 6, 1.0));
		u.insert(new SorteableLong((long) 7, 1.0));
		u.insert(new SorteableLong((long) 8, 1.0));

		GeneralStructure<SorteableLong> v = new OrderStatisticTree<SorteableLong>();
		v.insert(new SorteableLong((long) 1, 2.0));
		v.insert(new SorteableLong((long) 2, 1.0));
		v.insert(new SorteableLong((long) 3, 1.0));
		v.insert(new SorteableLong((long) 4, 1.0));
		v.insert(new SorteableLong((long) 5, 1.0));
		v.insert(new SorteableLong((long) 6, 0.0));
		v.insert(new SorteableLong((long) 7, 1.0));
		v.insert(new SorteableLong((long) 8, 1.0));

		assertEquals(Similarity.cosineVector(u, v), 0.822, 0.01);

	}

	@Test
	public void testCosineDiferentItems() {
		GeneralStructure<SorteableLong> u = new OrderStatisticTree<SorteableLong>();

		u.insert(new SorteableLong((long) 1, 1.0));
		u.insert(new SorteableLong((long) 2, 1.0));
		u.insert(new SorteableLong((long) 4, 1.0));

		GeneralStructure<SorteableLong> v = new OrderStatisticTree<SorteableLong>();
		v.insert(new SorteableLong((long) 1, 2.0));
		v.insert(new SorteableLong((long) 2, 1.0));
		v.insert(new SorteableLong((long) 3, 1.0));

		assertEquals(0.7071, Similarity.cosineVector(u, v), 0.01);

	}

	@Test
	public void testCosineDiferentSize() {
		GeneralStructure<SorteableLong> u = new OrderStatisticTree<SorteableLong>();

		u.insert(new SorteableLong((long) 1, 4.0));
		u.insert(new SorteableLong((long) 2, 5.0));
		u.insert(new SorteableLong((long) 3, 4.0));
		u.insert(new SorteableLong((long) 4, 6.0));

		GeneralStructure<SorteableLong> v = new OrderStatisticTree<SorteableLong>();
		v.insert(new SorteableLong((long) 1, 4.0));
		v.insert(new SorteableLong((long) 3, 5.0));

		assertEquals(0.583, Similarity.cosineVector(u, v), 0.01);

	}

	@Test
	public void testPearson() {
		GeneralStructure<SorteableLong> u = new OrderStatisticTree<SorteableLong>();

		u.insert(new SorteableLong((long) 1, 2.0));
		u.insert(new SorteableLong((long) 2, 3.0));
		u.insert(new SorteableLong((long) 3, 4.0));
		u.insert(new SorteableLong((long) 4, 4.0));

		GeneralStructure<SorteableLong> v = new OrderStatisticTree<SorteableLong>();
		v.insert(new SorteableLong((long) 1, 2.0));
		v.insert(new SorteableLong((long) 2, 5.0));
		v.insert(new SorteableLong((long) 3, 5.0));
		v.insert(new SorteableLong((long) 4, 8.0));

		assertEquals(0.86, Similarity.pearsonCorrelation(u, v), 0.01);

	}

	// http://files.grouplens.org/papers/FnT%20CF%20Recsys%20Survey.pdf
	// page 17, example
	@Test
	public void testPearson2() {
		GeneralStructure<SorteableLong> u = new OrderStatisticTree<SorteableLong>();

		u.insert(new SorteableLong((long) 1, 4.0));
		u.insert(new SorteableLong((long) 3, 3.0));
		u.insert(new SorteableLong((long) 4, 5.0));

		GeneralStructure<SorteableLong> v = new OrderStatisticTree<SorteableLong>();
		v.insert(new SorteableLong((long) 1, 5.0));
		v.insert(new SorteableLong((long) 2, 4.0));
		v.insert(new SorteableLong((long) 3, 2.0));

		GeneralStructure<SorteableLong> z = new OrderStatisticTree<SorteableLong>();
		z.insert(new SorteableLong((long) 1, 2.0));
		z.insert(new SorteableLong((long) 2, 4.0));
		z.insert(new SorteableLong((long) 4, 3.0));

		assertEquals(0.78, Similarity.pearsonCorrelation(v, u), 0.01);

		assertEquals(-0.515, Similarity.pearsonCorrelation(v, z), 0.01);

	}
	
	//http://ijcai13.org/files/tutorial_slides/td3.pdf
	@Test
	public void testPearsonAvgIntersection() {
		//In that pdf the average of Pearsons Correlation were only in the equivalent items
		GeneralStructure<SorteableLong> u = new OrderStatisticTree<SorteableLong>();

		u.insert(new SorteableLong((long) 1, 5.0));
		u.insert(new SorteableLong((long) 2, 3.0));
		u.insert(new SorteableLong((long) 3, 4.0));
		u.insert(new SorteableLong((long) 4, 4.0));

		GeneralStructure<SorteableLong> v = new OrderStatisticTree<SorteableLong>();
		v.insert(new SorteableLong((long) 1, 3.0));
		v.insert(new SorteableLong((long) 2, 1.0));
		v.insert(new SorteableLong((long) 3, 2.0));
		v.insert(new SorteableLong((long) 4, 3.0));
		//v.insert(new SorteableLong((long) 5, 3.0));

		GeneralStructure<SorteableLong> z = new OrderStatisticTree<SorteableLong>();
		z.insert(new SorteableLong((long) 1, 4.0));
		z.insert(new SorteableLong((long) 2, 3.0));
		z.insert(new SorteableLong((long) 3, 4.0));
		z.insert(new SorteableLong((long) 4, 3.0));
		//z.insert(new SorteableLong((long) 5, 5.0));

		GeneralStructure<SorteableLong> w = new OrderStatisticTree<SorteableLong>();
		w.insert(new SorteableLong((long) 1, 1.0));
		w.insert(new SorteableLong((long) 2, 5.0));
		w.insert(new SorteableLong((long) 3, 5.0));
		w.insert(new SorteableLong((long) 4, 2.0));
		//w.insert(new SorteableLong((long) 5, 1.0));
		
		assertEquals(0.85, Similarity.pearsonCorrelation(v, u), 0.01);

		assertEquals(0.7, Similarity.pearsonCorrelation(z, u), 0.01);
		
		assertEquals(-0.79, Similarity.pearsonCorrelation(w, u), 0.01);
	}

	@Test
	public void testPearsonHanddbookdatasetPUB() {
		GeneralStructure<SorteableLong> u1 = new OrderStatisticTree<SorteableLong>();
		GeneralStructure<SorteableLong> u2 = new OrderStatisticTree<SorteableLong>();
		GeneralStructure<SorteableLong> u3 = new OrderStatisticTree<SorteableLong>();
		GeneralStructure<SorteableLong> u4 = new OrderStatisticTree<SorteableLong>();

		u1.insert(new SorteableLong((long) 1, 5.0));
		u1.insert(new SorteableLong((long) 2, 1.0));
		u1.insert(new SorteableLong((long) 4, 2.0));
		u1.insert(new SorteableLong((long) 5, 2.0));

		u2.insert(new SorteableLong((long) 1, 1.0));
		u2.insert(new SorteableLong((long) 2, 5.0));
		u2.insert(new SorteableLong((long) 3, 2.0));
		u2.insert(new SorteableLong((long) 4, 5.0));
		u2.insert(new SorteableLong((long) 5, 5.0));

		u3.insert(new SorteableLong((long) 1, 2.0));
		u3.insert(new SorteableLong((long) 3, 3.0));
		u3.insert(new SorteableLong((long) 4, 5.0));
		u3.insert(new SorteableLong((long) 5, 4.0));

		u4.insert(new SorteableLong((long) 1, 4.0));
		u4.insert(new SorteableLong((long) 2, 3.0));
		u4.insert(new SorteableLong((long) 3, 5.0));
		u4.insert(new SorteableLong((long) 4, 3.0));

		assertEquals(-0.938, Similarity.pearsonCorrelation(u1, u2), 0.001);
		assertEquals(-0.839, Similarity.pearsonCorrelation(u1, u3), 0.001);
		assertEquals(0.659, Similarity.pearsonCorrelation(u1, u4), 0.001);

		assertEquals(0.922, Similarity.pearsonCorrelation(u2, u3), 0.001);
		assertEquals(-0.787, Similarity.pearsonCorrelation(u2, u4), 0.001);

		assertEquals(-0.659, Similarity.pearsonCorrelation(u3, u4), 0.001);
	}

	@Test
	public void testPearsonHanddbookdatasetMyExample() {
		GeneralStructure<SorteableLong> u1 = new OrderStatisticTree<SorteableLong>();
		GeneralStructure<SorteableLong> u2 = new OrderStatisticTree<SorteableLong>();

		u1.insert(new SorteableLong((long) 1, 3.0));
		u1.insert(new SorteableLong((long) 3, 5.0));
		u1.insert(new SorteableLong((long) 4, 3.0));

		u2.insert(new SorteableLong((long) 1, 4.0));
		u2.insert(new SorteableLong((long) 2, 2.0));
		u2.insert(new SorteableLong((long) 3, 4.0));
		u2.insert(new SorteableLong((long) 4, 3.0));
		u2.insert(new SorteableLong((long) 5, 3.0));

		assertEquals(0.355, Similarity.pearsonCorrelation(u1, u2), 0.001);

	}

	@Test
	public void testPearsonHanddbookExampleUB() {
		datasetPIB.addInteraction((long) 1, (long) 1, 5.0);
		datasetPIB.addInteraction((long) 1, (long) 2, 1.0);
		datasetPIB.addInteraction((long) 1, (long) 4, 2.0);
		datasetPIB.addInteraction((long) 1, (long) 5, 2.0);

		datasetPIB.addInteraction((long) 2, (long) 1, 1.0);
		datasetPIB.addInteraction((long) 2, (long) 2, 5.0);
		datasetPIB.addInteraction((long) 2, (long) 3, 2.0);
		datasetPIB.addInteraction((long) 2, (long) 4, 5.0);
		datasetPIB.addInteraction((long) 2, (long) 5, 5.0);

		datasetPIB.addInteraction((long) 3, (long) 1, 2.0);
		datasetPIB.addInteraction((long) 3, (long) 3, 3.0);
		datasetPIB.addInteraction((long) 3, (long) 4, 5.0);
		datasetPIB.addInteraction((long) 3, (long) 5, 4.0);

		datasetPIB.addInteraction((long) 4, (long) 1, 4.0);
		datasetPIB.addInteraction((long) 4, (long) 2, 3.0);
		datasetPIB.addInteraction((long) 4, (long) 3, 5.0);
		datasetPIB.addInteraction((long) 4, (long) 4, 3.0);

		datasetPIB.train(false);
		double predicted = datasetPIB.predictInteraction((long) 3, (long) 2);
		assertEquals(3.77, predicted, 0.01);
	}

	@Test
	public void testCosinePredictIteraction() {
		/* User 1 */
		dataset.addInteraction((long) 1, (long) 1, 5.0);
		dataset.addInteraction((long) 1, (long) 2, 4.0);
		dataset.addInteraction((long) 1, (long) 3, 3.0);
		dataset.addInteraction((long) 1, (long) 4, 2.0);
		dataset.addInteraction((long) 1, (long) 5, 4.0);

		/* User 2 */
		dataset.addInteraction((long) 2, (long) 1, 4.0);
		dataset.addInteraction((long) 2, (long) 2, 3.0);
		dataset.addInteraction((long) 2, (long) 3, 2.0);
		dataset.addInteraction((long) 2, (long) 6, 1.0);
		dataset.addInteraction((long) 2, (long) 7, 5.0);

		/* User 3 */
		dataset.addInteraction((long) 3, (long) 7, 4.0);
		dataset.addInteraction((long) 3, (long) 6, 2.0);
		dataset.addInteraction((long) 3, (long) 1, 4.0);
		dataset.addInteraction((long) 3, (long) 11, 1.0);
		dataset.addInteraction((long) 3, (long) 2, 5.0);

		dataset.train(false);
		dataset.predictInteraction((long) 1, (long) 6);
		assertEquals(1.9, dataset.predictInteraction((long) 1, (long) 6), 0.1);
	}

	@Test
	public void testPearsonPredictIteraction() {
		/* User 1 */
		datasetP.addInteraction((long) 1, (long) 1, 5.0);
		datasetP.addInteraction((long) 1, (long) 2, 4.0);
		datasetP.addInteraction((long) 1, (long) 3, 3.0);
		datasetP.addInteraction((long) 1, (long) 4, 2.0);
		datasetP.addInteraction((long) 1, (long) 5, 4.0);

		/* User 2 */
		datasetP.addInteraction((long) 2, (long) 1, 4.0);
		datasetP.addInteraction((long) 2, (long) 2, 3.0);
		datasetP.addInteraction((long) 2, (long) 3, 2.0);
		datasetP.addInteraction((long) 2, (long) 6, 1.0);
		datasetP.addInteraction((long) 2, (long) 7, 5.0);

		/* User 3 */
		datasetP.addInteraction((long) 3, (long) 7, 4.0);
		datasetP.addInteraction((long) 3, (long) 6, 2.0);
		datasetP.addInteraction((long) 3, (long) 1, 4.0);
		datasetP.addInteraction((long) 3, (long) 11, 1.0);
		datasetP.addInteraction((long) 3, (long) 2, 5.0);

		datasetP.train(false);
		assertEquals(1.9, datasetP.predictInteraction((long) 1, (long) 6), 0.1);

	}

	@Test
	public void LCS() {
		assertEquals("BCBA", LongestCommonSubsequence.longestCommonSubsequence(
				"ABCBDAB", "BDCABA"));
	}

	@Test
	public void ModifiedLCS() {
		String[] set1 = new String[7];
		set1[0] = "Bad Religion";
		set1[1] = "Artic Monkeys";
		set1[2] = "Social Distortion";
		set1[3] = "The Clash";
		set1[4] = "Ramones";
		set1[5] = "Sex Pistols";
		set1[6] = "Black Keys";
		Arrays.sort(set1);

		String[] set2 = new String[7];
		set2[0] = "Led Zeppelin";
		set2[1] = "ACDC";
		set2[2] = "Black Keys";
		set2[3] = "Stone Temple Pilots";
		set2[4] = "Black Flag";
		set2[5] = "Bad Religion";
		set2[6] = "Ramones";
		Arrays.sort(set2);

		List<String> res = LongestCommonSubsequence.longestCommonSubsequence(
				set1, set2);

		assertNotNull(res.indexOf("Bad Religion"));
		assertNotNull(res.indexOf("Ramones"));
		assertNotNull(res.indexOf("Black Keys"));
		assertEquals(-1, res.indexOf("Black Flag"), 0);
	}

	@Test
	public void testLCSPredict() {
		/* User 1 */
		datasetLCSU.addInteraction((long) 1, (long) 1, 5.0);
		datasetLCSU.addInteraction((long) 1, (long) 2, 4.0);
		datasetLCSU.addInteraction((long) 1, (long) 3, 3.0);
		datasetLCSU.addInteraction((long) 1, (long) 4, 2.0);
		datasetLCSU.addInteraction((long) 1, (long) 5, 4.0);

		/* User 2 */
		datasetLCSU.addInteraction((long) 2, (long) 1, 4.0);
		datasetLCSU.addInteraction((long) 2, (long) 2, 3.0);
		datasetLCSU.addInteraction((long) 2, (long) 3, 2.0);
		datasetLCSU.addInteraction((long) 2, (long) 6, 1.0);
		datasetLCSU.addInteraction((long) 2, (long) 7, 5.0);

		/* User 3 */
		datasetLCSU.addInteraction((long) 3, (long) 1, 4.0);
		datasetLCSU.addInteraction((long) 3, (long) 2, 5.0);
		datasetLCSU.addInteraction((long) 3, (long) 6, 2.0);
		datasetLCSU.addInteraction((long) 3, (long) 7, 4.0);
		datasetLCSU.addInteraction((long) 3, (long) 11, 1.0);

		datasetLCSU.train(false);
		assertEquals(1.92, datasetLCSU.predictInteraction((long) 1, (long) 6),
				0.01);

	}

}
