package es.uam.eps.tfg.datatest;

import es.uam.eps.tfg.structures.Sorteable;

public class Key implements Sorteable<Key> {
	private int key;

	public Key(int value) {
		this.key = value;
	}

	@Override
	public int sortAgainst(Key other) {
		if (other == null)
			return -1;

		if (this.key == other.key)
			return 0;
		else
			return this.key < other.key ? -1 : 1;
	}

	public void printKey() {
		System.out.print(key + " ");
	}

	public String toString() {
		return (key + "");
	}
}
