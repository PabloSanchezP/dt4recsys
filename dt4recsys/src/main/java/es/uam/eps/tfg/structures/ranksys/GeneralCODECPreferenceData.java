package es.uam.eps.tfg.structures.ranksys;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.AbstractFastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.IdxPref;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.TransposedPreferenceData;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.tfg.structures.GeneralStructure;
import static es.uam.eps.tfg.structures.ranksys.Keyed.keyed;
import es.uam.eps.tfg.structures.trees.bsearchtrees.BinarySearchTree;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.doubles.DoubleIterators;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntIterators;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import static java.util.stream.IntStream.range;
import java.util.stream.Stream;
import static java.util.stream.Stream.empty;
import org.jooq.lambda.Unchecked;
import org.ranksys.compression.codecs.CODEC;
import org.ranksys.compression.codecs.NullCODEC;
import static org.ranksys.compression.util.Delta.atled;
import static org.ranksys.compression.util.Delta.delta;
import org.ranksys.core.util.iterators.ArrayDoubleIterator;
import org.ranksys.core.util.iterators.ArrayIntIterator;
import org.ranksys.core.util.tuples.Tuple2io;
import static org.ranksys.core.util.tuples.Tuples.tuple;
import org.ranksys.formats.index.ItemsReader;
import org.ranksys.formats.index.UsersReader;
import static org.ranksys.formats.parsing.Parsers.sp;
import org.ranksys.formats.preference.SimpleBinaryPreferencesReader;

/**
 *
 * @author Saúl Vargas (Saul@VargasSandoval.es)
 */
public class GeneralCODECPreferenceData<U, I, Cu, Ci, Cr> extends AbstractFastPreferenceData<U, I> implements FastPreferenceData<U, I> {

    private final CODEC<Cu> uCodec;
    private final CODEC<Ci> iCodec;
    private final CODEC<Cr> ratCodec;

    private final Int2IntMap uCounts;
    private final GeneralStructure<Keyed<Cu, Cr>> uPrefs;
    private final Int2IntMap iCounts;
    private final GeneralStructure<Keyed<Ci, Cr>> iPrefs;
    private boolean hasRatings;

    public GeneralCODECPreferenceData(FastPreferenceData<U, I> preferences, FastUserIndex<U> users, FastItemIndex<I> items, CODEC<Cu> uCodec, CODEC<Ci> iCodec, CODEC<Cr> ratCodec, Supplier<GeneralStructure<Keyed<Cu, Cr>>> structureUSupplier, Supplier<GeneralStructure<Keyed<Ci, Cr>>> structureISupplier) {
        this(ul(preferences), il(preferences), users, items, uCodec, iCodec, ratCodec, structureUSupplier, structureISupplier);
    }

    public GeneralCODECPreferenceData(FastPreferenceData<U, I> preferences, FastUserIndex<U> users, FastItemIndex<I> items, CODEC<Cu> uCodec, CODEC<Ci> iCodec, CODEC<Cr> ratCodec, Supplier<GeneralStructure<Keyed<Cu, Cr>>> structureUSupplier, Supplier<GeneralStructure<Keyed<Ci, Cr>>> structureISupplier, boolean hasRatings) {
        this(ulb(preferences), ilb(preferences), users, items, uCodec, iCodec, ratCodec, structureUSupplier, structureISupplier, false);
    }

    private static Stream<Tuple2io<int[][]>> ul(FastPreferenceData<?, ?> preferences) {
        return preferences.getUidxWithPreferences().mapToObj(k -> {
            IdxPref[] pairs = preferences.getUidxPreferences(k)
                    .sorted((p1, p2) -> Integer.compare(p1.v1, p2.v1))
                    .toArray(n -> new IdxPref[n]);
            int[] idxs = new int[pairs.length];
            int[] vs = new int[pairs.length];
            for (int i = 0; i < pairs.length; i++) {
                idxs[i] = pairs[i].v1;
                vs[i] = (int) pairs[i].v2;
            }
            return tuple(k, new int[][]{idxs, vs});
        });
    }

    private static Stream<Tuple2io<int[][]>> il(FastPreferenceData<?, ?> preferences) {
        return ul(new TransposedPreferenceData<>(preferences));
    }

    private static Stream<Tuple2io<int[]>> ulb(FastPreferenceData<?, ?> preferences) {
        return preferences.getUidxWithPreferences().mapToObj(k -> {
            IdxPref[] pairs = preferences.getUidxPreferences(k)
                    .sorted((p1, p2) -> Integer.compare(p1.v1, p2.v1))
                    .toArray(n -> new IdxPref[n]);
            int[] idxs = new int[pairs.length];
            for (int i = 0; i < pairs.length; i++) {
                idxs[i] = pairs[i].v1;
            }
            return tuple(k, idxs);
        });
    }

    private static Stream<Tuple2io<int[]>> ilb(FastPreferenceData<?, ?> preferences) {
        return ulb(new TransposedPreferenceData<>(preferences));
    }

    public GeneralCODECPreferenceData(Stream<Tuple2io<int[][]>> ul, Stream<Tuple2io<int[][]>> il, FastUserIndex<U> users, FastItemIndex<I> items, CODEC<Cu> uCodec, CODEC<Ci> iCodec, CODEC<Cr> ratCodec, Supplier<GeneralStructure<Keyed<Cu, Cr>>> structureUSupplier, Supplier<GeneralStructure<Keyed<Ci, Cr>>> structureISupplier) {
        this(uCodec, iCodec, ratCodec,
                new Int2IntOpenHashMap(), structureUSupplier.get(),
                new Int2IntOpenHashMap(), structureISupplier.get(),
                users, items);
        hasRatings = true;

        index(ul, uPrefs, uCounts, uCodec, this.ratCodec);
        index(il, iPrefs, iCounts, iCodec, this.ratCodec);
    }

    public GeneralCODECPreferenceData(Stream<Tuple2io<int[]>> ul, Stream<Tuple2io<int[]>> il, FastUserIndex<U> users, FastItemIndex<I> items, CODEC<Cu> uCodec, CODEC<Ci> iCodec, CODEC<Cr> ratCodec, Supplier<GeneralStructure<Keyed<Cu, Cr>>> structureUSupplier, Supplier<GeneralStructure<Keyed<Ci, Cr>>> structureISupplier, boolean hasRatings) {
        this(uCodec, iCodec, ratCodec,
                new Int2IntOpenHashMap(), structureUSupplier.get(),
                new Int2IntOpenHashMap(), structureISupplier.get(),
                users, items);
        if (hasRatings) {
            // invalid
            return;
        }
        hasRatings = false;

        index(ul, uPrefs, uCounts, uCodec, this.ratCodec, false);
        index(il, iPrefs, iCounts, iCodec, this.ratCodec, false);
    }

    private <Cx, Cv> void index(Stream<Tuple2io<int[][]>> lists, GeneralStructure<Keyed<Cx, Cv>> prefs, Int2IntMap lens, CODEC<Cx> x_codec, CODEC<Cv> r_codec) {
        lists.forEach(list -> {
            int k = list.v1;
            int[] _idxs = list.v2[0];
            int[] _vs = list.v2[1];

            lens.put(k, _idxs.length);
            if (!x_codec.isIntegrated()) {
                delta(_idxs, 0, _idxs.length);
            }
            Cx idxs = x_codec.co(_idxs, 0, _idxs.length);
            Cv rats = r_codec.co(_vs, 0, _vs.length);
            prefs.insert(keyed(k, idxs, rats));
        });
    }

    private <Cx, Cv> void index(Stream<Tuple2io<int[]>> lists, GeneralStructure<Keyed<Cx, Cv>> prefs, Int2IntMap lens, CODEC<Cx> x_codec, CODEC<Cv> r_codec, boolean hasRatings) {
        lists.forEach(list -> {
            int k = list.v1;
            int[] _idxs = list.v2;
            int[] _vs = new int[]{Integer.parseInt("1")};

            lens.put(k, _idxs.length);
            if (!x_codec.isIntegrated()) {
                delta(_idxs, 0, _idxs.length);
            }
            Cx idxs = x_codec.co(_idxs, 0, _idxs.length);
            Cv rats = r_codec.co(_vs, 0, _vs.length);
            prefs.insert(keyed(k, idxs, rats));
        });
    }

    public GeneralCODECPreferenceData(CODEC<Cu> uCodec, CODEC<Ci> iCodec, CODEC<Cr> ratCodec, Int2IntMap u_len, GeneralStructure<Keyed<Cu, Cr>> u_prefs, Int2IntMap i_len, GeneralStructure<Keyed<Ci, Cr>> i_prefs, FastUserIndex<U> userIndex, FastItemIndex<I> itemIndex) {
        super(userIndex, itemIndex);
        this.uCodec = uCodec;
        this.iCodec = iCodec;
        this.ratCodec = ratCodec;
        this.uCounts = u_len;
        this.uPrefs = u_prefs;
        this.iCounts = i_len;
        this.iPrefs = i_prefs;
    }

    @Override
    public int numUsers(int iidx) {
        return iCounts.get(iidx);
    }

    @Override
    public int numItems(int uidx) {
        return uCounts.get(uidx);
    }

    @Override
    public int numPreferences() {
        return uCounts.values().stream().mapToInt(i -> i).sum();
    }

    @Override
    public IntStream getUidxWithPreferences() {
        return uCounts.keySet().stream().mapToInt(i -> i);
    }

    @Override
    public IntStream getIidxWithPreferences() {
        return iCounts.keySet().stream().mapToInt(i -> i);
    }

    @Override
    public Stream<IdxPref> getUidxPreferences(final int uidx) {
        return getPreferences(uPrefs.search(keyed(uidx)), uCounts.get(uidx), uCodec, ratCodec);
    }

    @Override
    public Stream<IdxPref> getIidxPreferences(final int iidx) {
        return getPreferences(iPrefs.search(keyed(iidx)), iCounts.get(iidx), iCodec, ratCodec);
    }

    @Override
    public IntIterator getUidxIidxs(final int uidx) {
        return getIdx(uPrefs.search(keyed(uidx)), uCounts.get(uidx), uCodec);
    }

    @Override
    public DoubleIterator getUidxVs(final int uidx) {
        return getVs(uPrefs.search(keyed(uidx)), uCounts.get(uidx), ratCodec);
    }

    @Override
    public IntIterator getIidxUidxs(final int iidx) {
        return getIdx(iPrefs.search(keyed(iidx)), iCounts.get(iidx), iCodec);
    }

    @Override
    public DoubleIterator getIidxVs(final int iidx) {
        return getVs(iPrefs.search(keyed(iidx)), iCounts.get(iidx), ratCodec);
    }

    @Override
    public boolean useIteratorsPreferentially() {
        return false;
    }

    private <Cx, Cv> Stream<IdxPref> getPreferences(Keyed<Cx, Cv> prefs, int len, CODEC<Cx> x_codec, CODEC<Cv> r_codec) {
        if (len == 0) {
            return empty();
        }
        int[] idxs = new int[len];
        x_codec.dec(prefs.getCi(), idxs, 0, len);
        if (!x_codec.isIntegrated()) {
            atled(idxs, 0, len);
        }

        if (hasRatings) {
            int[] vs = new int[len];
            r_codec.dec(prefs.getCr(), vs, 0, len);

            return range(0, len).mapToObj(i -> new IdxPref(idxs[i], vs[i]));
        } else {
            return range(0, len).mapToObj(i -> new IdxPref(idxs[i], 1.0));
        }
    }

    private <Cx> IntIterator getIdx(Keyed<Cx, ?> prefs, int len, CODEC<Cx> x_codec) {
        if (len == 0) {
            return IntIterators.EMPTY_ITERATOR;
        }
        int[] idxs = new int[len];
        x_codec.dec(prefs.getCi(), idxs, 0, len);
        if (!x_codec.isIntegrated()) {
            atled(idxs, 0, len);
        }
        return new ArrayIntIterator(idxs);
    }

    private <Cv> DoubleIterator getVs(Keyed<?, Cv> prefs, int len, CODEC<Cv> r_codec) {
        if (len == 0) {
            return DoubleIterators.EMPTY_ITERATOR;
        }
        int[] vsi = new int[len];
        if (hasRatings) {
            r_codec.dec(prefs.getCr(), vsi, 0, len);
        } else {
            Arrays.fill(vsi, 1);
        }
        double[] vsd = new double[len];
        for (int i = 0; i < len; i++) {
            vsd[i] = vsi[i];
        }
        return new ArrayDoubleIterator(vsd);
    }

    public static void main(String[] args) throws IOException {
        String uPath = "/home/saul/recsys/ml1M/users.txt";
        String iPath = "/home/saul/recsys/ml1M/items.txt";
        String pPath = "/home/saul/recsys/ml1M/total.data";

        FastUserIndex<String> users = SimpleFastUserIndex.load(UsersReader.read(uPath, sp));
        FastItemIndex<String> items = SimpleFastItemIndex.load(ItemsReader.read(iPath, sp));

        FastPreferenceData<String, String> sdata = SimpleFastPreferenceData.load(SimpleBinaryPreferencesReader.get().read(pPath, sp, sp), users, items);

        FastPreferenceData<String, String> gdata = new GeneralCODECPreferenceData<>(sdata, users, items, new NullCODEC(), new NullCODEC(), new NullCODEC(), () -> new BinarySearchTree<>(), () -> new BinarySearchTree<>());

        System.out.println("sdata:");
        getUBKNNRecommendation("123", sdata).getItems().forEach(Unchecked.consumer(rec -> {
            System.out.println(rec.v1 + "\t" + rec.v2);
        }));
        System.out.println("gdata:");
        getUBKNNRecommendation("123", gdata).getItems().forEach(Unchecked.consumer(rec -> {
            System.out.println(rec.v1 + "\t" + rec.v2);
        }));
    }

    private static Recommendation<String, String> getUBKNNRecommendation(String user, FastPreferenceData<String, String> prefs) {
        UserSimilarity<String> similarity = new VectorCosineUserSimilarity<>(prefs, 0.5, false);
        UserNeighborhood<String> neighborhood = new TopKUserNeighborhood<>(similarity, 100);
        UserNeighborhoodRecommender<String, String> recommender = new UserNeighborhoodRecommender<>(prefs, neighborhood, 1);

        return recommender.getRecommendation(user, 10);
    }
}
