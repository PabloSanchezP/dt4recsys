package es.uam.eps.tfg.structures.trees.bsearchtrees;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.BinaryNode;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.BinarySearchTreeNode;
import java.io.Serializable;

/***
 * Binary-search-tree class. A binary search tree has a root and elements on the
 * left side (smaller elements) and on the right side (greater elements).
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 * @version 1.0
 */
public class BinarySearchTree<T extends Sorteable<T>> extends BinaryTree<T> implements Serializable {

	/**
	 * Constructor method
	 */
	public BinarySearchTree() {
		super();
	}

	/***
	 * 
	 * @param node
	 *            the new node to add to the tree
	 */
	@Override
	public void insert(T element) {
		BinarySearchTreeNode<T> node = new BinarySearchTreeNode<T>(element);
		BinarySearchTreeNode<T> aux = (BinarySearchTreeNode<T>) this.root;
		BinarySearchTreeNode<T> ref = null;
		while (aux != null) {
			ref = aux;
			int diff = node.getK().sortAgainst(aux.getK());

			if (diff < 0)
				aux = (BinarySearchTreeNode<T>) aux.getLeft();
			else if (diff > 0)
				aux = (BinarySearchTreeNode<T>) aux.getRight();
			else {
				if (this.duplicatesAllowed == true)
					aux = (BinarySearchTreeNode<T>) aux.getLeft();
				else
					return;
			}
		}
		node.setParent(ref);

		/* Empty tree */
		if (ref == null)
			this.root = (BinaryNode<T>) node;
		/* Smaller element */
		else if (node.getK().sortAgainst(ref.getK()) < 0)
			ref.setLeft((BinaryNode<T>) node);
		/* Bigger element */
		else
			ref.setRight((BinaryNode<T>) node);

		this.totalElements++;
	}

	/***
	 * Method to obtain the minimum value of a tree
	 * 
	 * @param node
	 *            the root node
	 * @return the minimum value node
	 */
	public BinaryNode<T> minimum(BinaryNode<T> node) {
		BinaryNode<T> x = node;
		while (x.getLeft() != null) {
			x = x.getLeft();
		}
		return x;
	}

	/***
	 * Method to obtain the maximum value of a tree
	 * 
	 * @param node
	 *            the root node
	 * @return the maximum value node
	 */
	public BinaryNode<T> maximum(BinaryNode<T> node) {
		BinaryNode<T> x = node;
		while (x.getRight() != null) {
			x = x.getRight();
		}
		return x;
	}

	/***
	 * Method to obtain the successor of a node
	 * 
	 * @param node
	 *            the node which we are going to get the successor
	 * @return the successor of node
	 */
	public BinaryNode<T> successor(BinaryNode<T> node) {
		BinaryNode<T> x = node;
		if (x.getRight() != null)
			return minimum(x.getRight());
		BinaryNode<T> y = x.getParent();
		while (y != null && x == y.getRight()) {
			x = y;
			y = y.getParent();
		}
		return y;
	}

	private void transplant(BinaryNode<T> u, BinaryNode<T> v) {
		if (u.getParent() == null)
			this.root = v;
		else if (u == u.getParent().getLeft())
			u.getParent().setLeft(v);
		else
			u.getParent().setRight(v);
		if (v != null)
			v.setParent(u.getParent());
	}

	/***
	 * Method to delete a binary node of the tree
	 * 
	 * @param z
	 *            the binary node to delete
	 */
	@Override
	public void delete(T element) {
		BinaryNode<T> z = nodeSearch(element);
		if (element == null) /* Error control */
			return;
		if (z.getLeft() == null)
			transplant(z, z.getRight());
		else if (z.getRight() == null)
			transplant(z, z.getLeft());
		else {
			BinaryNode<T> y = minimum(z.getRight());
			if (y.getParent() != z) {
				transplant(y, y.getRight());
				y.setRight(z.getRight());
				y.getRight().setParent(y);
			}
			transplant(z, y);
			y.setLeft(z.getLeft());
			y.getLeft().setParent(y);
		}
		this.totalElements--;
	}

	/**
	 * Method to search in the binary tree
	 * 
	 * @param k
	 *            the key that is going to be searched
	 * @return the key
	 */
	@Override
	public T search(T k) {
		BinaryNode<T> x = this.root;
		while (x != null && x.getK().sortAgainst(k) != 0) {
			if (x.getK().sortAgainst(k) > 0)
				x = x.getLeft();
			else
				x = x.getRight();
		}
		if (x == null)
			return null;
		return x.getK();
	}
	
	/***
	 * Same method as before but it returns a node, not an element
	 * @param k
	 * @return
	 */
	private BinaryNode<T> nodeSearch(T k){
		BinaryNode<T> x = this.root;
		while (x != null && x.getK().sortAgainst(k) != 0) {
			if (x.getK().sortAgainst(k) > 0)
				x = x.getLeft();
			else
				x = x.getRight();
		}
		if (x == null)
			return null;
		return x;
	}

	@Override
	public String getStructure() {
		return "BinarySearchTree";
	}

}
