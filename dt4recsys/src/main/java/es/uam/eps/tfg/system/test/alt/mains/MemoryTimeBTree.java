package es.uam.eps.tfg.system.test.alt.mains;



import es.uam.eps.tfg.system.test.alt.RecommenderSystemFactory;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;

/**
 * Main class to see the performance of the BTree by the parameter of t
 * the parameter t of the BTree indicated the number of children that it can have:
 * (2t), the maximum of keys (2t-1) and the minimum keys(t-1) that it stores
 * @author Pablo
 *
 */
public class MemoryTimeBTree {
	private static final long MEGABYTE = 1024L * 1024L;
	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;



	// Variable that indicates the threshold for LCS to consider in its
	// similarity

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	//./datasets/Movielens/u1.base
	public static void main(String[] args) throws Exception {
		int t;
		t = Integer.parseInt(args[0]);
		
		String[] trainfiles = new String[] { 
				"u1.base",
				//"u2.base", 
				//"u3.base",
		/* "./datasets/Movielens/u4.base", "./datasets/Movielens/u5.base", */

		};

		String[] testfiles = new String[] { 
				"u1.test",
				//"u2.test", 
				//"u3.test",
		/* "./datasets/Movielens/u4.test", "./datasets/Movielens/u5.test", */};

		

		// Array of algorithms to be tested

		System.gc();
		System.out
				.println("Structure;trainReadingTotalTime;trainReadingTotalMemory;trainTotalTime;trainTotalMemory;testReadingTotalTime;"
						+ "testReadingTotalMemory;testTotalTime;testTotalMemory");

		Runtime runtime = Runtime.getRuntime();
		// First time-> baseline

		// load data
		long trainReadingTotalTime = 0;
		long trainReadingTotalMemory = 0;
		long trainTotalTime = 0;
		long trainTotalMemory = 0;
		long testReadingTotalTime = 0;
		long testReadingTotalMemory = 0;
		long testTotalTime = 0;
		long testTotalMemory = 0;
		RecommenderSystemIF<Long, Long> dataset = new NeighbourhoodRanking(t, t, t, 100,
				"Cosine Normal", Structures.BTree,
				ReccomendationScheme.UserBased);
		int i = 0;
		for (String fileTrain : trainfiles) {
			/*if (i == 0) { // Baseline to warm up application
				RecommenderSystemIF<Long, Long> dataset2 = new BaselineDatasetMeanBiases(
						BIAS_TYPE.COMBINED);
				loadTrainingDataset(dataset2, fileTrain);
				dataset2.train();
				loadTestDataset(dataset2, testfiles[i], 0.0);
				dataset2.test(0.0);
				dataset2.computeEvaluationBean(4.0, new int[] { 1000 });
				//System.out.println("BaselineFinished for structure: " + args[0]);
			}*/

			System.gc();

			// Reading training file
			long usedMemoryBefore = runtime.totalMemory()
					- runtime.freeMemory();
			long timeBefore = System.currentTimeMillis();

			// READ TRAIN
			RecommenderSystemFactory.loadTrainingDataset(dataset, fileTrain,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);

			long usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			long timeAfter = System.currentTimeMillis();
			trainReadingTotalMemory += usedMemoryAfter - usedMemoryBefore;
			trainReadingTotalTime += timeAfter - timeBefore;
			// Finishing reading train. Now we train
			 System.gc();

			// Train
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();
			dataset.train(false);
			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();

			// Obtain memory and time of train
			trainTotalTime += timeAfter - timeBefore;
			trainTotalMemory += usedMemoryAfter - usedMemoryBefore;

			System.gc();

			// Read test
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();

			RecommenderSystemFactory.loadTestDataset(dataset, testfiles[i], 0.0,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);
			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();

			testReadingTotalMemory += usedMemoryAfter - usedMemoryBefore;
			testReadingTotalTime += timeAfter - timeBefore;
			System.gc();

			int[] threshold = new int[] { 1000 };
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();

			// Test time
			dataset.test(0.0);
			
			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();
			testTotalMemory += usedMemoryAfter - usedMemoryBefore;
			testTotalTime += timeAfter - timeBefore;
			
			
			System.gc();
			
			usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();
			timeBefore = System.currentTimeMillis();
			//COmpute evBean
			dataset.computeEvaluationBean(4.0, threshold);
		
			usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
			timeAfter = System.currentTimeMillis();

			testTotalMemory += usedMemoryAfter - usedMemoryBefore;
			testTotalTime += timeAfter - timeBefore;

			i++;
		}

		/*
		 * System.out.println(structure+";"
		 * +(double)trainReadingTotalTime+";"+trainReadingTotalMemory
		 * +";"+trainTotalTime+";"+trainTotalMemory
		 * +";"+testReadingTotalTime+";"+testReadingTotalMemory
		 * +";"+testTotalTime+";"+testTotalMemory);
		 */

		double d = trainfiles.length;
		System.out.println(args[0] + ";"
				+ String.format("%.2f", ((double) trainReadingTotalTime / (d*1000)))
				+ ";"
				+ String.format("%.2f", ((double) trainReadingTotalMemory / (d*MEGABYTE)))
				+ ";" + String.format("%.2f", ((double) trainTotalTime / (d*1000)))
				+ ";" + String.format("%.2f", ((double) trainTotalMemory / (d*MEGABYTE)))
				+ ";"
				+ String.format("%.2f", ((double) testReadingTotalTime / (d*1000)))
				+ ";"
				+ String.format("%.2f", ((double) testReadingTotalMemory / (d*MEGABYTE)))
				+ ";" + String.format("%.2f", ((double) testTotalTime / (d*1000)))
				+ ";" + String.format("%.2f", ((double) testTotalMemory / (d*MEGABYTE))));

	}

	

	
}
