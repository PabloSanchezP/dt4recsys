package es.uam.eps.tfg.structures.ranksys;

import es.saulvargas.recsys2015.Utils;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.ParseStructureFactory;
import es.uam.eps.tfg.structures.basic.DoubleLinkedList;
import es.uam.eps.tfg.structures.basic.HashTable;
import es.uam.eps.tfg.structures.trees.BTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.BinarySearchTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.OrderStatisticTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.RedBlackTree;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import it.unimi.dsi.fastutil.ints.IntArrays;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.Boolean.parseBoolean;
import java.io.IOException;
import java.io.File;
import static java.lang.Integer.parseInt;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import org.jooq.lambda.Unchecked;
import java.util.stream.IntStream;
import static java.util.stream.DoubleStream.of;
import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.compression.codecs.CODEC;
import org.ranksys.compression.codecs.NullCODEC;
import org.ranksys.compression.codecs.dsi.EliasFanoBitStreamCODEC;
import org.ranksys.compression.codecs.dsi.FixedLengthBitStreamCODEC;
import org.ranksys.compression.codecs.dsi.GammaBitStreamCODEC;
import org.ranksys.compression.codecs.dsi.RiceBitStreamCODEC;
import org.ranksys.compression.codecs.dsi.ZetaBitStreamCODEC;
import org.ranksys.compression.codecs.lemire.FORVBCODEC;
import org.ranksys.compression.codecs.lemire.FastPFORVBCODEC;
import org.ranksys.compression.codecs.lemire.IntegratedFORVBCODEC;
import org.ranksys.compression.codecs.lemire.IntegratedVByteCODEC;
import org.ranksys.compression.codecs.lemire.NewPFDVBCODEC;
import org.ranksys.compression.codecs.lemire.OptPFDVBCODEC;
import org.ranksys.compression.codecs.lemire.Simple16CODEC;
import org.ranksys.compression.codecs.lemire.VByteCODEC;
import org.ranksys.compression.preferences.BinaryCODECPreferenceData;
import org.ranksys.compression.preferences.RatingCODECPreferenceData;
import org.ranksys.core.util.tuples.Tuple2io;
import org.ranksys.formats.index.ItemsReader;
import org.ranksys.formats.index.UsersReader;
import org.ranksys.formats.parsing.Parser;
import static org.ranksys.formats.parsing.Parsers.ip;
import static org.ranksys.formats.parsing.Parsers.sp;
import org.ranksys.formats.preference.CompressibleBinaryPreferencesFormat;
import org.ranksys.formats.preference.SimpleBinaryPreferencesReader;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.preference.CompressibleRatingPreferencesFormat;

public class Experiment {

    private static boolean CALL_GC = false;

    public static void main(String[] args) throws Exception {
        //args = new String[]{"3", "hashtable_list", "6040", "3706", "23", "ml1M/", "ml1M", "ifor", "fixed", "4", "urv_1000", "28351"};

        // step structure nusers nitems auxn path dataset idxcodec vcodec [n funname seed]
        int step = Integer.parseInt(args[0]);

        Structures structure = ParseStructureFactory.parseStructure(args[1]);
        int nusers = Integer.parseInt(args[2]);
        int nitems = Integer.parseInt(args[3]);
        int auxn = Integer.parseInt(args[4]);

        String path = args[5];
        String dataset = args[6];
        String idxCodec = args[7];
        String vCodec = args[8];

        switch (step) {
            case 1: {
                // es.saulvargas.recsys2015.Generate
                if ((dataset.equals("msd") || dataset.equals("hetrec_lastfm")) && !vCodec.equals("null")) {
                    System.err.println("does not apply here: implicit data");
                    return;
                }
                if (vCodec.startsWith("i")) {
                    System.err.println("integrated codec only for ids");
                    return;
                }

                switch (dataset) {
                    case "ml1M":
                    case "ml10M":
                    case "ml20M":
                    case "netflix":
                    case "ymusic":
                    case "yahoo":
                        store(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, ip, ip);
                        break;
                    case "msd":
                    case "hetrec_lastfm":
                    default:
                        store(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, sp, sp);
                        break;
                }
            }
            break;

            case 2: {
                // es.saulvargas.recsys2015.Benchmark
                int n = parseInt(args[9]);
                String funName = args[10];
                long seed = parseLong(args[11]);

                if ((dataset.equals("msd") || dataset.equals("hetrec_lastfm")) && !vCodec.equals("null")) {
                    System.err.println("does not apply here: implicit data");
                    return;
                }
                if (vCodec.startsWith("i")) {
                    System.err.println("integrated codec only for ids");
                    return;
                }

                test(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, n, funName, seed);
            }
            break;

            case 3: {
                // test from memory
                int n = parseInt(args[9]);
                String funName = args[10];
                long seed = parseLong(args[11]);
                try {
                    boolean callgc = parseBoolean(args[12]);
                    CALL_GC = callgc;
                } catch (Exception e) {
                }

                if ((dataset.equals("msd") || dataset.equals("hetrec_lastfm")) && !vCodec.equals("null")) {
                    System.err.println("does not apply here: implicit data");
                    return;
                }
                if (vCodec.startsWith("i")) {
                    System.err.println("integrated codec only for ids");
                    return;
                }

                switch (dataset) {
                    case "ml1M":
                    case "ml10M":
                    case "ml20M":
                    case "netflix":
                    case "ymusic":
                    case "yahoo":
                        testFromMemory(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, ip, ip, n, funName, seed);
                        break;
                    case "msd":
                    case "hetrec_lastfm":
                    default:
                        testFromMemory(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, sp, sp, n, funName, seed);
                        break;
                }
            }
            break;

            case 4: {
                // test from memory, but loading data from a compressed file
                String idxCodec2 = args[9];
                String vCodec2 = args[10];

                int n = parseInt(args[11]);
                String funName = args[12];
                long seed = parseLong(args[13]);
                try {
                    boolean callgc = parseBoolean(args[14]);
                    CALL_GC = callgc;
                } catch (Exception e) {
                }

                if ((dataset.equals("msd") || dataset.equals("hetrec_lastfm")) && !vCodec.equals("null")) {
                    System.err.println("does not apply here: implicit data");
                    return;
                }
                if (vCodec.startsWith("i")) {
                    System.err.println("integrated codec only for ids");
                    return;
                }

                switch (dataset) {
                    case "ml1M":
                    case "ml10M":
                    case "ml20M":
                    case "netflix":
                    case "ymusic":
                    case "yahoo":
                        testFromLoadedSimpleStructure(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, ip, ip, idxCodec2, vCodec2, n, funName, seed);
                        break;
                    case "msd":
                    case "hetrec_lastfm":
                    default:
                        testFromLoadedSimpleStructure(structure, nusers, nitems, auxn, path, dataset, idxCodec, vCodec, sp, sp, idxCodec2, vCodec2, n, funName, seed);
                        break;
                }
            }
            break;
        }
    }

    /**
     * Get path to serialized PreferenceData instances.
     *
     * @param path base path
     * @param dataset name of the dataset
     * @param idxCodec codec of identifiers
     * @param vCodec codec of ratings
     * @return path of the preference data serialized file
     */
    public static String getPath(String path, String dataset, String idxCodec, String vCodec) {
        return path + "/preference-data/" + idxCodec + "-" + vCodec + ".obj.gz";
    }

    /**
     * Returns a codec by name.
     *
     * @param name name of the codec
     * @param fixedLength number of bits for fixed-length coding
     * @return codec
     */
    public static CODEC<?> getCodec(String name, int fixedLength) {
        int k = -1;
        if (name.contains("_")) {
            String[] tokens = name.split("_");
            name = tokens[0];
            k = parseInt(tokens[1]);
        }

        switch (name) {
            case "null":
                return new NullCODEC();
            case "gamma":
                return new GammaBitStreamCODEC();
            case "zeta":
                return new ZetaBitStreamCODEC(k < 0 ? 3 : k);
            case "rice":
                return new RiceBitStreamCODEC();
            case "vbyte":
                return new VByteCODEC();
            case "ivbyte":
                return new IntegratedVByteCODEC();
            case "for":
                return new FORVBCODEC();
            case "ifor":
                return new IntegratedFORVBCODEC();
            case "simple":
                return new Simple16CODEC();
            case "optpfd":
                return new OptPFDVBCODEC();
            case "newpfd":
                return new NewPFDVBCODEC();
            case "fastpfor":
                return new FastPFORVBCODEC();
            case "ief":
                return new EliasFanoBitStreamCODEC();
            case "fixed":
                return new FixedLengthBitStreamCODEC(k < 0 ? fixedLength : k);
//            case "gvbyte":
//                return new GroupVByteCODEC();
            default:
                System.err.println("I don't know what " + name + " is :-(");
                System.exit(-1);
                return null;
        }
    }

    /**
     * Get the bits required for identifiers and ratings for identifiers and
     * ratings.
     *
     * @param path base path
     * @param dataset name of dataset
     * @return an array of number of bits for user identifiers, item identifiers
     * and ratings
     * @throws IOException when IO error
     */
    public static int[] getFixedLength(String path, String dataset) throws IOException {
        FastUserIndex<String> users = SimpleFastUserIndex.load(UsersReader.read(path + "/users.txt", sp));
        FastItemIndex<String> items = SimpleFastItemIndex.load(ItemsReader.read(path + "/items.txt", sp));

        int uFixedLength = 32 - Integer.numberOfLeadingZeros(users.numUsers() - 1);
        int iFixedLength = 32 - Integer.numberOfLeadingZeros(items.numItems() - 1);
        int vFixedLength;
        switch (dataset) {
            case "msd":
            case "hetrec_lastfm":
                vFixedLength = 1;
                break;
            case "ml10M":
            case "ml20M":
                vFixedLength = 4;
                break;
            case "ml1M":
            case "netflix":
            case "ymusic":
            case "yahoo":
                vFixedLength = 3;
                break;
            default:
                vFixedLength = 32;
        }

        return new int[]{uFixedLength, iFixedLength, vFixedLength};
    }

    private static String getStructurePrefix(Structures structure) {
        return (structure == null ? "" : structure.toString());
    }

    private static <U, I> void store(Structures structure, int nusers, int nitems, int auxn, String path,
            String dataset, String idxCodec, String vCodec, Parser<U> up, Parser<I> ip) throws IOException {

        FastUserIndex<U> users = SimpleFastUserIndex.load(UsersReader.read(path + "/users.txt", up));
        FastItemIndex<I> items = SimpleFastItemIndex.load(ItemsReader.read(path + "/items.txt", ip));

        String dataPath = path + "/ratings.data";
        String uDataPath = path + "/ratings" + getStructurePrefix(structure) + ".u";
        String iDataPath = path + "/ratings" + getStructurePrefix(structure) + ".i";

        Function<Structures, GeneralStructure> sf = Unchecked.function(s -> {
            switch (s) {
                case BTree:
                    return new BTree<>();
                case BinarySearchTree:
                    return new BinarySearchTree<>();
                case DoubleLinkedList:
                    return new DoubleLinkedList<>();
                case HashTable_List:
                    return new HashTable<>(Structures.DoubleLinkedList, auxn);
                case HashTable_RedBlack:
                    return new HashTable<>(Structures.RedBlackTree, auxn);
                case OrderStatisticTree:
                    return new OrderStatisticTree<>();
                case RedBlackTree:
                    return new RedBlackTree<>();
                default:
                    throw new AssertionError();
            }
        });
        Function<CODEC<?>[], FastPreferenceData<U, I>> cdf = Unchecked.function(cds -> {
            switch (dataset) {
                case "msd":
                case "hetrec_lastfm":
                    CompressibleBinaryPreferencesFormat binaryFormat = CompressibleBinaryPreferencesFormat.get();

                    if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
                        Stream<Tuple3<U, I, Double>> tuples = SimpleBinaryPreferencesReader.get().read(dataPath, up, ip);
                        FastPreferenceData<U, I> data = null;
                        if (structure == null) {
                            data = SimpleFastPreferenceData.load(tuples, users, items);
                        } else {
                            data = GeneralPreferenceData.load(tuples, users, items, structure, nusers, nitems, auxn);
                        }
                        binaryFormat.write(data, uDataPath, iDataPath);
                    }

                    Stream<Tuple2io<int[]>> ulb = binaryFormat.read(uDataPath);
                    Stream<Tuple2io<int[]>> ilb = binaryFormat.read(iDataPath);

                    if (structure == null) {
                        return new BinaryCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1]);
                    } else {
                        return new GeneralCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1], cds[2], () -> sf.apply(structure), () -> sf.apply(structure), false);
                    }
                case "ml1M":
                case "ml10M":
                case "ml20M":
                case "netflix":
                case "ymusic":
                case "yahoo":
                default:
                    CompressibleRatingPreferencesFormat ratingFormat = CompressibleRatingPreferencesFormat.get();

                    if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
                        Stream<Tuple3<U, I, Double>> tuples = SimpleRatingPreferencesReader.get().read(dataPath, up, ip);
                        FastPreferenceData<U, I> data = null;
                        if (structure == null) {
                            data = SimpleFastPreferenceData.load(tuples, users, items);
                        } else {
                            data = GeneralPreferenceData.load(tuples, users, items, structure, nusers, nitems, auxn);
                        }
                        ratingFormat.write(data, uDataPath, iDataPath);
                    }

                    Stream<Tuple2io<int[][]>> ulr = ratingFormat.read(uDataPath);
                    Stream<Tuple2io<int[][]>> ilr = ratingFormat.read(iDataPath);

                    if (structure == null) {
                        return new RatingCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2]);
                    } else {
                        return new GeneralCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2], () -> sf.apply(structure), () -> sf.apply(structure));
                    }
            }
        });

        long time0 = System.nanoTime();
        int[] lens = getFixedLength(path, dataset);
        CODEC cd_uidxs = getCodec(idxCodec, lens[0]);
        CODEC cd_iidxs = getCodec(idxCodec, lens[1]);
        CODEC cd_vs = getCodec(vCodec, lens[2]);
        FastPreferenceData<U, I> preferences = cdf.apply(new CODEC[]{cd_uidxs, cd_iidxs, cd_vs});
        double loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
        idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
        System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);

        String fields = dataset + "\t" + idxCodec + "\t" + vCodec;
        System.out.println(fields + "\tus\t" + cd_uidxs.stats()[1]);
        System.out.println(fields + "\tis\t" + cd_iidxs.stats()[1]);
        System.out.println(fields + "\tvs\t" + cd_vs.stats()[1]);

        Utils.serialize(preferences, getPath(path, dataset, idxCodec, vCodec));
    }

    public static <U, I> void test(Structures structure, int nusers, int nitems, int auxn, String path, String dataset,
            String idxCodec, String vCodec, int n, String funName, long seed) throws Exception {
        long time0 = System.nanoTime();
        idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
        FastPreferenceData<U, I> preferences = Utils.deserialize(getPath(path, dataset, idxCodec, vCodec));
        double loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
        System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);

        Random rnd = new Random(seed);
        int[] targetUsers;
        if (funName.contains("_")) {
            int N = parseInt(funName.split("_")[1]);
            funName = funName.split("_")[0];
            targetUsers = rnd.ints(0, preferences.numUsers()).distinct().limit(N).toArray();
        } else {
            targetUsers = preferences.getAllUidx().toArray();
        }
        IntArrays.shuffle(targetUsers, rnd);

        Consumer<FastPreferenceData<U, I>> fun;
        switch (funName) {
            case "urv":
                fun = prefs -> {
                    UserSimilarity<U> us = new VectorCosineUserSimilarity<>(prefs, 0.5, true);
                    UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
                    UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "urs":
                fun = prefs -> {
                    UserSimilarity<U> us = new SetCosineUserSimilarity<>(prefs, 0.5, true);
                    UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
                    UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "irv":
                fun = prefs -> {
                    ItemSimilarity<I> is = new VectorCosineItemSimilarity<>(prefs, 0.5, true);
                    ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
                    ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "irs":
                fun = prefs -> {
                    ItemSimilarity<I> is = new SetCosineItemSimilarity<>(prefs, 0.5, true);
                    ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
                    ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            default:
                fun = null;
                break;
        }

        double[] times = tiktok(fun, preferences, n);

        String fields = dataset + "\t" + idxCodec + "\t" + vCodec + "\t" + funName;
        for (double t : times) {
            System.out.println(fields + "\tt\t" + t);
        }
        System.out.println(fields + "\tlt\t" + times[times.length - 1]);
        System.out.println(fields + "\tat\t" + of(times).average().getAsDouble());
        System.out.println(fields + "\tmt\t" + of(times).min().getAsDouble());
    }

    public static <U, I> void testFromMemory(Structures structure, int nusers, int nitems, int auxn, String path,
            String dataset, String idxCodec, String vCodec, Parser<U> up, Parser<I> ip,
            int n, String funName, long seed) throws Exception {

        FastUserIndex<U> users = SimpleFastUserIndex.load(UsersReader.read(path + "/users.txt", up));
        FastItemIndex<I> items = SimpleFastItemIndex.load(ItemsReader.read(path + "/items.txt", ip));

        String dataPath = path + "/ratings.data";
        String uDataPath = path + "/ratings" + getStructurePrefix(structure) + ".u";
        String iDataPath = path + "/ratings" + getStructurePrefix(structure) + ".i";

        Function<Structures, GeneralStructure> sf = Unchecked.function(s -> {
            switch (s) {
                case BTree:
                    return new BTree<>();
                case BinarySearchTree:
                    return new BinarySearchTree<>();
                case DoubleLinkedList:
                    return new DoubleLinkedList<>();
                case HashTable_List:
                    return new HashTable<>(Structures.DoubleLinkedList, auxn);
                case HashTable_RedBlack:
                    return new HashTable<>(Structures.RedBlackTree, auxn);
                case OrderStatisticTree:
                    return new OrderStatisticTree<>();
                case RedBlackTree:
                    return new RedBlackTree<>();
                default:
                    throw new AssertionError();
            }
        });
        Function<CODEC<?>[], FastPreferenceData<U, I>> cdf = Unchecked.function(cds -> {
            switch (dataset) {
                case "msd":
                case "hetrec_lastfm":
                    CompressibleBinaryPreferencesFormat binaryFormat = CompressibleBinaryPreferencesFormat.get();

                    if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
                        Stream<Tuple3<U, I, Double>> tuples = SimpleBinaryPreferencesReader.get().read(dataPath, up, ip);
                        FastPreferenceData<U, I> data = null;
                        if (structure == null) {
                            data = SimpleFastPreferenceData.load(tuples, users, items);
                        } else {
                            data = GeneralPreferenceData.load(tuples, users, items, structure, nusers, nitems, auxn);
                        }
                        binaryFormat.write(data, uDataPath, iDataPath);
                    }

                    Stream<Tuple2io<int[]>> ulb = binaryFormat.read(uDataPath);
                    Stream<Tuple2io<int[]>> ilb = binaryFormat.read(iDataPath);

                    if (structure == null) {
                        return new BinaryCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1]);
                    } else {
                        return new GeneralCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1], cds[2], () -> sf.apply(structure), () -> sf.apply(structure), false);
                    }
                case "ml1M":
                case "ml10M":
                case "ml20M":
                case "netflix":
                case "ymusic":
                case "yahoo":
                default:
                    CompressibleRatingPreferencesFormat ratingFormat = CompressibleRatingPreferencesFormat.get();

                    if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
                        Stream<Tuple3<U, I, Double>> tuples = SimpleRatingPreferencesReader.get().read(dataPath, up, ip);
                        FastPreferenceData<U, I> data = null;
                        if (structure == null) {
                            data = SimpleFastPreferenceData.load(tuples, users, items);
                        } else {
                            data = GeneralPreferenceData.load(tuples, users, items, structure, nusers, nitems, auxn);
                        }
                        ratingFormat.write(data, uDataPath, iDataPath);
                    }

                    Stream<Tuple2io<int[][]>> ulr = ratingFormat.read(uDataPath);
                    Stream<Tuple2io<int[][]>> ilr = ratingFormat.read(iDataPath);

                    if (structure == null) {
                        return new RatingCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2]);
                    } else {
                        return new GeneralCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2], () -> sf.apply(structure), () -> sf.apply(structure));
                    }
            }
        });

        Runtime runtime = Runtime.getRuntime();

        long time0 = System.nanoTime();
        long memory1, memory2, memoryTotal = 0;
        if (CALL_GC) {
            System.gc();
        }
        memory1 = runtime.totalMemory() - runtime.freeMemory();
        int[] lens = getFixedLength(path, dataset);
        CODEC cd_uidxs = getCodec(idxCodec, lens[0]);
        CODEC cd_iidxs = getCodec(idxCodec, lens[1]);
        CODEC cd_vs = getCodec(vCodec, lens[2]);
        FastPreferenceData<U, I> preferences = cdf.apply(new CODEC[]{cd_uidxs, cd_iidxs, cd_vs});
        double loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
        if (CALL_GC) {
            System.gc();
        }
        memory2 = runtime.totalMemory() - runtime.freeMemory();
        idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
        //System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);
        long loadingMemory = memory2 - memory1;
        //System.err.println("loadingmem " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingMemory);
        memoryTotal += loadingMemory;

        String fields = dataset + "\t" + idxCodec + "\t" + vCodec + "\tnone";
        System.out.println(fields + "\tloadtime\t" + loadingTime);
        System.out.println(fields + "\tloadmem\t" + loadingMemory);
        System.out.println(fields + "\tus\t" + cd_uidxs.stats()[1]);
        System.out.println(fields + "\tis\t" + cd_iidxs.stats()[1]);
        System.out.println(fields + "\tvs\t" + cd_vs.stats()[1]);

        //Utils.serialize(preferences, getPath(path, dataset, idxCodec, vCodec));
        //time0 = System.nanoTime();
        //idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
        //FastPreferenceData<U, I> preferences = Utils.deserialize(getPath(path, dataset, idxCodec, vCodec));
        //loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
        //System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);
        Random rnd = new Random(seed);
        int[] targetUsers;
        if (funName.contains("_")) {
            int N = parseInt(funName.split("_")[1]);
            funName = funName.split("_")[0];
            targetUsers = rnd.ints(0, preferences.numUsers()).distinct().limit(N).toArray();
        } else {
            targetUsers = preferences.getAllUidx().toArray();
        }
        IntArrays.shuffle(targetUsers, rnd);

        Consumer<FastPreferenceData<U, I>> fun;
        switch (funName) {
            case "urv":
                fun = prefs -> {
                    UserSimilarity<U> us = new VectorCosineUserSimilarity<>(prefs, 0.5, true);
                    UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
                    UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "urs":
                fun = prefs -> {
                    UserSimilarity<U> us = new SetCosineUserSimilarity<>(prefs, 0.5, true);
                    UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
                    UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "irv":
                fun = prefs -> {
                    ItemSimilarity<I> is = new VectorCosineItemSimilarity<>(prefs, 0.5, true);
                    ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
                    ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "irs":
                fun = prefs -> {
                    ItemSimilarity<I> is = new SetCosineItemSimilarity<>(prefs, 0.5, true);
                    ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
                    ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            default:
                fun = null;
                break;
        }

        double[][] timesAndMem = tiktok(fun, preferences, n, runtime);

        fields = dataset + "\t" + idxCodec + "\t" + vCodec + "\t" + funName;
        for (double t : timesAndMem[0]) {
            System.out.println(fields + "\tt\t" + t);
        }
        for (double m : timesAndMem[1]) {
            System.out.println(fields + "\tm\t" + m);
            System.out.println(fields + "\ttotalmem\t" + (memoryTotal + m));
        }
        System.out.println(fields + "\tlastt\t" + timesAndMem[0][n - 1]);
        System.out.println(fields + "\tavgt\t" + of(timesAndMem[0]).average().getAsDouble());
        System.out.println(fields + "\tmint\t" + of(timesAndMem[0]).min().getAsDouble());
        System.out.println(fields + "\tavgm\t" + of(timesAndMem[1]).average().getAsDouble());
        System.out.println(fields + "\tminm\t" + of(timesAndMem[1]).min().getAsDouble());
        System.out.println(fields + "\tmaxm\t" + of(timesAndMem[1]).max().getAsDouble());
    }

    public static <U, I> void testFromLoadedSimpleStructure(Structures structure, int nusers, int nitems, int auxn, String path,
            String dataset, String idxCodec, String vCodec, Parser<U> up, Parser<I> ip,
            String idxCodec2, String vCodec2,
            int n, String funName, long seed) throws Exception {

        FastUserIndex<U> users = SimpleFastUserIndex.load(UsersReader.read(path + "/users.txt", up));
        FastItemIndex<I> items = SimpleFastItemIndex.load(ItemsReader.read(path + "/items.txt", ip));

        String dataPath = path + "/ratings.data";
        String uDataPath = path + "/ratings" + getStructurePrefix(structure) + ".u";
        String iDataPath = path + "/ratings" + getStructurePrefix(structure) + ".i";

        Function<Structures, GeneralStructure> sf = Unchecked.function(s -> {
            switch (s) {
                case BTree:
                    return new BTree<>();
                case BinarySearchTree:
                    return new BinarySearchTree<>();
                case DoubleLinkedList:
                    return new DoubleLinkedList<>();
                case HashTable_List:
                    return new HashTable<>(Structures.DoubleLinkedList, auxn);
                case HashTable_RedBlack:
                    return new HashTable<>(Structures.RedBlackTree, auxn);
                case OrderStatisticTree:
                    return new OrderStatisticTree<>();
                case RedBlackTree:
                    return new RedBlackTree<>();
                default:
                    throw new AssertionError();
            }
        });
        Function<CODEC<?>[], FastPreferenceData<U, I>> cdf = Unchecked.function(cds -> {
            switch (dataset) {
                case "msd":
                case "hetrec_lastfm":
                    CompressibleBinaryPreferencesFormat binaryFormat = CompressibleBinaryPreferencesFormat.get();

                    if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
                        FastPreferenceData<U, I> preferencesFromFile = Utils.deserialize(getPath(path, dataset, idxCodec2, vCodec2));
                        Stream<Tuple3<U, I, Double>> tuples = preferencesFromFile.getAllUsers().flatMap(u -> {
                            return preferencesFromFile.getUidxPreferences(preferencesFromFile.user2uidx(u)).map(pref -> {
                                return Tuple.tuple(u, preferencesFromFile.iidx2item(pref.v1()), pref.v2());
                            });
                        });
                        FastPreferenceData<U, I> data = null;
                        if (structure == null) {
                            data = SimpleFastPreferenceData.load(tuples, users, items);
                        } else {
                            data = GeneralPreferenceData.load(tuples, users, items, structure, nusers, nitems, auxn);
                        }
                        binaryFormat.write(data, uDataPath, iDataPath);
                    }

                    Stream<Tuple2io<int[]>> ulb = binaryFormat.read(uDataPath);
                    Stream<Tuple2io<int[]>> ilb = binaryFormat.read(iDataPath);

                    if (structure == null) {
                        return new BinaryCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1]);
                    } else {
                        return new GeneralCODECPreferenceData(ulb, ilb, users, items, cds[0], cds[1], cds[2], () -> sf.apply(structure), () -> sf.apply(structure), false);
                    }
                case "ml1M":
                case "ml10M":
                case "ml20M":
                case "netflix":
                case "ymusic":
                case "yahoo":
                default:
                    CompressibleRatingPreferencesFormat ratingFormat = CompressibleRatingPreferencesFormat.get();

                    if (!new File(uDataPath).exists() || !new File(iDataPath).exists()) {
                        FastPreferenceData<U, I> preferencesFromFile = Utils.deserialize(getPath(path, dataset, idxCodec2, vCodec2));
                        Stream<Tuple3<U, I, Double>> tuples = preferencesFromFile.getAllUsers().flatMap(u -> {
                            return preferencesFromFile.getUidxPreferences(preferencesFromFile.user2uidx(u)).map(pref -> {
                                return Tuple.tuple(u, preferencesFromFile.iidx2item(pref.v1()), pref.v2());
                            });
                        });
                        FastPreferenceData<U, I> data = null;
                        if (structure == null) {
                            data = SimpleFastPreferenceData.load(tuples, users, items);
                        } else {
                            data = GeneralPreferenceData.load(tuples, users, items, structure, nusers, nitems, auxn);
                        }
                        ratingFormat.write(data, uDataPath, iDataPath);
                    }

                    Stream<Tuple2io<int[][]>> ulr = ratingFormat.read(uDataPath);
                    Stream<Tuple2io<int[][]>> ilr = ratingFormat.read(iDataPath);

                    if (structure == null) {
                        return new RatingCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2]);
                    } else {
                        return new GeneralCODECPreferenceData(ulr, ilr, users, items, cds[0], cds[1], cds[2], () -> sf.apply(structure), () -> sf.apply(structure));
                    }
            }
        });

        Runtime runtime = Runtime.getRuntime();

        long time0 = System.nanoTime();
        long memory1, memory2, memoryTotal = 0;
        if (CALL_GC) {
            System.gc();
        }
        memory1 = runtime.totalMemory() - runtime.freeMemory();
        int[] lens = getFixedLength(path, dataset);
        CODEC cd_uidxs = getCodec(idxCodec, lens[0]);
        CODEC cd_iidxs = getCodec(idxCodec, lens[1]);
        CODEC cd_vs = getCodec(vCodec, lens[2]);
        FastPreferenceData<U, I> preferences = cdf.apply(new CODEC[]{cd_uidxs, cd_iidxs, cd_vs});
        double loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
        if (CALL_GC) {
            System.gc();
        }
        memory2 = runtime.totalMemory() - runtime.freeMemory();
        idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
        //System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);
        long loadingMemory = memory2 - memory1;
        //System.err.println("loadingmem " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingMemory);
        memoryTotal += loadingMemory;

        String fields = dataset + "\t" + idxCodec + "\t" + vCodec + "\tnone";
        System.out.println(fields + "\tloadtime\t" + loadingTime);
        System.out.println(fields + "\tloadmem\t" + loadingMemory);
        System.out.println(fields + "\tus\t" + cd_uidxs.stats()[1]);
        System.out.println(fields + "\tis\t" + cd_iidxs.stats()[1]);
        System.out.println(fields + "\tvs\t" + cd_vs.stats()[1]);

        //Utils.serialize(preferences, getPath(path, dataset, idxCodec, vCodec));
        //time0 = System.nanoTime();
        //idxCodec = getStructurePrefix(structure) + "__" + idxCodec;
        //FastPreferenceData<U, I> preferences = Utils.deserialize(getPath(path, dataset, idxCodec, vCodec));
        //loadingTime = (System.nanoTime() - time0) / 1_000_000_000.0;
        //System.err.println("loaded " + dataset + " with " + idxCodec + "+" + vCodec + ": " + loadingTime);
        Random rnd = new Random(seed);
        int[] targetUsers;
        if (funName.contains("_")) {
            int N = parseInt(funName.split("_")[1]);
            funName = funName.split("_")[0];
            targetUsers = rnd.ints(0, preferences.numUsers()).distinct().limit(N).toArray();
        } else {
            targetUsers = preferences.getAllUidx().toArray();
        }
        IntArrays.shuffle(targetUsers, rnd);

        Consumer<FastPreferenceData<U, I>> fun;
        switch (funName) {
            case "urv":
                fun = prefs -> {
                    UserSimilarity<U> us = new VectorCosineUserSimilarity<>(prefs, 0.5, true);
                    UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
                    UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "urs":
                fun = prefs -> {
                    UserSimilarity<U> us = new SetCosineUserSimilarity<>(prefs, 0.5, true);
                    UserNeighborhood<U> un = new TopKUserNeighborhood<>(us, 100);
                    UserNeighborhoodRecommender<U, I> rec = new UserNeighborhoodRecommender<>(prefs, un, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "irv":
                fun = prefs -> {
                    ItemSimilarity<I> is = new VectorCosineItemSimilarity<>(prefs, 0.5, true);
                    ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
                    ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            case "irs":
                fun = prefs -> {
                    ItemSimilarity<I> is = new SetCosineItemSimilarity<>(prefs, 0.5, true);
                    ItemNeighborhood<I> in = new CachedItemNeighborhood<>(new TopKItemNeighborhood<>(is, 100));
                    ItemNeighborhoodRecommender<U, I> rec = new ItemNeighborhoodRecommender<>(prefs, in, 1);
                    IntStream.of(targetUsers).parallel().forEach(user -> rec.getRecommendation(user, 100));
                };
                break;
            default:
                fun = null;
                break;
        }

        double[][] timesAndMem = tiktok(fun, preferences, n, runtime);

        fields = dataset + "\t" + idxCodec + "\t" + vCodec + "\t" + funName;
        for (double t : timesAndMem[0]) {
            System.out.println(fields + "\tt\t" + t);
        }
        for (double m : timesAndMem[1]) {
            System.out.println(fields + "\tm\t" + m);
            System.out.println(fields + "\ttotalmem\t" + (memoryTotal + m));
        }
        System.out.println(fields + "\tlastt\t" + timesAndMem[0][n - 1]);
        System.out.println(fields + "\tavgt\t" + of(timesAndMem[0]).average().getAsDouble());
        System.out.println(fields + "\tmint\t" + of(timesAndMem[0]).min().getAsDouble());
        System.out.println(fields + "\tavgm\t" + of(timesAndMem[1]).average().getAsDouble());
        System.out.println(fields + "\tminm\t" + of(timesAndMem[1]).min().getAsDouble());
        System.out.println(fields + "\tmaxm\t" + of(timesAndMem[1]).max().getAsDouble());
    }

    private static <T> double[] tiktok(Consumer<T> fun, T t, int n) {
        double[] times = new double[n];

        for (int i = 0; i < n; i++) {
            long time0 = System.nanoTime();
            fun.accept(t);
            times[i] = (System.nanoTime() - time0) / 1_000_000_000.0;
        }

        return times;
    }

    private static <T> double[][] tiktok(Consumer<T> fun, T t, int n, Runtime runtime) {
        double[][] timesAndMem = new double[2][n];

        for (int i = 0; i < n; i++) {
            long time0 = System.nanoTime();
            long memory1, memory2;
            if (CALL_GC) {
                System.gc();
            }
            memory1 = runtime.totalMemory() - runtime.freeMemory();
            fun.accept(t);
            if (CALL_GC) {
                System.gc();
            }
            memory2 = runtime.totalMemory() - runtime.freeMemory();
            long trainingMemory = memory2 - memory1;
            timesAndMem[0][i] = (System.nanoTime() - time0) / 1_000_000_000.0;
            timesAndMem[1][i] = trainingMemory;
        }

        return timesAndMem;
    }
}
