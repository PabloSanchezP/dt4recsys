package es.uam.eps.tfg.structures.trees.bsearchtrees;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.BinaryNode;
import java.io.Serializable;

/***
 * Class of binary Tree. A binary tree has a root and 3 methods to explore the
 * tree
 * 
 * -Inorder.
 * 
 * -Postorder.
 * 
 * -Preorder.
 * 
 * Every method of exploring the tree it is formed by 2 method. The first one,
 * public, and the method that starts in the root, and the recursive one.
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 * @version 1.0
 */
public abstract class BinaryTree<T extends Sorteable<T>> implements
		GeneralStructure<T>, Serializable {
	protected BinaryNode<T> root;
	protected int totalElements = 0;
	protected boolean duplicatesAllowed = false;

	public void setDuplicatesAllowed(boolean allow) {
		duplicatesAllowed = allow;
	}

	/***
	 * Method that prints the tree in in-Order mode.
	 */
	public void inOrder(PrintStream out) {
		inOrderRec(out, this.root);
		out.println();
	}

	private void inOrderRec(PrintStream out, BinaryNode<T> root2) {
		if (root2 == null || root2.getK() == null)
			return;
		inOrderRec(out, root2.getLeft());
		root2.printNode(out);
		inOrderRec(out, root2.getRight());

	}

	/***
	 * Method that prints the tree in pre-Order mode.
	 */
	public void preOrder(PrintStream out) {
		preOrderRec(out, this.root);
		out.println();
	}

	private void preOrderRec(PrintStream out, BinaryNode<T> node) {
		if (node == null || node.getK() == null)
			return;
		node.printNode(out);
		preOrderRec(out, node.getLeft());
		preOrderRec(out, node.getRight());
	}

	/***
	 * Method that prints the tree in post-Order mode.
	 */
	public void postOrder(PrintStream out) {
		postOrderRec(out, this.root);
		out.println();
	}

	private void postOrderRec(PrintStream out, BinaryNode<T> node) {
		if (node == null || node.getK() == null)
			return;
		postOrderRec(out, node.getLeft());
		postOrderRec(out, node.getRight());
		node.printNode(out);
	}

	public String toString() {
		return toStringRev(this.root);
	}

	private String toStringRev(BinaryNode<T> node) {
		String result = "";
		/*
		 * As the implementation of red Black node uses the sentinel nil, we
		 * need to check if the node's key is null
		 */
		if (node == null || node.getK() == null)
			return result;
		result += node.toString();
		result += " (" + toStringRev(node.getLeft()) + ") ("
				+ toStringRev(node.getRight()) + ")";
		return result;
	}

	@Override
	public int getTotalElements() {
		return this.totalElements;
	}

	public BinaryNode<T> getRoot() {
		return root;
	}

	@Override
	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		return toArrayListRec(this.root, result);
	}

	private List<T> toArrayListRec(BinaryNode<T> root2, List<T> result) {
		if (root2 == null || root2.getK() == null)
			return result;
		toArrayListRec(root2.getLeft(), result);
		result.add(root2.getK());
		toArrayListRec(root2.getRight(), result);
		return result;
	}
}
