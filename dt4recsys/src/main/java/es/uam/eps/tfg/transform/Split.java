package es.uam.eps.tfg.transform;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class that splits a text file in train and test. The percentage of train it
 * is determined by its attribute percentage. This attribute must be a value
 * between 1 and 100.
 * 
 * @author Pablo
 * 
 */
public class Split {

	int percentage;

	public Split() {

		percentage = 80;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	/**
	 * generate method. It receives a source file that will be divide in a test
	 * and in a train file
	 * 
	 * @param source
	 *            the source path of the file
	 * @param test
	 *            the test file path
	 * @param train
	 *            the train file path
	 */
	public void generate(String source, String test, String train) {
		BufferedReader br = null;
		FileWriter train_file = null;
		PrintWriter pw_train = null;

		FileWriter test_file = null;
		PrintWriter pw_test = null;
		try {
			String sCurrentLine = null;
			br = new BufferedReader(new FileReader(source));
			/* First line are the titles */
			sCurrentLine = br.readLine();

			train_file = new FileWriter(train);
			pw_train = new PrintWriter(train_file);

			test_file = new FileWriter(test);
			pw_test = new PrintWriter(test_file);

			while ((sCurrentLine = br.readLine()) != null) {
				int r = (int) (Math.random() * (100 - 1)) + 1;
				if (r >= percentage) {
					pw_test.write(sCurrentLine);
					pw_test.write("\n");
				} else {
					pw_train.write(sCurrentLine);
					pw_train.write("\n");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				test_file.close();
				train_file.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
