package es.uam.eps.tfg.structures.basic;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import java.io.Serializable;

/***
 * Generic class of double linked list. It is a general class that means that
 * the double list can be of any type
 * 
 * @author Pablo
 * @version 1.0
 * @param <T>
 *            the elements that are on the double linked list
 */
public class DoubleLinkedList<T extends Sorteable<T>> implements
		GeneralStructure<T>, Serializable {
	private int size;
	private Nodelist first;
	private Nodelist tail;

	/**
	 * Constructor methods it does not take any arguments
	 */
	public DoubleLinkedList() {
		size = 0;
		first = null;
		tail = null;
	}

	/**
	 * The specific node of the linked list
	 * 
	 * @author Pablo
	 * 
	 */
	public class Nodelist implements Serializable{
		public T element;
		public Nodelist next;
		public Nodelist prev;

		public Nodelist(T element) {
			this.element = element;
			this.next = null;
			this.prev = null;
		}
	}

	/**
	 * Returns the size of the linked list
	 * 
	 * @return the number of nodes of the double-linked list
	 */
	public int size() {
		return size;
	}

	/**
	 * Method to see if the list is empty or not
	 * 
	 * @return true if it is empty and false if not
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Add an element in the first position to the list
	 * 
	 * @param element
	 *            the element to add
	 */
	@Override
	public void insert(T element) {

		Nodelist aux = new Nodelist(element);
		aux.next = first;

		if (first != null)
			first.prev = aux;

		first = aux;
		if (tail == null)
			tail = aux;

		size++;
	}

	/**
	 * Add an element at the end of the double linked list
	 * 
	 * @param element
	 *            the element to add
	 */
	public void insertTail(T element) {

		Nodelist aux = new Nodelist(element);
		aux.prev = tail;
		if (tail != null)
			tail.next = aux;
		tail = aux;
		if (first == null)
			first = aux;
		size++;
	}

	/**
	 * Method to print the list from the beginning
	 */
	public void iterateForward() {
		Nodelist aux = first;
		while (aux != null) {
			System.out.println(aux.element.toString());
			aux = aux.next;
		}
	}

	/**
	 * Method to print the list from the end
	 */
	public void iterateBackwards() {
		Nodelist tmp = tail;
		while (tmp != null) {
			System.out.println(tmp.element.toString());
			tmp = tmp.prev;
		}
	}

	/**
	 * Standard method to remove an element
	 * 
	 * @param element
	 *            the element to remove
	 */
	@Override
	public void delete(T element) {
		if (this.size == 0) {
			return;
		}
		if (this.size == 1 && this.first.element.sortAgainst(element)==0) {
			this.first = this.tail = null; // removing the only item from list
			this.size--;
			return;
		}
		if (this.first.element.sortAgainst(element)==0) { /* remove first */
			this.first = this.first.next;
			this.first.prev = null;
			this.size--;
			return;
		}
		if (this.tail.element.sortAgainst(element)==0) { /* remove tail */
			this.tail = this.tail.prev;
			this.tail.next = null;
			this.size--;
			return;
		}

		for (Nodelist current = this.first; current != null; current = current.next) {
			if (current.element.sortAgainst(element)==0) {
				Nodelist previous = current.prev;
				Nodelist next = current.next;
				previous.next = next;
				next.prev = previous;
				this.size--;
				return;
			}
		}
	}

	/**
	 * Method to remove the element from the beginning
	 * 
	 * @return null if no element exist or the element
	 */
	public T removeFirst() {
		if (size == 0)
			return null;
		Nodelist aux = first;
		first = first.next;
		first.prev = null;
		size--;
		return aux.element;
	}

	/**
	 * This method removes element from the end of the linked list
	 * 
	 * @return null if no element exist or the element
	 */
	public T removeTail() {
		if (size == 0)
			return null;
		Nodelist aux = tail;
		tail = tail.prev;
		tail.next = null;
		size--;
		return aux.element;
	}

	/**
	 * Method to search an element
	 * 
	 * @param element
	 *            the element to search
	 * @return the sublist where the element is contained, or null
	 */
	@Override
	public T search(T element) {
		Nodelist aux = first;
		while (aux != null) {
			if (element.sortAgainst(aux.element) == 0) {
				return aux.element;
			}
			aux = aux.next;
		}
		return null;
	}

	/***
	 * Method to concatenate 2 lists
	 * 
	 * @param other
	 *            the other list to concatenate (at the tail of this list)
	 */
	public void concatenate(DoubleLinkedList<T> other) {
		Nodelist aux = other.first;
		while (aux != null) {
			insertTail(aux.element);
			aux = aux.next;
		}
	}

	/***
	 * Method to obtain an element by the index
	 * 
	 * @param index
	 *            the index of the element. (First element is 0)
	 * @return the element or null
	 */
	public T get(int index) {
		Nodelist aux = this.first;
		int iteration = 0;
		while (aux != null) {
			if (index == iteration)
				return aux.element;
			iteration++;
			aux = aux.next;
		}
		return null;
	}

	/* Getters and setters */

	public int getSize() {
		return size;
	}

	public Nodelist getFirst() {
		return first;
	}

	public Nodelist getTail() {
		return tail;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		Nodelist aux = first;
		while (aux != null) {
			sb.append(aux.element.toString()).append(" ");
			aux = aux.next;
		}
		return sb.toString();
	}

	@Override
	public String getStructure() {
		return "DoubleLinkedList";
	}

	@Override
	public int getTotalElements() {
		return this.size;
	}

	@Override
	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		Nodelist aux = this.first;
		while (aux != null) {
			result.add(aux.element);
			aux = aux.next;
		}
		return result;
	}
}
