package es.uam.eps.tfg.algorithms;

import java.math.BigInteger;
import java.util.Random;

public final class StringMatchingAlgorithms {

	private static final long D = 256;
	private static final long Q = BigInteger.probablePrime(31, new Random())
			.longValue();

	/**
	 * Naive implementation of StringMatchingAlgorithm
	 * 
	 * @param text
	 *            the text where we are going to search
	 * @param pattern
	 *            the pattern to search in the text
	 * @return the number of times that the pattern appears
	 */
	public static int naiveStringMatcher(String text, String pattern) {
		int result = 0;
		int tlenght = text.length();
		int plenght = pattern.length();
		for (int i = 0; i <= (tlenght - plenght); i++) {
			if (pattern.substring(0, plenght - 1).equals(
					text.substring(i, i + plenght - 1)))
				result++;
		}
		return result;
	}

	/**
	 * Karp algorithm to match 2 substrings.
	 * 
	 * @param text
	 *            the text where we are going to search
	 * @param pattern
	 *            the patter which we are going to search
	 * @return the number of times that the pattern appears
	 */
	public static int rabinKarpMatcher(String text, String pattern) {
		int result = 0;
		int n = text.length();
		int m = pattern.length();
		long h = (long) (Math.pow(D, m - 1) % Q);
		int p = 0;
		int t0 = 0;
		for (int i = 0; i < m; i++) {
			p = (int) ((D * p + pattern.charAt(i)) % Q);
			t0 = (int) ((D * t0 + text.charAt(i)) % Q);
		}
		for (int s = 0; s <= (n - m); s++) {
			if (p == t0) {
				if (pattern.substring(0, m - 1).equals(
						text.substring(s, s + m - 1)))
					result++;
			}
			if (s < (n - m))
				t0 = (int) ((D * (t0 - text.charAt(s) * h) + text.charAt(s + m)) % Q);

		}
		return result;
	}

}
