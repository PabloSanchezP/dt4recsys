package es.uam.eps.tfg.transform;

/***
 * Main that generates a train file and a test file from a source file
 * 
 * @author Pablo
 */
public class Generate {
	private static String sourceFile = "./datasets/LastFm/user_artists.dat";
	private static String destinationFile = "./datasets/LastFm/user_artists_parsed.dat";

	public static void main(String[] args) {

		GeneralNormalization n = new GeneralNormalization();
		n.transfer2(sourceFile, destinationFile);
		Split s = new Split();
		for (int i = 0; i < 5; i++) {
			s.generate(destinationFile, "./datasets/LastFm/lastFMtest"
					+ (i + 1) + ".dat", "./datasets/LastFm/lastFMtrain"
					+ (i + 1) + ".dat");
		}
	}

}
