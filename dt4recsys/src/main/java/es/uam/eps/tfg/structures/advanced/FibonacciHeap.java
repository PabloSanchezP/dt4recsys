package es.uam.eps.tfg.structures.advanced;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.basic.DoubleLinkedList;
import es.uam.eps.tfg.structures.basic.DoubleLinkedList.Nodelist;
import es.uam.eps.tfg.structures.nodes.FibonacciNode;

public class FibonacciHeap<T extends Sorteable<T>> implements
		GeneralStructure<T> {
	private FibonacciNode<T> min;
	private DoubleLinkedList<FibonacciNode<T>> heaps;
	private int numberOfNodes;
	protected boolean duplicatesAllowed = true;

	public FibonacciHeap() {
		heaps = new DoubleLinkedList<FibonacciNode<T>>();
		min = null;
	}

	/** {@inheritDoc} */
	@Override
	public void insert(T element) {
		FibonacciNode<T> n = new FibonacciNode<T>(element);

		if (min == null) {
			heaps.insert(n);
			min = n;
		} else {
			heaps.insert(n);
			if (n.getK().sortAgainst(min.getK()) < 0)
				min = n;
		}
		numberOfNodes++;
	}

	public FibonacciHeap<T> union(FibonacciHeap<T> secondOne) {
		FibonacciHeap<T> result = new FibonacciHeap<T>();
		result.min = this.min;
		result.heaps.concatenate(this.heaps);
		result.heaps.concatenate(secondOne.heaps);
		if (this.min == null
				|| (secondOne.min != null && secondOne.min.getK().sortAgainst(
						this.min.getK()) < 0))
			result.min = secondOne.min;
		result.numberOfNodes = secondOne.numberOfNodes + this.numberOfNodes;
		return result;
	}

	public FibonacciNode<T> extractMin() {
		FibonacciNode<T> z = this.min;
		int limit = z.getChildren();
		if (z != null) {
			for (int i = 0; i < limit; i++) {
				this.heaps.insert((FibonacciNode<T>) z.getChild().get(i));
				((FibonacciNode<T>) z.getChild().get(i)).setParent(null);
			}
			this.heaps.delete(z);
			if (heaps.isEmpty() == true)
				this.min = null;
			else {
				this.min = this.heaps.get(0);
				this.consolidate();
			}
			this.numberOfNodes--;
		}
		return z;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void consolidate() {
		FibonacciNode<T>[] auxArray = new FibonacciNode[this.numberOfNodes];
		for (int i = 0; i < this.numberOfNodes; i++)
			auxArray[i] = null;
		for (Nodelist w = this.heaps.getFirst(); w != null; w = w.next) {
			FibonacciNode<T> x = (FibonacciNode<T>) w.element;
			int d = x.getDegree();
			while (auxArray[d] != null) {
				FibonacciNode<T> y = auxArray[d];
				if (x.getK().sortAgainst(y.getK()) > 0) {
					FibonacciNode<T> swap = x;
					x = y;
					y = swap;
				}
				heapLink(x, y);
				auxArray[d] = null;
				d++;
			}
			auxArray[d] = x;
		}
		this.min = null;
		for (int i = 0; i < auxArray.length; i++) {
			if (auxArray[i] != null) {
				if (this.min == null) {
					heaps = new DoubleLinkedList<FibonacciNode<T>>();
					heaps.insert(auxArray[i]);
				} else {
					heaps.insert(auxArray[i]);
					if (auxArray[i].getK().sortAgainst(this.min.getK()) < 0)
						this.min = auxArray[i];
				}
			}
		}
	}

	private void heapLink(FibonacciNode<T> x, FibonacciNode<T> y) {
		this.heaps.delete(y);
		x.addChild(y);
		x.setDegree(x.getDegree() + 1);
		y.setMark(false);
	}

	public void decreaseKey(FibonacciNode<T> x, T k) {
		FibonacciNode<T> y = null;
		if (k.sortAgainst(x.getK()) > 0) {
			System.out.println("Error. New key is greater than current key");
			return;
		}
		x.setK(k);
		y = x.getParent();
		if (y != null && x.getK().sortAgainst(y.getK()) < 0) {
			cut(x, y);
			cascadingCut(y);
		}
		if (x.getK().sortAgainst(this.min.getK()) < 0) {
			this.min = x;

		}

	}

	private void cut(FibonacciNode<T> x, FibonacciNode<T> y) {
		y.getChild().remove(x);
		y.setDegree(y.getDegree() - 1);
		this.insert(x.getK());
		x.setParent(null);
		x.setMark(false);
	}

	private void cascadingCut(FibonacciNode<T> y) {
		FibonacciNode<T> z = y.getParent();
		if (z != null) {
			if (y.getMark() == false)
				y.setMark(true);
			else {
				cut(y, z);
				cascadingCut(z);
			}
		}
	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < numberOfNodes; i++)
			result += this.heaps.get(i).toString();
		return result;

	}

	/** {@inheritDoc} */
	@Override
	public T search(T k) {
		T result = null;
		for (int i = 0; i < heaps.getSize(); i++) {
			result = this.heaps.get(i).search(k);
			if (result != null)
				return result;
		}
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public void delete(T element) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getStructure() {
		return "FibonnaciHeap";
	}

	@Override
	public int getTotalElements() {
		return numberOfNodes;
	}

	@Override
	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		@SuppressWarnings("rawtypes")
		Nodelist x = heaps.getFirst();
		while (x != null) {
			@SuppressWarnings("unchecked")
			FibonacciNode<T> aux = (FibonacciNode<T>) x.element;
			result.add(aux.getK());
			toArrayListRec(aux, result);
			x = x.next;
		}
		return result;
	}

	public List<T> toArrayListRec(FibonacciNode<T> node, List<T> result) {
		if (node.getChild() == null)
			return result;
		for (FibonacciNode<T> children : node.getChild()) {
			result.add(children.getK());
			toArrayListRec(children, result);
		}
		return result;
	}

}
