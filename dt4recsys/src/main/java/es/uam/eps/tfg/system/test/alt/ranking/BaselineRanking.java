package es.uam.eps.tfg.system.test.alt.ranking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;

import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases;

public class BaselineRanking extends BaselineDatasetMeanBiases {

	// Variable to set how many items we will consider
	protected static final int TOP = 10;

	public BaselineRanking(BIAS_TYPE type) {
		super(type);
	}

	public void evaluate() {
		double precision = 0;
		double recall = 0;
		double ndcg = 0;
		double mrr = 0;
		int cont = 0;
		for (Entry<Long, UserBias> entryU : this.users.entrySet()) {
			if (entryU.getValue().getTest() != null
					&& entryU.getValue().getRecommended() != null
					&& entryU.getValue().getTest().isEmpty() == false) {
				cont++;
				precision += this.precision(entryU.getValue().getTest(), entryU
						.getValue().getRecommended(), TOP);
				recall += this.recall(entryU.getValue().getTest(), entryU
						.getValue().getRecommended(), TOP);
				mrr += this.mrr(entryU.getValue().getTest(), entryU.getValue()
						.getRecommended(), TOP);
				ndcg += this.ndcg(entryU.getValue().getTest(), entryU
						.getValue().getRecommended(), TOP);
			}
		}

		System.out.println("---Result of ranking algorithms---");
		System.out.println("Precision: " + precision / cont);
		System.out.println("Mrr: " + mrr / cont);
		System.out.println("Ndcg: " + ndcg / cont);
		System.out.println("Recall: " + recall / cont);
	}

	/*******/
	/** MRR **/
	/*******/
	/***
	 * Mrr method. For each real item, it computes where is that real item in
	 * the recommended list of items. It computes the division between 1 and
	 * that number, and sum them all.
	 * 
	 * @param reals
	 *            the list of real items
	 * @param recommended
	 *            the list of recommended items
	 * @return the mrr value
	 */
	public double mrr(List<SorteableLong> reals,
			List<SorteableLong> recommended, int k) {
		int rank = 1;
		double acc = 0;
		int totalPosibilities = k > recommended.size() ? recommended.size() : k;
		for (SorteableLong r : reals) {
			rank = 1;
			for (SorteableLong rec : recommended) {
				if (rec.getWrapped().equals(r.getWrapped())) {
					acc += (double) 1 / (double) rank;

					break;
				}
				if (rank == totalPosibilities) { // Not found in the recommended
													// set
					acc += 0;
					break;
				}
				rank++;
			}
		}
		return acc / reals.size();
	}

	/**
	 * Method to compute the normalized Discounted cumulative gain
	 * 
	 * @param reallyRated
	 *            the list of items that have been really rated (items of the
	 *            test file)
	 * @param rec
	 *            the list of items recommended
	 * @param k
	 *            the number of items that we will take
	 * @return the value of the algorithm
	 */
	private double ndcg(List<SorteableLong> reallyRated,
			List<SorteableLong> rec, int k) {
		List<SorteableLong> realsCopy = new ArrayList<SorteableLong>(
				reallyRated);
		Collections.sort(realsCopy, Collections.reverseOrder());

		double d = 0;
		double i = 0;
		d = dcg(realsCopy, rec, k);
		i = idcg(realsCopy, k);
		if (i == 0)
			return 0;
		return d / i;
	}

	/**
	 * Method to compute the ideal Discounted cumulative gain. To do so, we
	 * order the test items by value of the user (from 5 to 1)
	 * 
	 * @param reallyRated
	 * @param k
	 * @return the ideal value
	 */
	private double idcg(List<SorteableLong> reallyRated, int k) {
		int cont = 0;
		double acc = 0;

		int itemCounter = 1;
		for (SorteableLong i : reallyRated) {
			if (cont == k)
				return acc;
			double log2i = log(itemCounter, 2);
			if (itemCounter == 1)
				acc += i.getValue();
			itemCounter++;
			if (log2i == 0)
				continue;
			else
				acc += i.getValue() / log2i;
			cont++;
		}
		return acc;
	}

	/**
	 * Method to obtain the intersection between the list of items that have
	 * been really rated by the user and items that have been recommended
	 * 
	 * @param reallyRated
	 *            the items that have been really rated
	 * @param rec
	 *            the list of items recommended
	 * @return the intersection list
	 */
	@SuppressWarnings("unused")
	private List<SorteableLong> obtainItems(List<SorteableLong> reallyRated,
			List<SorteableLong> rec) {
		List<SorteableLong> result = new ArrayList<SorteableLong>();

		for (SorteableLong r : reallyRated) {
			for (SorteableLong r2 : rec) {
				if (r2.getWrapped().equals(r.getWrapped())) {
					result.add(r);
					break;
				}
			}

		}
		return result;
	}

	/**
	 * Method to compute the discounted cumulative gain
	 * 
	 * @param reallyRated
	 *            the list of items hat have been rated
	 * @param useful
	 *            the items that have been recommended and are in reallyRated
	 * @return discounted cumulative gain (between 0 and 1)
	 */
	private double dcg(List<SorteableLong> reallyRated,
			List<SorteableLong> useful, int k) {
		int itemCounter = 1;
		int relevance = 0;
		double log2i = 0;
		double acc = 0;
		List<SorteableLong> usefulSub = useful.subList(0,
				k > useful.size() ? useful.size() : k);
		for (SorteableLong item : reallyRated) {
			boolean flag = false;
			itemCounter = 1;
			for (SorteableLong item2 : usefulSub) {
				if (item2.getWrapped().equals(item.getWrapped())) {
					flag = true;
					relevance = item.getValue().intValue();
					break;
				}
				itemCounter++;
			}

			// relevance is the ranking of the item
			if (flag == false)
				relevance = 0;
			log2i = log(itemCounter, 2);
			if (itemCounter == 1)
				acc += relevance;
			if (log2i == 0)
				continue;
			else
				acc += relevance / log2i;
		}
		return acc;
	}

	/**
	 * General method to compute logarithms
	 * 
	 * @param x
	 *            the element
	 * @param base
	 *            the base of the algorithm
	 * @return the result of that operation
	 */
	private double log(int x, int base) {
		return (double) (Math.log(x) / Math.log(base));
	}

	/***************/
	/** Precision **/
	/***************/
	/***
	 * Precision method is obtained in the division of the set form by the
	 * intersection between real (relevant) items and recommended items and the
	 * set of recommended items
	 * 
	 * @param reals
	 *            the relevant items
	 * @param recommended
	 *            the recommended items
	 * @param top2
	 * @return the division (number between 0 and 1)
	 */
	private double precision(List<SorteableLong> reals,
			List<SorteableLong> recommended, int k) {
		double good = 0;

		int cont2 = 0;
		List<SorteableLong> realsCopy = new ArrayList<SorteableLong>(reals);
		Collections.sort(realsCopy, Collections.reverseOrder());
		for (SorteableLong real : realsCopy) {
			cont2 = 0;
			for (SorteableLong r : recommended) {
				if (cont2 == k)
					break;
				cont2++;
				if (r.getWrapped().equals(real.getWrapped())) {
					good++;
					break;
				}
			}
		}
		return (good / (double) Math.min(recommended.size(), k));
	}

	/************/
	/** Recall **/
	/************/
	/***
	 * Recall method is obtained in the division of the set form by the
	 * intersection between real (relevant) items and recommended items and the
	 * set of real (relevant items)
	 * 
	 * @param reals
	 *            the relevant items
	 * @param recommended
	 *            the recommended items
	 * @param top2
	 * @return the division (measured between 0 to 1)
	 */
	public double recall(List<SorteableLong> reals,
			List<SorteableLong> recommended, int k) {
		double good = 0;
		int cont2 = 0;
		List<SorteableLong> realsCopy = new ArrayList<SorteableLong>(reals);
		Collections.sort(realsCopy, Collections.reverseOrder());
		for (SorteableLong real : realsCopy) {
			cont2 = 0;
			for (SorteableLong r : recommended) {
				if (cont2 == k)
					break;
				cont2++;
				if (r.getWrapped().equals(real.getWrapped())) {
					good++;
					break;
				}
			}
		}
		if (reals.size() == 0)
			return 0;
		return (good / Math.min(reals.size(), k));
	}
}
