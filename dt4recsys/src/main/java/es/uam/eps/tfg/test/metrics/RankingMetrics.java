package es.uam.eps.tfg.test.metrics;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.system.eval.EvaluatorUtils;
import es.uam.eps.tfg.system.eval.EvaluatorUtils.EvaluationBean;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;

/**
 * JUnit test of Ranking metrics.
 * 
 * @author Pablo
 * 
 */
public class RankingMetrics {
	NeighbourhoodRanking n;
	EvaluationBean bean;

	@Before
	public void initializeNeighBourhoobRanking() {
		n = new NeighbourhoodRanking(0, "LCS", Structures.BinarySearchTree, 0);
		bean = new EvaluationBean();
	}

	@Test
	public void precisionSimple() {

		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 3 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 4.0));
		itemsRated.add(new SorteableLong((long) 3, 3.0));

		// 4 elements retrieved, only 2 of them are in really rated
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 6, 5.0));
		itemsTest.add(new SorteableLong((long) 5, 4.0));
		itemsTest.add(new SorteableLong((long) 2, 4.0));

		Collections.sort(itemsTest, Collections.reverseOrder());
		// Result expected: 2/4 = 0.5
		double precision = n
				.precision(itemsRated, itemsTest, Integer.MAX_VALUE);
		assertEquals(0.5, precision, 0.0001);
		EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
				new int[] { Integer.MAX_VALUE }, bean);
		precision = bean.getPrecision()[0];
		assertEquals(0.5, precision, 0.0001);
	}

	@Test
	public void precisionAtWithTies() {

		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();

		// 3 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 4.0));
		itemsRated.add(new SorteableLong((long) 3, 3.0));

		// 4 elements retrieved, only 2 of them are in really rated
		// let's simulate a baseline that does not depend on the item
		//   o) case a: inserted in list by item id
		if(true){
			bean = new EvaluationBean();
			List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();
			itemsTest.add(new SorteableLong((long) 1, 5.0));
			itemsTest.add(new SorteableLong((long) 2, 5.0));
			itemsTest.add(new SorteableLong((long) 5, 5.0));
			itemsTest.add(new SorteableLong((long) 6, 5.0));

			//System.out.println(itemsTest);
			//Collections.sort(itemsTest, Collections.reverseOrder());
			//System.out.println(itemsTest); // Problem: it does not change!
			// Result expected: 2/2 = 1.0
			double precision = n.precision(itemsRated, itemsTest, 2);
			assertEquals(1.0, precision, 0.0001);
			EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
					new int[] { 2 }, bean);
			precision = bean.getPrecision()[0];
			assertEquals(1.0, precision, 0.0001);
		}

		//   o) case b: inserted in list by inverse of item id
		if(true){
			bean = new EvaluationBean();
			List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();
			itemsTest.add(new SorteableLong((long) 6, 5.0));
			itemsTest.add(new SorteableLong((long) 5, 5.0));
			itemsTest.add(new SorteableLong((long) 2, 5.0));
			itemsTest.add(new SorteableLong((long) 1, 5.0));

			//System.out.println(itemsTest);
			//Collections.sort(itemsTest, Collections.reverseOrder());
			//System.out.println(itemsTest); // Problem: it does not change!
			// Result expected: 0/2 = 0.0
			double precision = n.precision(itemsRated, itemsTest, 2);
			assertEquals(0.0, precision, 0.0001);
			EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
					new int[] { 2 }, bean);
			precision = bean.getPrecision()[0];
			assertEquals(0.0, precision, 0.0001);
		}

		//   o) case c: inserted in list by item id but sorting (by inverse item id)
		if(true){
			bean = new EvaluationBean();
			List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();
			itemsTest.add(new SorteableLong((long) 1, 5.0));
			itemsTest.add(new SorteableLong((long) 2, 5.0));
			itemsTest.add(new SorteableLong((long) 5, 5.0));
			itemsTest.add(new SorteableLong((long) 6, 5.0));

			//System.out.println(itemsTest);
			Collections.sort(itemsTest, Collections.reverseOrder());
			//System.out.println(itemsTest);
			// Result expected: 0/2 = 0.0
			double precision = n.precision(itemsRated, itemsTest, 2);
			assertEquals(0.0, precision, 0.0001);
			EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
					new int[] { 2 }, bean);
			precision = bean.getPrecision()[0];
			assertEquals(0.0, precision, 0.0001);
		}
	}

	@Test
	public void precisionAt() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 4, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 4.0));
		itemsRated.add(new SorteableLong((long) 3, 3.0));

		// 7 elements retrieved, but precision at 4
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 7, 3.0));
		itemsTest.add(new SorteableLong((long) 5, 4.0));
		itemsTest.add(new SorteableLong((long) 4, 4.0));
		itemsTest.add(new SorteableLong((long) 3, 5.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 2, 3.0));

		Collections.sort(itemsTest, Collections.reverseOrder());
		// If we order, we take the 4 best in test. 1,3,4 and 5

		// Result expected: 4/4
		double precision = n.precision(itemsRated, itemsTest, 4);
		assertEquals(1, precision, 0.0001);
		EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
				new int[] { 4 }, bean);
		precision = bean.getPrecision()[0];
		assertEquals(1, precision, 0.0001);

	}

	@Test
	public void recallSimple() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 4, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 4.0));
		itemsRated.add(new SorteableLong((long) 3, 3.0));

		// 7 elements retrieved, but precision at 4
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 8, 3.0));
		itemsTest.add(new SorteableLong((long) 5, 4.0));
		itemsTest.add(new SorteableLong((long) 9, 4.0));
		itemsTest.add(new SorteableLong((long) 3, 5.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 2, 3.0));

		// Result expected: 4/5
		double recall = n.recall(itemsRated, itemsTest, Integer.MAX_VALUE);
		assertEquals(0.8, recall, 0.0001);
		EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
				new int[] { Integer.MAX_VALUE }, bean);
		recall = bean.getRecall()[0];
		assertEquals(0.8, recall, 0.0001);

	}

	@Test
	public void recallAt() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 4, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 4.0));
		itemsRated.add(new SorteableLong((long) 3, 3.0));

		// 7 elements retrieved, but recall at 4
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 3, 5.0));
		itemsTest.add(new SorteableLong((long) 5, 4.0));
		itemsTest.add(new SorteableLong((long) 9, 4.0));
		itemsTest.add(new SorteableLong((long) 8, 3.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 2, 3.0));

		// Result expected: 3/5
		double recall = n.recall(itemsRated, itemsTest, 4);
		assertEquals(0.6, recall, 0.0001);
		EvaluatorUtils.precisionRecall(itemsRated, itemsTest, 0.0,
				new int[] { 4 }, bean);
		recall = bean.getRecall()[0];
		assertEquals(0.6, recall, 0.0001);
	}

	@Test
	public void mrrSimple() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 5.0));
		itemsRated.add(new SorteableLong((long) 3, 5.0));
		itemsRated.add(new SorteableLong((long) 4, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));

		// 7 elements retrieved, mrr
		itemsTest.add(new SorteableLong((long) 2, 5.0));
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 3, 4.0));
		itemsTest.add(new SorteableLong((long) 4, 4.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 7, 3.0));
		itemsTest.add(new SorteableLong((long) 5, 2.0));

		// Result expected: (1/2 + 1 + 1/3 + 1/4+ 1/7) / 5
		double mrr = n.mrr(itemsRated, itemsTest, Integer.MAX_VALUE);
		assertEquals(1.0, mrr, 0.1); // --> incorrect
		EvaluatorUtils.mrr(itemsRated, itemsTest, 0.0, bean);
		mrr = bean.getMrr();
		// Result expected: 1 / 1
		assertEquals(1.0, mrr, 0.1);
	}

	@Test
	public void mapSimple() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 2, 5.0));
		itemsRated.add(new SorteableLong((long) 3, 5.0));
		itemsRated.add(new SorteableLong((long) 4, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));

		// 7 elements retrieved
		itemsTest.add(new SorteableLong((long) 2, 5.0));
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 3, 4.0));
		itemsTest.add(new SorteableLong((long) 4, 4.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 7, 3.0));
		itemsTest.add(new SorteableLong((long) 5, 2.0));

		// Result expected: (1/1 + 2/2 + 3/3 + 4/4 + 5/7) / 5 = 0.9428
		double map = n.map(itemsRated, itemsTest, Integer.MAX_VALUE);
		assertEquals(0.943, map, 0.001);
		EvaluatorUtils.map(itemsRated, itemsTest, 0.0, bean);
		map = bean.getMap();
		assertEquals(0.943, map, 0.001);
	}

	@Test
	public void ndcgSimple() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 3.0));
		itemsRated.add(new SorteableLong((long) 3, 3.0));
		itemsRated.add(new SorteableLong((long) 2, 2.0));
		itemsRated.add(new SorteableLong((long) 6, 2.0));
		itemsRated.add(new SorteableLong((long) 5, 1.0));
		itemsRated.add(new SorteableLong((long) 4, 0.0));

		// 5 elements retrieved
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 2, 5.0));
		itemsTest.add(new SorteableLong((long) 3, 4.0));
		itemsTest.add(new SorteableLong((long) 4, 4.0));
		itemsTest.add(new SorteableLong((long) 5, 3.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));

		// Result expected: 3+2+1.892+0+0.431+0.774= 8.10 (dcg)
		// 8.10 / 8.69(ndcg) = 0.932
		double ndcg = n.ndcg(itemsRated, itemsTest, Integer.MAX_VALUE);
		assertEquals(0.932, ndcg, 0.001);
		EvaluatorUtils.ndcg(itemsRated, itemsTest, 0.0,
				new int[] { Integer.MAX_VALUE }, bean);
		ndcg = bean.getNdcg()[0];
		assertEquals(0.932, ndcg, 0.001);

	}

	@Test
	public void ndcgAt() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));
		itemsRated.add(new SorteableLong((long) 6, 4.0));
		itemsRated.add(new SorteableLong((long) 3, 4.0));
		itemsRated.add(new SorteableLong((long) 2, 3.0));
		itemsRated.add(new SorteableLong((long) 4, 1.0));

		// 5 elements retrieved
		itemsTest.add(new SorteableLong((long) 1, 5.0));
		itemsTest.add(new SorteableLong((long) 2, 5.0));
		itemsTest.add(new SorteableLong((long) 3, 4.0));
		itemsTest.add(new SorteableLong((long) 4, 4.0));

		itemsTest.add(new SorteableLong((long) 5, 4.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 7, 3.0));

		// Result expected: only 4 values
		// DCG should be = 5+3+2.52+0.5=11.02
		// IDCG should be = 5+ 5+ 2.52 +2 = 14.52
		double ndcg = n.ndcg(itemsRated, itemsTest, 4);
		assertEquals(0.759, ndcg, 0.001);
		EvaluatorUtils.ndcg(itemsRated, itemsTest, 0.0, new int[] { 4 }, bean);
		ndcg = bean.getNdcg()[0];
		assertEquals(0.759, ndcg, 0.001);
	}

	@Test
	public void ndcgAtv2() {
		List<SorteableLong> itemsRated = new ArrayList<SorteableLong>();
		List<SorteableLong> itemsTest = new ArrayList<SorteableLong>();

		// 5 elements rated
		itemsRated.add(new SorteableLong((long) 1, 5.0));
		itemsRated.add(new SorteableLong((long) 5, 5.0));
		itemsRated.add(new SorteableLong((long) 3, 4.0));
		itemsRated.add(new SorteableLong((long) 6, 4.0));
		itemsRated.add(new SorteableLong((long) 7, 4.0));
		itemsRated.add(new SorteableLong((long) 2, 3.0));
		itemsRated.add(new SorteableLong((long) 4, 1.0));

		// 5 elements retrieved
		itemsTest.add(new SorteableLong((long) 1, 5.49));
		itemsTest.add(new SorteableLong((long) 2, 5.2));
		itemsTest.add(new SorteableLong((long) 3, 4.3));
		itemsTest.add(new SorteableLong((long) 8, 4.2));

		itemsTest.add(new SorteableLong((long) 5, 4.0));
		itemsTest.add(new SorteableLong((long) 4, 3.0));
		itemsTest.add(new SorteableLong((long) 6, 3.0));
		itemsTest.add(new SorteableLong((long) 7, 3.0));
		itemsTest.add(new SorteableLong((long) 9, 3.0));

		// Result expected: only 4 values
		// DCG should be = 5+3+2.52+0=10.53
		// IDCG should be = 5+ 5 + 2.52 + 2 = 14.52
		double ndcg = n.ndcg(itemsRated, itemsTest, 4);
		assertEquals(0.724, ndcg, 0.001);
		EvaluatorUtils.ndcg(itemsRated, itemsTest, 0.0, new int[] { 4 }, bean);
		ndcg = bean.getNdcg()[0];
		assertEquals(0.724, ndcg, 0.001);
	}

}
