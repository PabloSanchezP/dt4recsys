package es.uam.eps.tfg.system.test.alt;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.ParseStructureFactory;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.basic.DoubleLinkedList;
import es.uam.eps.tfg.structures.basic.HashTable;
import es.uam.eps.tfg.structures.trees.BTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.BinarySearchTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.OrderStatisticTree;
import es.uam.eps.tfg.structures.trees.bsearchtrees.RedBlackTree;
import es.uam.eps.tfg.system.eval.EvaluatorUtils;
import es.uam.eps.tfg.system.eval.UserTest;
import es.uam.eps.tfg.system.eval.EvaluatorUtils.EvaluationBean;

/**
 * Abstract class that implements RecommenderSystemIF. This class is used to
 * create other child classes that uses the same structure to store users and
 * items. The classes that extends this class must implement the train method in
 * order to be compared between the children.
 * 
 * 
 * @author Pablo
 * 
 */

public abstract class General implements RecommenderSystemIF<Long, Long> {
	boolean meanCentering;

	public enum Structures {
		HashTable_RedBlack, HashTable_List, RedBlackTree, OrderStatisticTree, BinarySearchTree, DoubleLinkedList, BTree
	}

	public enum ReccomendationScheme {
		ItemBased, UserBased
	}

	protected int ne =3;
	protected int no =3;
	protected int auxSize =3;
	protected Structures structUsed;
	protected String info;

	protected ReccomendationScheme actualScheme;
	protected GeneralStructure<Element> elementBased;
	protected GeneralStructure<SorteableLong> otherElement;
	protected double totalRatings;
	protected double totalValues;
	protected int k;
	protected double limit = 0; // minimum similarity

	public General(int k, Structures s, ReccomendationScheme Scheme) {
		this.k = k;
		totalRatings = 0;
		totalValues = 0;
		actualScheme = Scheme;
		if (s == Structures.BTree)
			initStructures(s, 3, 3, 3);
		else
			initStructures(s, 1039, 1039, 1039);
		structUsed=s;
		meanCentering = true;
	}

	public General(int k, Structures s) {
		this.k = k;
		totalRatings = 0;
		totalValues = 0;
		actualScheme = ReccomendationScheme.UserBased;
		if (s == Structures.BTree)
			initStructures(s, 3, 3, 3);
		else
			initStructures(s, 1039, 1039, 1039);
		structUsed=s;
		meanCentering = true;
	}

	public General(int k, Structures s, ReccomendationScheme Scheme, int ne, int no, int auxSize) {
		this.k = k;
		totalRatings = 0;
		totalValues = 0;
		actualScheme = Scheme;
		structUsed=s;
		//if (s == Structures.BTree)
			//initStructures(s, 3, 3, 3);
		//else
		initStructures(s, ne, no, auxSize);
		meanCentering = true;
	}

	public General(int k, Structures s, int ne, int no) {
		this.k = k;
		totalRatings = 0;
		totalValues = 0;
		actualScheme = ReccomendationScheme.UserBased;
		if (s == Structures.BTree)
			initStructures(s, 3, 3, 3);
		else
			initStructures(s, ne, no, ne);
		structUsed=s;
		meanCentering = true;
	}
	
	public General(int k, Structures s, ReccomendationScheme Scheme, int ne, int no,Structures sub) {
		this.k = k;
		totalRatings = 0;
		totalValues = 0;
		actualScheme = Scheme;
		structUsed=sub;
		if (s == Structures.BTree)
			initStructures(s, 3, 3, 3);
		else
			initStructures(s, ne, no, ne);
		meanCentering = true;
	}

	public boolean isMeanCentering() {
		return meanCentering;
	}

	public void setMeanCentering(boolean meanCentering) {
		this.meanCentering = meanCentering;
	}

	/**
	 * Method to implement users and items from an element of the structures
	 * enumeration
	 * 
	 * @param s
	 *            the element of the enumeration
	 */
	private void initStructures(Structures s, int ne, int no, int auxSize) {
		this.ne=ne;
		this.no=no;
		this.auxSize = auxSize;
		
		this.elementBased = ParseStructureFactory.getStructure(s, ne);
		this.otherElement = ParseStructureFactory.getStructure(s, no);
	}

	@Override
	public EvaluationBean computeEvaluationBean(double relevanceThreshold, int[] cutoffs) {
		if (actualScheme.equals(ReccomendationScheme.UserBased)) {
			return EvaluatorUtils.evaluate(relevanceThreshold, cutoffs, elementBased.toList());
		} else {
			return EvaluatorUtils.evaluate(relevanceThreshold, cutoffs, otherElement.toList());
		}
	}

	@Override
	public void addTestInteraction(Long user, Long item, Double interaction, Double heigh) {
		if (interaction < heigh)
			return;
		if (actualScheme.equals(ReccomendationScheme.UserBased)) {
			Element newUser = new Element(user);
			Element u = elementBased.search(newUser);
			if (u == null)
				return;
			u.getTest().add(new SorteableLong(item, interaction));
		} else {
			SorteableLong newUser = new SorteableLong(user);
			SorteableLong u = otherElement.search(newUser);
			if (u == null)
				return;
			u.getTest().add(new SorteableLong(item, interaction));
		}
	}

	@Override
	public void addInteraction(Long user, Long item, Double interaction) {
		if (this.actualScheme.equals(ReccomendationScheme.UserBased))
			this.userBased(user, item, interaction);
		else
			this.itemBased(user, item, interaction);

		// Compute other information like the average of all items
		totalValues += interaction;
		totalRatings++;
	}

	/**
	 * Item based method to add an interaction. The element will be the item
	 * and the SorteableLong will be the user.
	 * @param user the long representing the id of the user
	 * @param item the long representing the id of the item.
	 * @param interaction the interaction
	 */
	private void itemBased(Long user, Long item, Double interaction) {
		// insert item (if it is new)
		Element newItem = new Element(item);
		Element u = elementBased.search(newItem);
		if (u == null) {
			newItem.FinishAlloc(structUsed,auxSize);
			elementBased.insert(newItem);
			u = newItem;
		}
		// insert interaction
		u.addInteraction(user, interaction);

		// insert user (if it is new)
		SorteableLong newUser = new SorteableLong(user);
		SorteableLong usersearched = otherElement.search(newUser);
		if (usersearched == null) {
			newUser.addRate(interaction);
			otherElement.insert(newUser);
		} else
			usersearched.addRate(interaction); // add the rate of that user
	}

	/**
	 * User based method to add an interaction. The element will be the user
	 * and the SorteableLong will be the item.
	 * @param user the long representing the id of the user
	 * @param item the long representing the id of the item.
	 * @param interaction the interaction
	 */
	private void userBased(Long user, Long item, Double interaction) {
		// insert user (if it is new)
		Element newUser = new Element(user);
		Element u = elementBased.search(newUser);
		if (u == null) {
			newUser.FinishAlloc(structUsed, auxSize);
			elementBased.insert(newUser);
			u = newUser;
		}
		// insert interaction
		u.addInteraction(item, interaction);

		// insert item (if it is new)
		SorteableLong newItem = new SorteableLong(item);
		SorteableLong itemsearched = otherElement.search(newItem);
		if (itemsearched == null) {
			newItem.addRate(interaction);
			otherElement.insert(newItem);
		} else
			itemsearched.addRate(interaction); // add the rate of that item
	}

	@Override
	public List<Long> rankItems(Long user) {
		return null;
	}

	@Override
	public void test(double defaultValue) {
		// TODO Auto-generated method stub
	}

	@Override
	public int getTotalItems() {
		if (actualScheme.equals(ReccomendationScheme.UserBased))
			return otherElement.getTotalElements();
		else
			return elementBased.getTotalElements();
	}

	@Override
	public int getTotalUsers() {
		if (actualScheme.equals(ReccomendationScheme.UserBased))
			return elementBased.getTotalElements();
		else
			return otherElement.getTotalElements();
	}

	@Override
	public void setinfo(String info) {
		this.info = info;
	}


	@Override
	/**
	 * Method to predict an Interaction. It receives 2 longs, indicating the id
	 * of the user and the id of the item.
	 * The predicted value its computed using the k neighbors and its similarity. 
	 */
	public double predictInteraction(Long user, Long item) {
		double p = 0.0;
		double weight = 0;
		int cont = 1;
		Element based;
		SorteableLong other;
		if (actualScheme.equals(ReccomendationScheme.UserBased)) {
			based = elementBased.search(new Element(user));
			other = new SorteableLong(item);
		} else {
			based = elementBased.search(new Element(item));
			other = new SorteableLong(user);
		}

		if (based != null) {
			ArrayList<WeightedElement> neighbours = based.getNeighbours();
			for (WeightedElement v : neighbours) {
				if (cont >= k)
					break;
				SorteableLong pref = v.getElement().getElementsRated().search(other);

				// Only consider positive weights
				if (pref != null && v.w > limit) {
					weight += Math.abs(v.getw());
					if (meanCentering == true)
						p += ((pref.getValue() - v.getElement().avgElement()) * v.getw()); // Mean
																							// Centering
					else
						p += pref.getValue() * v.getw();
					cont++;
				}

			}
		}
		if (weight == 0) {
			/*
			 * if (u==null) return this.avgItems(); else return u.avGUser();
			 */
			return Double.NaN;
		}

		if (meanCentering == true)
			return (p / weight) + based.avgElement(); // Mean Centering
		else
			return p / weight;
	}

	@Override
	public void setMinimumSimilarity(double w) {
		limit = w;
	}

	@Override
	public void setNeighbours(int k) {
		this.k = k;
	}

	@SuppressWarnings("unused")
	private double avgItems() {
		return this.totalValues / this.totalRatings;

	}

	/**
	 * Element class. An element can be a user (if it is user-based) or an item (if it is item 
	 * based strategy). Each element have:
	 * -A general structure of sorteable longs rated.
	 * -A ordered list of elements (ordered by simmilarity).
	 * -A list of sorteable long items to test.
	 * -A list of sorteable long recommended.
	 * @author Pablo
	 *
	 */
	public static class Element implements Sorteable<Element>, UserTest<SorteableLong> {
		private Long id;
		private GeneralStructure<SorteableLong> elementsRated;
		private ArrayList<WeightedElement> neighbours;
		private ArrayList<SorteableLong> test;
		private List<SorteableLong> recommended;

		private double totalRated; // totalRatings of the element
		private double desviation; // Sum of ratings of the element
		private int totalRatings; // number of ratings of this element

		public Element(Long id) {
			this.id = id;
			
		}
		
		public void FinishAlloc(Structures st, int ne){
			totalRated = 0.0;
			totalRatings = 0;
			// useStructure(st, ne);
			this.elementsRated = ParseStructureFactory.getStructure(st, ne);
			neighbours = new ArrayList<WeightedElement>();
			test = new ArrayList<SorteableLong>();
			recommended = new ArrayList<SorteableLong>();
			desviation = 0;
			
		}
		
		@Override
		public List<SorteableLong> getRecommended() {
			return recommended;
		}

		public void setRecommended(List<SorteableLong> l) {
			recommended = l;
		}

		public Long getId() {
			return id;
		}

		@Override
		public List<SorteableLong> getTest() {
			return test;
		}

		public void addInteraction(Long item, Double value) {
			// we assume there are no repetitions
			elementsRated.insert(new SorteableLong(item, value));
			totalRated += value;
			totalRatings++;
			desviation += value * value;
		}

		public double avgElement() {
			return totalRated / totalRatings;
		}

		public int getTotalRatings() {
			return totalRatings;
		}
		
		public double deviation() {
			return (desviation / totalRatings) - avgElement() * avgElement();
		}

		public void addNeighbour(WeightedElement v) {
			neighbours.add(v);
		}

		public ArrayList<WeightedElement> getNeighbours() {
			return neighbours;
		}

		public GeneralStructure<SorteableLong> getElementsRated() {
			return elementsRated;
		}

		@Override
		public int sortAgainst(Element o) {
			if (o == null)
				return -1;
			return id.compareTo(o.id);
		}

		@Override
		public boolean equals(Object obj) {
			return id.equals(obj);
		}

		@Override
		public String toString() {
			return "E" + id;
		}
	}

	public static class WeightedElement implements Comparable<WeightedElement>, Sorteable<WeightedElement> {
		private Element u;
		private double w;

		public WeightedElement(Element u, double w) {
			this.u = u;
			this.w = w;
		}

		@Override
		public int compareTo(WeightedElement o) {
			return Double.compare(w, o.w);
		}

		public Element getElement() {
			return u;
		}

		public double getw() {
			return w;
		}

		@Override
		public int sortAgainst(WeightedElement o) {
			if (o == null)
				return -1;

			return Double.compare(w, o.w);
		}
	}
}
