package es.uam.eps.tfg.system.test.alt;


import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import es.uam.eps.tfg.structures.ParseStructureFactory;
import es.uam.eps.tfg.system.eval.EvaluatorUtils.EvaluationBean;
import es.uam.eps.tfg.system.test.alt.General.Structures;

/***
 * Most important main of all.
 * It receives 12 arguments indicating:
 * -The training file.
 * -The test file.
 * -The structure (see parseStructure method).
 * -The recommender that will be used (see buildDataset method).
 * -The number of neighbors (each neighbor separated by comma: ex "5,10,100")
 * -The cutoffs (used in ranking metrics to get for example ndcg@10). Same procedure as neighbors (separated by commas).
 * -The relevance threshold. The number that will be used to check if a item is relevant.
 * -The default value. In case that an item cannot be predicted, it will take this number.
 * -The number of users of the dataset.
 * -The number of items of the dataset.
 * -An aux number that will be used in some structures (like hash tables) for the substructures created.
 * -A boolean indicating if the evaluation will be done.
 * @author Pablo
 *
 */
public class ConsoleComparison {

	private static final int COLUMN_USER = 0;
	private static final int COLUMN_ITEM = 1;
	private static final int COLUMN_RATING = 2;

	private static final int MIN_NEIGHBOURS=10;
	
	public static void main(String[] args) throws Exception {
		//$training_file $test_file $structure $rec $neighbours $cutoffs $relTh $defValue $users $items 23
		 
		//Example of a poisble call
		/*args = new String[] { "./datasets/Movielens/u2.base",
		 "./datasets/Movielens/u2.test", "hashtable_tree", "Pearson_Normal_IB_MC", "10",
		 "10,1000", "4", "NaN", "1000", "1600", "23", "true" };*/
		 

		System.out.println(
				"training_file test_file structure recommenders neighbours cutoffs relevance_threshold default_value nusers nitems auxn doeval");
		System.out.println(Arrays.toString(args));

		String trainingFile = args[0];
		String testFile = args[1];

		Structures structure = ParseStructureFactory.parseStructure(args[2]);

		String recommenders = args[3];
		String neighbours = args[4];

		String cutoffsString = args[5];
		String[] cutoffsSplitted = cutoffsString.split(",");
		int[] cutoffs = new int[cutoffsSplitted.length];
		for (int i = 0; i < cutoffs.length; i++) {
			cutoffs[i] = Integer.parseInt(cutoffsSplitted[i]);
		}

		double relevanceThreshold = Double.parseDouble(args[6]);
		double defaultValue = Double.parseDouble(args[7]);

		int nusers = Integer.parseInt(args[8]);
		int nitems = Integer.parseInt(args[9]);
		int auxn = Integer.parseInt(args[10]);

		boolean doEval = Boolean.parseBoolean(args[11]);

		final Set<Double> thresholds = new HashSet<Double>();
		thresholds.add(relevanceThreshold);
		thresholds.add(0.0);

		Runtime runtime = Runtime.getRuntime();

		for (String recommender : recommenders.split(",")) {
			long time1, time2, timeTotal = 0;
			long memory1, memory2, memoryTotal = 0;
			memory1 = runtime.totalMemory() - runtime.freeMemory();
			time1 = System.currentTimeMillis();
			RecommenderSystemIF<Long, Long> rec = RecommenderSystemFactory.buildDataset(recommender, structure, nusers, nitems, auxn,MIN_NEIGHBOURS);
			RecommenderSystemFactory.loadTrainingDataset(rec, trainingFile,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);
			memory2 = runtime.totalMemory() - runtime.freeMemory();
			time2 = System.currentTimeMillis();
			long timeLoadTraining = time2 - time1;
			timeTotal += timeLoadTraining;
			long memoryLoadTraining = memory2 - memory1;
			memoryTotal += memoryLoadTraining;

			memory1 = runtime.totalMemory() - runtime.freeMemory();
			time1 = System.currentTimeMillis();
			RecommenderSystemFactory.loadTestDataset(rec, testFile, 0.0,COLUMN_USER,COLUMN_ITEM,COLUMN_RATING);
			memory2 = runtime.totalMemory() - runtime.freeMemory();
			time2 = System.currentTimeMillis();
			long timeLoadTest = time2 - time1;
			timeTotal += timeLoadTest;
			long memoryLoadTest = memory2 - memory1;
			memoryTotal += memoryLoadTest;

			memory1 = runtime.totalMemory() - runtime.freeMemory();
			time1 = System.currentTimeMillis();
			rec.train(true);
			memory2 = runtime.totalMemory() - runtime.freeMemory();
			time2 = System.currentTimeMillis();
			long timeTraining = time2 - time1;
			timeTotal += timeTraining;
			long memoryTraining = memory2 - memory1;
			memoryTotal += memoryTraining;

			if (recommender.contains("baseline")) {
				// ignore neighbours
				memory1 = runtime.totalMemory() - runtime.freeMemory();
				time1 = System.currentTimeMillis();
				if (doEval) {
					rec.test(defaultValue);
				}
				memory2 = runtime.totalMemory() - runtime.freeMemory();
				time2 = System.currentTimeMillis();
				long timeTest = time2 - time1;
				timeTotal += timeTest;
				long memoryTest = memory2 - memory1;
				memoryTotal += memoryTest;

				for (double relTh : thresholds) {
					memory1 = runtime.totalMemory() - runtime.freeMemory();
					time1 = System.currentTimeMillis();
					EvaluationBean bean = null;
					if (doEval) {
						bean = rec.computeEvaluationBean(relTh, cutoffs);
					}
					memory2 = runtime.totalMemory() - runtime.freeMemory();
					time2 = System.currentTimeMillis();
					long timeEval = time2 - time1;
					timeTotal += timeEval;
					long memoryEval = memory2 - memory1;
					memoryTotal += memoryEval;

					print(System.out, bean, recommender + "__relTh_" + relTh + "__defV_" + defaultValue,
							timeLoadTraining, timeTraining, timeLoadTest, timeTest, timeEval, timeTotal,
							memoryLoadTraining, memoryTraining, memoryLoadTest, memoryTest, memoryEval, memoryTotal);
				}
			} else {
				for (String neighbour : neighbours.split(",")) {
					long curTimeTotal = timeTotal;
					long curMemoryTotal = memoryTotal;

					rec.setNeighbours(Integer.parseInt(neighbour));

					memory1 = runtime.totalMemory() - runtime.freeMemory();
					time1 = System.currentTimeMillis();
					if (doEval) {
						rec.test(defaultValue);
					}
					memory2 = runtime.totalMemory() - runtime.freeMemory();
					time2 = System.currentTimeMillis();
					long timeTest = time2 - time1;
					curTimeTotal += timeTest;
					long memoryTest = memory2 - memory1;
					curMemoryTotal += memoryTest;

					for (double relTh : thresholds) {
						memory1 = runtime.totalMemory() - runtime.freeMemory();
						time1 = System.currentTimeMillis();
						EvaluationBean bean = null;
						if (doEval) {
							bean = rec.computeEvaluationBean(relTh, cutoffs);
						}
						memory2 = runtime.totalMemory() - runtime.freeMemory();
						time2 = System.currentTimeMillis();
						long timeEval = time2 - time1;
						curTimeTotal += timeEval;
						long memoryEval = memory2 - memory1;
						curMemoryTotal += memoryEval;

						print(System.out, bean,
								recommender + "__relTh_" + relTh + "__defV_" + defaultValue + "__neigh_" + neighbour,
								timeLoadTraining, timeTraining, timeLoadTest, timeTest, timeEval, curTimeTotal,
								memoryLoadTraining, memoryTraining, memoryLoadTest, memoryTest, memoryEval,
								curMemoryTotal);
					}
				}
			}
		}
	}

	private static void print(PrintStream out, EvaluationBean bean, String recName, long timeLoadTraining,
			long timeTraining, long timeLoadTest, long timeTest, long timeEval, long timeTotal, long memoryLoadTraining,
			long memoryTraining, long memoryLoadTest, long memoryTest, long memoryEval, long memoryTotal) {
		if (bean != null) {
			out.println(recName + "\tmae\t" + bean.getMae());
			out.println(recName + "\trmse\t" + bean.getRmse());
			out.println(recName + "\tuser_mae\t" + bean.getUserMae());
			out.println(recName + "\tuser_rmse\t" + bean.getUserRmse());
			out.println(recName + "\tn_error\t" + bean.getnError());
			out.println(recName + "\tn\t" + bean.getN());
			out.println(recName + "\tmrr\t" + bean.getMrr());
			out.println(recName + "\tmap\t" + bean.getMap());
			for (int i = 0; i < bean.getCutoffs().length; i++) {
				out.println(recName + "\tP@" + bean.getCutoffs()[i] + "\t" + bean.getPrecision()[i]);
				out.println(recName + "\tR@" + bean.getCutoffs()[i] + "\t" + bean.getRecall()[i]);
				out.println(recName + "\tNDCG@" + bean.getCutoffs()[i] + "\t" + bean.getNdcg()[i]);
			}
		}
		// in ms
		out.println(recName + "\tload_training_time\t" + timeLoadTraining);
		out.println(recName + "\ttraining_time\t" + timeTraining);
		out.println(recName + "\tload_test_time\t" + timeLoadTest);
		out.println(recName + "\ttest_time\t" + timeTest);
		out.println(recName + "\teval_time\t" + timeEval);
		out.println(recName + "\ttotal_time\t" + timeTotal);
		// in bytes
		out.println(recName + "\tload_training_memory\t" + memoryLoadTraining);
		out.println(recName + "\ttraining_memory\t" + memoryTraining);
		out.println(recName + "\tload_test_memory\t" + memoryLoadTest);
		out.println(recName + "\ttest_memory\t" + memoryTest);
		out.println(recName + "\teval_memory\t" + memoryEval);
		out.println(recName + "\ttotal_memory\t" + memoryTotal);
	}

	

	



}
