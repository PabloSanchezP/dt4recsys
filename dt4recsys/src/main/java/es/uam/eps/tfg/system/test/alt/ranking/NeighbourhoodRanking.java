package es.uam.eps.tfg.system.test.alt.ranking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.system.eval.EvaluatorUtils;

import es.uam.eps.tfg.system.test.alt.ratingprediction.neighbourhood.NeighBourhoodSimilarity;

/***
 * Abstract class that implements all methods of rankings measures. All the
 * classes that extends ranking must implement the test method and the main that
 * runs the program
 * 
 * @author Pablo
 * 
 */
public class NeighbourhoodRanking extends NeighBourhoodSimilarity {
	// Variable to set how many items we will consider
	protected static final int TOP = 10;

	public NeighbourhoodRanking(int k, String similarity, Structures s, int LCSthreshold) {
		super(k, similarity, s, LCSthreshold);
	}

	public NeighbourhoodRanking(int k, String similarity, Structures s, ReccomendationScheme sch) {
		super(k, similarity, s, sch);
	}

	public NeighbourhoodRanking(int k, String similarity, Structures s, int LCSthreshold, ReccomendationScheme sch) {
		super(k, similarity, s, LCSthreshold, sch);
	}

	public NeighbourhoodRanking(int k, String similarity, Structures s, ReccomendationScheme sch, int limit,
			int LCSthreshold) {
		super(k, similarity, s, sch, limit, LCSthreshold);
	}

	///// Primes
	public NeighbourhoodRanking(int prime1, int prime2, int k, String similarity, Structures s, int LCSthreshold) {
		super(prime1, prime2, k, similarity, s, LCSthreshold);
	}

	public NeighbourhoodRanking(int prime1, int prime2, int auxint, int k, String similarity, Structures s,
			ReccomendationScheme sch) {
		super(prime1, prime2, auxint, k, similarity, s, sch);
	}

	public NeighbourhoodRanking(int prime1, int prime2, int k, String similarity, Structures s, int LCSthreshold,
			ReccomendationScheme sch) {
		super(prime1, prime2, k, similarity, s, LCSthreshold, sch);
	}

	public NeighbourhoodRanking(int prime1, int prime2, int auxint, int k, String similarity, Structures s,
			ReccomendationScheme sch, int limit, int LCSthreshold) {
		super(prime1, prime2, auxint, k, similarity, s, sch, limit, LCSthreshold);
	}
	
	//ComparisonSubstructures
	public NeighbourhoodRanking(int prime1, int prime2, int k, String similarity, Structures s,
			ReccomendationScheme sch,Structures sub) {
		super(prime1, prime2, k, similarity, s, sch,sub);
	}

	@Override
	public void test(double defaultValue) {
		if (actualScheme.equals(ReccomendationScheme.UserBased)) {
			// testUserBased();
			EvaluatorUtils.testAllItems(elementBased.toList(), otherElement.toList(), this, defaultValue);
		} else {
			// testItemBased();
			EvaluatorUtils.testAllUsers(otherElement.toList(), elementBased.toList(), this, defaultValue);
		}
	}



	public String evaluateV2() {
		String result = "";
		double precision = 0;
		double recall = 0;
		double ndcg = 0;
		double mrr = 0;
		int cont = 0;
		if (actualScheme.equals(ReccomendationScheme.UserBased)) {
			// EvaluatorUtils.evaluate(0.0, new int[]{TOP},
			// elementBased.toArrayList());
			for (Element u : elementBased.toList()) {
				if (u.getTest() != null && u.getRecommended() != null && u.getTest().isEmpty() == false) {
					cont++;
					precision += this.precision(u.getTest(), u.getRecommended(), TOP);
					recall += this.recall(u.getTest(), u.getRecommended(), TOP);
					mrr += this.mrr(u.getTest(), u.getRecommended(), TOP);
					ndcg += this.ndcg(u.getTest(), u.getRecommended(), TOP);
				}
			}
		} else {
			// EvaluatorUtils.evaluate(0.0, new int[]{TOP},
			// otherElement.toArrayList());
			for (SorteableLong u : otherElement.toList()) {
				if (u.getTest() != null && u.getRecommended() != null && u.getTest().isEmpty() == false) {
					cont++;
					precision += this.precision(u.getTest(), u.getRecommended(), TOP);
					recall += this.recall(u.getTest(), u.getRecommended(), TOP);
					mrr += this.mrr(u.getTest(), u.getRecommended(), TOP);
					ndcg += this.ndcg(u.getTest(), u.getRecommended(), TOP);
				}
			}
		}
		result += precision / cont + ";" + mrr / cont + ";" + ndcg / cont + ";" + recall / cont;
		return result;
	}

	/**
	 * Method to evaluate the results of the ranking
	 */
	public void evaluate() {
		double precision = 0;
		double recall = 0;
		double ndcg = 0;
		double mrr = 0;
		int cont = 0;

		if (actualScheme.equals(ReccomendationScheme.UserBased)) {
			for (Element u : elementBased.toList()) {
				if (u.getTest() != null && u.getRecommended() != null && u.getTest().isEmpty() == false) {
					cont++;
					precision += this.precision(u.getTest(), u.getRecommended(), TOP);
					recall += this.recall(u.getTest(), u.getRecommended(), TOP);
					mrr += this.mrr(u.getTest(), u.getRecommended(), TOP);
					ndcg += this.ndcg(u.getTest(), u.getRecommended(), TOP);
				}
			}
		} else {
			for (SorteableLong u : otherElement.toList()) {
				if (u.getTest() != null && u.getRecommended() != null && u.getTest().isEmpty() == false) {
					cont++;
					precision += this.precision(u.getTest(), u.getRecommended(), TOP);
					recall += this.recall(u.getTest(), u.getRecommended(), TOP);
					mrr += this.mrr(u.getTest(), u.getRecommended(), TOP);
					ndcg += this.ndcg(u.getTest(), u.getRecommended(), TOP);
				}
			}
		}

		System.out.println("---Result of ranking algorithms---");
		System.out.println("Precision: " + precision / cont);
		System.out.println("Mrr: " + mrr / cont);
		System.out.println("Ndcg: " + ndcg / cont);
		System.out.println("Recall: " + recall / cont);
	}

	

	/*******************/
	/** Ranking methods **/
	/*******************/

	/*******/
	/** MRR **/

	/*******/
	/***
	 * Mrr method. For each real item, it computes where is that real item in
	 * the recommended list of items. It computes the division between 1 and
	 * that number, and sum them all.
	 * 
	 * @param reals
	 *            the list of real items
	 * @param recommended
	 *            the list of recommended items
	 * @return the mrr value
	 */
	public double mrr(List<SorteableLong> reals, List<SorteableLong> recommended, int k) {
		int rank = 0;
		int totalPosibilities = k > recommended.size() ? recommended.size() : k;
		for (SorteableLong rec : recommended) {
			rank++;
			if (totalPosibilities == rank) // Not found in the k
				break;
			for (SorteableLong r : reals) {
				if (r.getWrapped() == rec.getWrapped()) { // Found in test
					return 1.0 / (double) rank;
				}
			}
		}

		return 0;
	}

	/**
	 * Method to compute the normalized Discounted cumulative gain
	 * 
	 * @param reallyRated
	 *            the list of items that have been really rated (items of the
	 *            test file)
	 * @param rec
	 *            the list of items recommended
	 * @param k
	 *            the number of items that we will take
	 * @return the value of the algorithm
	 */
	public double ndcg(List<SorteableLong> reallyRated, List<SorteableLong> rec, int k) {
		List<SorteableLong> realsCopy = new ArrayList<SorteableLong>(reallyRated);
		Collections.sort(realsCopy, Collections.reverseOrder());

		double d = 0;
		double i = 0;
		d = dcg(realsCopy, rec, k);
		i = idcg(realsCopy, k);
		if (i == 0)
			return 0;
		return d / i;
	}

	/**
	 * Method to compute the ideal Discounted cumulative gain. To do so, we
	 * order the test items by value of the user (from 5 to 1)
	 * 
	 * @param reallyRated
	 * @param k
	 * @return the ideal value
	 */
	private double idcg(List<SorteableLong> reallyRated, int k) {
		int cont = 0;
		double acc = 0;

		cont = 0;
		int itemCounter = 1;
		for (SorteableLong i : reallyRated) {
			if (cont == k)
				return acc;
			cont++;
			double log2i = log(itemCounter, 2);
			if (itemCounter == 1)
				acc += i.getValue();
			itemCounter++;
			if (log2i == 0)
				continue;
			else
				acc += i.getValue() / log2i;
		}
		return acc;
	}

	/**
	 * Method to obtain the intersection between the list of items that have
	 * been really rated by the user and items that have been recommended
	 * 
	 * @param reallyRated
	 *            the items that have been really rated
	 * @param rec
	 *            the list of items recommended
	 * @return the intersection list
	 */
	@SuppressWarnings("unused")
	private List<SorteableLong> obtainItems(List<SorteableLong> reallyRated, List<SorteableLong> rec) {
		List<SorteableLong> result = new ArrayList<SorteableLong>();

		for (SorteableLong r : reallyRated) {
			for (SorteableLong r2 : rec) {
				if (r2.getWrapped().equals(r.getWrapped())) {
					result.add(r);
					break;
				}
			}

		}
		return result;
	}

	/**
	 * Method to compute the discounted cumulative gain
	 * 
	 * @param reallyRated
	 *            the list of items hat have been rated
	 * @param useful
	 *            the items that have been recommended and are in reallyRated
	 * @return discounted cumulative gain (between 0 and 1)
	 */
	private double dcg(List<SorteableLong> reallyRated, List<SorteableLong> useful, int k) {
		int itemCounter = 0;
		int cont = 0;
		int relevance = 0;
		double log2i = 0;
		double acc = 0;
		boolean found = false;

		// Used to compute dcg
		List<SorteableLong> evaluated = new ArrayList<SorteableLong>();

		for (SorteableLong r : useful) {
			if (cont == k)
				break;
			found = false;
			// Searching in test
			for (SorteableLong rR : reallyRated) {
				if (r.getWrapped().equals(rR.getWrapped())) {
					found = true;
					evaluated.add(new SorteableLong(rR.getWrapped(), rR.getValue()));
					break;
				}
			}
			// Not found in test, so relevance is 0
			if (!found)
				evaluated.add(new SorteableLong(r.getWrapped(), 0.0));

			cont++;
		}

		for (SorteableLong item : evaluated) {
			itemCounter++;
			relevance = item.getValue().intValue();

			log2i = log(itemCounter, 2);
			if (itemCounter == 1)
				acc += relevance;
			if (log2i == 0)
				continue;
			else
				acc += relevance / log2i;
		}
		return acc;
	}

	public double map(List<SorteableLong> reallyRated, List<SorteableLong> useful, int k) {
		int rank = 0;
		double rel = 0;
		double acc = 0.0;
		for (SorteableLong rec : useful) {
			if (rank == k)
				break;
			rank++;

			boolean found = false;
			for (SorteableLong r : reallyRated) {

				if (rec.getWrapped().equals(r.getWrapped())) {
					found = true;
					break;
				}
			}
			if (found) {
				rel++;
				acc += rel / rank;
			}
		}
		return (rel == 0 ? 0.0 : acc / rel);
	}

	/**
	 * General method to compute logarithms
	 * 
	 * @param x
	 *            the element
	 * @param base
	 *            the base of the algorithm
	 * @return the result of that operation
	 */
	private double log(int x, int base) {
		return (double) (Math.log(x) / Math.log(base));
	}

	/***************/
	/** Precision **/

	/***************/
	/***
	 * Precision method is obtained in the division of the set form by the
	 * intersection between real (relevant) items and recommended items and the
	 * set of recommended items
	 * 
	 * @param reals
	 *            the relevant items
	 * @param recommended
	 *            the recommended items
	 * @param top2
	 * @return the division (number between 0 and 1)
	 */
	public double precision(List<SorteableLong> reals, List<SorteableLong> recommended, int k) {
		double good = 0;

		int cont2 = 0;
		List<SorteableLong> realsCopy = new ArrayList<SorteableLong>(reals);
		Collections.sort(realsCopy, Collections.reverseOrder());
		for (SorteableLong real : realsCopy) {
			cont2 = 0;
			for (SorteableLong r : recommended) {
				if (cont2 == k)
					break;
				cont2++;
				if (r.getWrapped().equals(real.getWrapped())) {
					good++;
					break;
				}
			}
		}
		return (good / (double) Math.min(recommended.size(), k));
	}

	/************/
	/** Recall **/

	/************/
	/***
	 * Recall method is obtained in the division of the set form by the
	 * intersection between real (relevant) items and recommended items and the
	 * set of real (relevant items)
	 * 
	 * @param reals
	 *            the relevant items
	 * @param recommended
	 *            the recommended items
	 * @param top2
	 * @return the division (measured between 0 to 1)
	 */
	public double recall(List<SorteableLong> reals, List<SorteableLong> recommended, int k) {
		double good = 0;
		int cont2 = 0;
		List<SorteableLong> realsCopy = new ArrayList<SorteableLong>(reals);
		Collections.sort(realsCopy, Collections.reverseOrder());
		for (SorteableLong real : realsCopy) {
			cont2 = 0;
			for (SorteableLong r : recommended) {
				if (cont2 == k)
					break;
				cont2++;
				if (r.getWrapped().equals(real.getWrapped())) {
					good++;
					break;
				}
			}
		}
		if (reals.size() == 0)
			return 0;
		return (good / reals.size());
	}

	@Override
	public String info() {
		return similarity + TOP + " Items";
	}

}
