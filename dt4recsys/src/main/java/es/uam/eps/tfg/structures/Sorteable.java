package es.uam.eps.tfg.structures;

/***
 * Interface that must be implemented by all elements that will be stored in
 * GenealStructure. It is the equivalent of Comparable
 * 
 * @author Pablo
 * 
 * @param <T>
 */
public interface Sorteable<T> {

	public int sortAgainst(T o);

}
