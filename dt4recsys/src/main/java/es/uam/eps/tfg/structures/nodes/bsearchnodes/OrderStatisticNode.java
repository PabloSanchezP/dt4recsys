package es.uam.eps.tfg.structures.nodes.bsearchnodes;

import es.uam.eps.tfg.structures.Sorteable;

/***
 * Class of nodes that are in the OrderStatisticTree. It extends the
 * RedBlackNode adding one more argument, the size.
 * 
 * @author Pablo
 * @version 1.0
 */
public class OrderStatisticNode<T extends Sorteable<T>> extends RedBlackNode<T> {
	private int size;

	public OrderStatisticNode(T k) {
		super(k);
		this.size = 1;
	}

	/* Getters and setters */
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
