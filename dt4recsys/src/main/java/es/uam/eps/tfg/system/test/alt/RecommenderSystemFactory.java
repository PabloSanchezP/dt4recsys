package es.uam.eps.tfg.system.test.alt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import es.uam.eps.tfg.system.test.alt.General.ReccomendationScheme;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import es.uam.eps.tfg.system.test.alt.ranking.NeighbourhoodRanking;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetRandomRS;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases.BIAS_TYPE;

/***
 * Final class that contains important methods that will be used by most mains in the application
 * Main methods are: buildDataset, loadTrainingDataset and loadTestDataset
 * @author Pablo
 *
 */
public final class RecommenderSystemFactory {

	public static int nextPrime(int n) {
		boolean isPrime = false;
		int start = 3;
		if (n % 2 == 0) {
			n = n + 1;
		}
		while (!isPrime) {
			isPrime = true;
			int m = (int) Math.ceil(Math.sqrt(n));
			for (int i = start; i <= m; i = i + 2) {
				if (n % i == 0) {
					isPrime = false;
					break;
				}
			}
			if (!isPrime) {
				n = n + 2;
			}
		}
		return n;
	}
	
	/**
	 * It creates a Recommender System interface using the arguments
	 * @param rec the recommender
	 * @param struct the structure
	 * @param nusers the number of users of the dataset
	 * @param nitems the number of items of the dataset
	 * @param auxn the auxiliary size of the substructures used (if needed)
	 * @param minNeighbours the minimun number of neighbours
	 * @return a recommender interface
	 */
	public static RecommenderSystemIF<Long, Long> buildDataset(String rec, Structures struct, int nusers, int nitems, int auxn, int minNeighbours) {
		int primeUser = nextPrime(nusers);
		int primeItem = nextPrime(nitems);
		int primeAux = nextPrime(auxn);
		RecommenderSystemIF<Long, Long> dataset = null;
		// baselines
		if (rec.equals("baseline_random")) {
			dataset = new BaselineDatasetRandomRS();
		} else if (rec.equals("baseline_user_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.USER);
		} else if (rec.equals("baseline_item_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.ITEM);
		} else if (rec.equals("baseline_system_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.SYSTEM);
		} else if (rec.equals("baseline_bias")) {
			dataset = new BaselineDatasetMeanBiases(BIAS_TYPE.COMBINED);
			// Pearson UB
		} else if (rec.equals("Pearson_Union0_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union0_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("PearsonPos_Union0_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union0_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// Pearson IB
		} else if (rec.equals("Pearson_Union0_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union0_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Union3_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Pearson_Normal_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("PearsonPos_Union0_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union0_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union0", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Union3_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("PearsonPos_Normal_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Pearson Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// Cosine UB
		} else if (rec.equals("Cosine_Normal_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("CosinePos_Normal_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.UserBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
			// Cosine IB
		} else if (rec.equals("Cosine_Union3_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Union3_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("Cosine_Normal_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(-1.0);
		} else if (rec.equals("CosinePos_Union3_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Union3_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Union3", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(false);
			dataset.setMinimumSimilarity(0);
		} else if (rec.equals("CosinePos_Normal_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "Cosine Normal", struct,
					ReccomendationScheme.ItemBased);
			((General) dataset).setMeanCentering(true);
			dataset.setMinimumSimilarity(0);
			// LCS UB
		} else if (rec.equals("LCS_-1_0_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_0_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_0_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_0_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_1_UB_Std")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 30, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_1_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, 16, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_UB_MC")) {
			dataset = new NeighbourhoodRanking(primeUser, primeItem, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.UserBased, -1, 1);
			((General) dataset).setMeanCentering(true);
			// LCS UB
		} else if (rec.equals("LCS_-1_0_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_0_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 0);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_0_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_0_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_0_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 0);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_16_1_IB_Std")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 1);
			((General) dataset).setMeanCentering(false);
		} else if (rec.equals("LCS_30_1_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 30, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_16_1_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, 16, 1);
			((General) dataset).setMeanCentering(true);
		} else if (rec.equals("LCS_-1_1_IB_MC")) {
			dataset = new NeighbourhoodRanking(primeItem, primeUser, primeAux, minNeighbours, "LCS", struct,
					ReccomendationScheme.ItemBased, -1, 1);
			((General) dataset).setMeanCentering(true);
		}
		dataset.setinfo(rec);
		return dataset;
	}
	
	/**
	 * It loads a training file to be used in the dataset
	 * @param rec the recommender interface
	 * @param trainingFile the path of the training file
	 * @param columnUser the user column
	 * @param columnItem the item column
	 * @param columnRating the rating column
	 * @throws IOException 
	 */
	public static void loadTrainingDataset(
			RecommenderSystemIF<Long, Long> rec, String trainingFile, int columnUser, int columnItem,int columnRating)
			throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(trainingFile));
		String line = null;
		while ((line = in.readLine()) != null) {
			String[] toks = line.split("\t");
			rec.addInteraction(Long.parseLong(toks[columnUser]),
					Long.parseLong(toks[columnItem]),
					Double.parseDouble(toks[columnRating]));
		}
		in.close();
	}

	/**
	 * It loads the test file to be used in the dataset
	 * @param recommender interface
	 * @param testFile the test file
	 * @param relevanceThreshold the threshold to that an item should reach to be consider as relevant
	 * @param columnUser the user column
	 * @param columnItem the item column
	 * @param columnRating the rating column
	 * @throws IOException
	 */
	public static void loadTestDataset(RecommenderSystemIF<Long, Long> rec,
			String testFile, double relevanceThreshold,int columnUser, int columnItem,int columnRating) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(testFile));
		String line = null;
		while ((line = in.readLine()) != null) {
			String[] toks = line.split("\t");
			rec.addTestInteraction(Long.parseLong(toks[columnUser]),
					Long.parseLong(toks[columnItem]),
					Double.parseDouble(toks[columnRating]), relevanceThreshold);
		}
		in.close();
	}
}
