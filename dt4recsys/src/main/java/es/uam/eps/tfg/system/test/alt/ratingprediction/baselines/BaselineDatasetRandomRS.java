package es.uam.eps.tfg.system.test.alt.ratingprediction.baselines;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Random;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.system.eval.EvaluatorUtils;
import es.uam.eps.tfg.system.eval.EvaluatorUtils.EvaluationBean;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases.ItemBias;
import es.uam.eps.tfg.system.test.alt.ratingprediction.baselines.BaselineDatasetMeanBiases.UserBias;

/***
 * Baseline random class.
 * It stores the items, the users and the maximum and minimum values of the dataset.
 * The predicted value will be a random number between the minimum and the maximum value.
 * 
 * @author Pablo
 *
 */
public class BaselineDatasetRandomRS implements RecommenderSystemIF<Long, Long> {

	protected Map<Long, UserBias> users;
	protected Map<Long, ItemBias> items;
	private Double minRating;
	private Double maxRating;
	private Random rnd;
	private String info;

	public BaselineDatasetRandomRS() {
		users = new HashMap<Long, UserBias>();
		items = new HashMap<Long, ItemBias>();

		minRating = Double.MAX_VALUE;
		maxRating = Double.MIN_VALUE;

		rnd = new Random();
	}

	@Override
	public int getTotalItems() {
		return items.size();
	}

	@Override
	public int getTotalUsers() {
		return users.size();
	}

	@Override
	public void addInteraction(Long user, Long item, Double interaction) {
		UserBias u = users.get(user);

		if (items.get(item) == null)
			items.put(item, new ItemBias(item));

		ItemBias ac = items.get(item);

		// New user
		if (u == null) {
			u = new UserBias(user);
			u.addInteraction(ac, interaction);
			users.put(user, u);
		} else
			// User found
			u.addInteraction(ac, interaction);

		minRating = Math.min(minRating, interaction);
		maxRating = Math.max(maxRating, interaction);
	}

	@Override
	public void train(boolean useTestUsers) {
		// do nothing
	}

	@Override
	public double predictInteraction(Long user, Long item) {
		double d = rnd.nextDouble();
		return minRating + d * (maxRating - minRating);
	}

	@Override
	public List<Long> rankItems(Long user) {
		return null;
	}

	@Override
	public String info() {
		return info;
	}

	@Override
	public void setNeighbours(int k) {
		return;
	}

	@Override
	public void setMinimumSimilarity(double w) {
		return;
	}

	@Override
	public void test(double defaultValue) {
		for (long user : users.keySet()) {
			UserBias u = users.get(user);
			List<SorteableLong> result = new ArrayList<SorteableLong>();
			for (long i : items.keySet()) {
				// if the item has been rated, then we do not consider it
				if (!u.getRatedItems().contains(i)) {
					double p = predictInteraction(user, i);
					if (!Double.isNaN(p)) {
						result.add(new SorteableLong(i, p));
					} else if (!Double.isNaN(defaultValue)) {
						result.add(new SorteableLong(i, defaultValue));
					}
				}
			}
			Collections.sort(result, Collections.reverseOrder());
			u.setRecommended(result);
		}
	}

	@Override
	public EvaluationBean computeEvaluationBean(double relevanceThreshold,
			int[] cutoffs) {
		return EvaluatorUtils.evaluate(relevanceThreshold, cutoffs,
				new ArrayList<UserBias>(users.values()));
	}

	@Override
	public void addTestInteraction(Long user, Long item, Double interaction,
			Double threshold) {
		if (interaction < threshold)
			return;
		UserBias u = users.get(user);
		if (u == null)
			return;
		u.addTestIteraction(item, interaction);
	}

	@Override
	public void setinfo(String info) {
		this.info = info;
	}
}
