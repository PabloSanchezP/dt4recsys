package es.uam.eps.tfg.structures.nodes;

import java.util.List;
import java.util.ArrayList;

import es.uam.eps.tfg.structures.Sorteable;
import java.io.Serializable;

/***
 * BNode class. It has and arrayList of keys and an arrayList of BNodes
 * (children). The most important methods are search, insert and splitnode.
 * 
 * @author Pablo
 *
 * @param <T>
 */
public class BNode<T extends Sorteable<T>> implements Serializable {

	private boolean leaf;
	private int n;

	private static final int DEFAULT_T = 3;
	private int t;
	private List<T> keys;
	private List<BNode<T>> c;

	public int getT() {
		return t;
	}

	public BNode() {
		this(DEFAULT_T);
	}

	public BNode(int t) {
		this.t = t;
		keys = new ArrayList<T>();
		c = new ArrayList<BNode<T>>();

		for (int i = 0; i < (2 * t) - 1; i++) {
			c.add(null);
			keys.add(null);
		}
		c.add(null);

	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public T search(T k) {
		int i = 0;
		while (i < n && k.sortAgainst(keys.get(i)) > 0)
			i++;
		if (i < n && k.sortAgainst(keys.get(i)) == 0)
			return keys.get(i);
		else if (this.leaf == true)
			return null;
		else
			return c.get(i).search(k);
	}

	/***
	 * Method to split a child of a BNode
	 * 
	 * @param i
	 *            the index of the child
	 */
	public void splitChild(int i) {
		int j = 0;
		BNode<T> z = new BNode<T>(this.getT());
		BNode<T> y = this.c.get(i);
		z.leaf = y.leaf;
		z.n = t - 1;

		for (j = 0; j < t - 1; j++) {
			z.keys.set(j, y.keys.get(j + t));
			y.keys.set(j + t, null);
		}
		if (y.leaf == false) {
			for (j = 0; j < t; j++) {
				z.c.set(j, y.c.get(j + t));
				y.c.set(j + t, null);
			}
		}
		y.n = t - 1;

		for (j = this.n; j > i; j--)
			this.c.set(j + 1, this.c.get(j));

		this.c.set(i + 1, z);

		for (j = this.n - 1; j >= i; j--)
			this.keys.set(j + 1, this.keys.get(j));

		this.keys.set(i, y.keys.get(t - 1));
		y.keys.set(t - 1, null);

		this.n++;
	}

	/***
	 * Method to insert an element (key) in a node
	 * 
	 * @param k
	 */
	public void insertNonFull(T k) {
		int i = this.n;
		if (this.leaf == true) {
			while (i >= 1 && k.sortAgainst(this.keys.get(i - 1)) < 0) {
				this.keys.set(i, this.keys.get(i - 1));
				i--;
			}
			this.keys.set(i, k);
			this.n++;
		} else {
			int j = 0;
			while (j < this.n && k.sortAgainst(this.keys.get(j)) > 0)
				j++;

			if (this.c.get(j).n == 2 * t - 1) {
				this.splitChild(j);
				if (k.sortAgainst(this.keys.get(j)) > 0)
					j++;
			}
			this.c.get(j).insertNonFull(k);
		}

	}

	public void removeData(int pos) {
		for (int i = pos; i < this.n - 1; i++)
			this.keys.set(i, this.keys.get(i + 1));
		this.n--;
	}

	public void copyPredecessor(int i) {
		BNode<T> leaf = this.c.get(i);
		while (leaf.c.get(leaf.n) != null)
			leaf = leaf.c.get(leaf.n);
		this.keys.set(i, leaf.keys.get(leaf.n - 1));
	}

	public void restore(int p) {
		if (p == this.n) {
			if (this.c.get(p - 1).n > ((t * 2) - 1) / 2)
				moveRight(p - 1);
			else
				combine(p);
		} else if (p == 0) {
			if (this.c.get(1).n > ((t * 2) - 1) / 2)
				moveLeft(1);
			else
				combine(1);
		} else {
			if (this.c.get(p - 1).n > ((t * 2) - 1) / 2)
				moveRight(p - 1);
			else if (this.c.get(p + 1).n > ((t * 2) - 1) / 2)
				moveLeft(p + 1);
			else
				combine(p);

		}

	}

	private void moveLeft(int p) {
		BNode<T> lb = this.c.get(p - 1);
		BNode<T> rb = this.c.get(p);

		lb.keys.set(lb.n, this.keys.get(p - 1));
		lb.keys.set(++lb.n, rb.keys.get(0));

		this.keys.set(p - 1, rb.keys.get(0));
		rb.n--;

		for (int i = 0; i < n; i++) {
			rb.keys.set(i, rb.keys.get(i + 1));
			rb.c.set(i, rb.c.get(i + 1));
		}
		rb.c.set(rb.n, rb.c.get(rb.n + 1));

	}

	private void combine(int p) {
		BNode<T> lb = this.c.get(p - 1);
		BNode<T> rb = this.c.get(p);

		lb.keys.set(lb.n, this.keys.get(p - 1));
		lb.c.set(++lb.n, rb.c.get(0));

		for (int i = 0; i < rb.n; i++) {
			lb.keys.set(lb.n, rb.keys.get(i));
			lb.c.set(++lb.n, rb.c.get(i + 1));
		}
		this.n--;
		for (int i = p - 1; i < this.n; i++) {
			this.keys.set(i, this.keys.get(i + 1));
			this.c.set(i + 1, this.c.get(i + 2));
		}

	}

	public int[] postition(T k) {
		int[] result = new int[2];
		int i = 0;
		while (i < n && k.sortAgainst(keys.get(i)) > 0)
			i++;
		result[0] = i;
		if (i < this.getN() && k.sortAgainst(this.keys.get(i)) == 0)
			result[1] = 1;
		else
			result[1] = -1;
		return result;
	}

	private void moveRight(int p) {
		BNode<T> lb = this.c.get(p);
		BNode<T> rb = this.c.get(p + 1);

		rb.c.set(rb.n + 1, rb.c.get(rb.n));

		for (int i = rb.n; i > 0; i--) {
			rb.keys.set(i, rb.keys.get(i - 1));
			rb.c.set(i, rb.c.get(i - 1));
		}
		rb.n++;
		rb.keys.set(0, this.keys.get(p));

		rb.c.set(0, lb.c.get(lb.n--));
		this.keys.set(p, lb.keys.get(lb.n));

	}

	public List<T> getKeys() {
		return keys;
	}

	public void setKeys(List<T> keys) {
		this.keys = keys;
	}

	public List<BNode<T>> getC() {
		return c;
	}

	public void setC(List<BNode<T>> c) {
		this.c = c;
	}

}
