package es.uam.eps.tfg.structures.ranksys;

import es.uam.eps.tfg.structures.Sorteable;
import java.io.Serializable;

/**
 *
 * @author Saúl Vargas (Saul@VargasSandoval.es)
 */
public class Keyed<Ci, Cr> implements Sorteable<Keyed<Ci, Cr>>, Serializable {

    private final long key;
    private final Ci ci;
    private final Cr cr;

    public Keyed(long key, Ci ci, Cr cr) {
        this.key = key;
        this.ci = ci;
        this.cr = cr;
    }

    public long getKey() {
        return key;
    }

    public Ci getCi() {
        return ci;
    }

    public Cr getCr() {
        return cr;
    }

    @Override
    public int sortAgainst(Keyed<Ci, Cr> o) {
        return Long.compare(key, o.key);
    }

    @Override
    public String toString() {
        return Long.toString(key);
    }

    public static <Ci, Cr> Keyed<Ci, Cr> keyed(long key, Ci ci, Cr cr) {
        return new Keyed(key, ci, cr);
    }

    public static <Ci, Cr> Keyed<Ci, Cr> keyed(long key) {
        return new Keyed(key, null, null);
    }

}
