package es.uam.eps.tfg.structures.basic;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.tfg.structures.GeneralStructure;
import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.trees.bsearchtrees.RedBlackTree;
import es.uam.eps.tfg.system.test.alt.General.Structures;
import java.io.Serializable;

/***
 * Hash tables implementation. It has a list of 31 (default) elements and each
 * element can be another class that implements the General Structure interface.
 * It can also accepts another prime.
 * 
 * @author Pablo
 * 
 * @param <T>
 *            the element that will have the hash table
 */
public class HashTable<T extends Sorteable<T>> implements GeneralStructure<T>, Serializable {

	private int HASHPRIME = 31;
	private int totalElements;
	private List<GeneralStructure<T>> list;
	private String name;

	public HashTable() {
		list = new ArrayList<GeneralStructure<T>>(HASHPRIME);
		name = "HashTable_DLL_" + HASHPRIME;
		for (int i = 0; i < HASHPRIME; i++) {
			/* Change if we want to use another structure */
			list.add(i, (new DoubleLinkedList<T>()));
		}
		totalElements = 0;
	}

	public HashTable(Structures s, int prime) {
		list = new ArrayList<GeneralStructure<T>>(HASHPRIME);
		HASHPRIME = prime;
		for (int i = 0; i < HASHPRIME; i++) {
			switch (s) {
			case RedBlackTree:
				list.add(i, (new RedBlackTree<T>()));
				name = "HashTable_RBT_" + HASHPRIME;
				break;
			case DoubleLinkedList:
				list.add(i, (new DoubleLinkedList<T>()));
				name = "HashTable_DLL_" + HASHPRIME;
			default:
				// do nothing
			}
		}
		/* Change if we want to use another structure */
		totalElements = 0;
	}

	@Override
	public T search(T k) {
		String s = k.toString();
		int index = (int) this.myHash(s);
		return list.get(index).search(k);
	}

	@Override
	public void insert(T element) {
		String s = element.toString();
		int index = (int) this.myHash(s);
		int before = list.get(index).getTotalElements();
		list.get(index).insert(element);
		if (before != list.get(index).getTotalElements())
			totalElements++;
	}

	@Override
	public void delete(T element) {
		int before;
		String s = element.toString();
		int index = (int) this.myHash(s);
		before = list.get(index).getTotalElements();
		list.get(index).delete(element);
		if (before > list.get(index).getTotalElements())
			totalElements--;
	}

	@Override
	public String getStructure() {
		return name;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	private long myHash(String s) {
		long hashvalue = 0;
		int limit = s.length();
		for (int i = 0; i < limit; i++)
			hashvalue += s.charAt(i);
		//hashvalue *= HASHPRIME;
		hashvalue %= HASHPRIME;
		return hashvalue;
	}

	public String toString() {
		String result = "";
		for (int i = 0; i < HASHPRIME; i++) {
			result += list.get(i).toString();
			result += " ";
		}
		return result;
	}

	@Override
	public List<T> toList() {
		List<T> result = new ArrayList<T>();
		for (int i = 0; i < HASHPRIME; i++)
			result.addAll(list.get(i).toList());
		return result;
	}
}
