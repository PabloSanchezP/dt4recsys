package es.uam.eps.tfg.structures.trees.bheaps;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.HeapNode;

/***
 * 
 * Class of a Max-Heap. It has an array of HeapNodes starting in 0, so the
 * methods parent, right and left have changed.
 * 
 * @author Pablo
 * @param <T>
 *            the element that will have the class
 * @version 1.0
 */
public class MaxHeap<T extends Sorteable<T>> extends BinaryHeap<T> {

	public MaxHeap(int maximum_lenght) {
		super(maximum_lenght);

	}

	/***
	 * This method builds the max-heap. This should be called after inserting
	 * all the nodes of the heap.
	 */
	public void buildMaxHeap() {
		this.size = this.lenght - 1;
		for (int i = (int) (this.lenght * 0.5); i >= 0; i--) {
			maxHeapify(i);
		}
	}

	/***
	 * To maintain the max-heap property we must call max_heapify. This method
	 * ensure that every children is lower than its parents
	 * 
	 * @param index
	 *            of the node in the array to apply max_heapify
	 */
	public void maxHeapify(int index) {
		int largest;
		int right = right(index);
		int left = left(index);

		/*
		 * If the key of the node in the left is greater than the key index, the
		 * largest element is the left one
		 */
		if (left <= this.size
				&& this.nodes[left].getK()
						.sortAgainst(this.nodes[index].getK()) > 0)
			largest = left;
		else
			largest = index;

		/* Now, we compare the key in the right */
		if (right <= this.size
				&& this.nodes[right].getK().sortAgainst(
						this.nodes[largest].getK()) > 0)
			largest = right;
		if (largest != index) {
			/* Swap elements, and call again heapify */
			HeapNode<T> aux = this.nodes[index];
			this.nodes[index] = this.nodes[largest];
			this.nodes[largest] = aux;

			maxHeapify(largest);
		}
	}

	/***
	 * The last method to call. This finally restructure the array to finally
	 * have an ordered array.
	 */
	public void heapSort() {
		int old_size = this.size;
		buildMaxHeap();
		for (int i = this.lenght - 1; i >= 1; i--) {
			HeapNode<T> aux = this.nodes[0];
			this.nodes[0] = this.nodes[i];
			this.nodes[i] = aux;
			this.size--;
			maxHeapify(0);
		}
		this.size = old_size;
	}

	/***
	 * Method to test if the array is a max heap
	 * 
	 * @param root
	 *            the index of the array
	 * @return true if it is a max heap or false if not.
	 */
	public boolean checkMaxHeap(int root) {
		if (root >= this.nodes.length || (2 * root + 2) >= this.nodes.length)
			return true;

		/* check if root is the greatest element */
		if (this.nodes[root].getK()
				.sortAgainst(this.nodes[2 * root + 1].getK()) < 0
				|| (this.nodes[root].getK().sortAgainst(
						this.nodes[2 * root + 2].getK()) < 0))
			return false;

		if (!checkMaxHeap(2 * root + 1)) /* check leftsubtree */
			return false;

		if (!checkMaxHeap(2 * root + 2)) /* check rightsubtree */
			return false;

		return true;
	}

	@Override
	public String getStructure() {
		return "MaxHeap";
	}

}
