package es.uam.eps.tfg.system.test.alt;

import java.util.List;

import es.uam.eps.tfg.system.eval.EvaluatorUtils.EvaluationBean;

/***
 * Recommender System Interface. All recommenders should implement this methods
 * in order to compute all the required information. It has 2 parameters the
 * type that will used as users and items
 * 
 * @author Pablo
 * 
 * @param <U>
 * @param <I>
 */
public interface RecommenderSystemIF<U, I> {

	/***
	 * Method to add an interaction to the recommender.
	 * 
	 * @param user
	 *            the user that has rated the item
	 * @param item
	 *            the item that has been rated
	 * @param interaction
	 *            the value that the user has given to the item
	 */
	void addInteraction(U user, I item, Double interaction);

	/***
	 * Training method
	 */
	void train(boolean useOnlyTestUsers);

	/**
	 * Method that will return a double that will be the recommenders
	 * iteracticion predicted by the user to that item.
	 * 
	 * @param user
	 * @param item
	 * @return
	 */
	double predictInteraction(U user, I item);

	/**
	 * Method to store the interactions of the test set
	 * 
	 * @param user
	 * @param item
	 * @param interaction
	 * @param heigh
	 */
	void addTestInteraction(U user, I item, Double interaction, Double heigh);

	List<I> rankItems(U user);

	/**
	 * Method to obtain the total items stored
	 * 
	 * @return number of items
	 */
	int getTotalItems();

	/**
	 * Method to obtain the total users stored
	 * 
	 * @return number of users
	 */
	int getTotalUsers();

	/**
	 * Method to obtain the information of the recommender
	 * 
	 * @return
	 */
	String info();

	/**
	 * Method to update the number of neighbours to consider while predicting
	 * interactions
	 * 
	 * @param k
	 */
	void setNeighbours(int k);

	/**
	 * Method to update the minimum similarity that a neighbour must have
	 * 
	 * @param w
	 */
	void setMinimumSimilarity(double w);

	/**
	 * Method to obtain results of the test
	 * 
	 * @param defaultValue
	 */
	void test(double defaultValue);

	/**
	 * Updates the info of the recommender
	 * 
	 * @param info
	 */
	void setinfo(String info);

	/***
	 * Computes the bean that have all the metrics to be calculated.
	 * 
	 * @param relevanceThreshold
	 * @param cutoffs
	 * @return
	 */
	EvaluationBean computeEvaluationBean(double relevanceThreshold,
			int[] cutoffs);
}
