package es.uam.eps.tfg.system.eval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import es.uam.eps.tfg.algorithms.Similarity.SorteableLong;
import es.uam.eps.tfg.system.test.alt.RecommenderSystemIF;
import es.uam.eps.tfg.system.test.alt.General.Element;


/**
 * Class used to make ranking evaluations. It used an evaluation bean that computes the results of
 * all metrics used to test the performance of the algorithms. 
 * 
 * @author Pablo
 *
 */
public class EvaluatorUtils {

	/**
	 * Method to obtain the ratings of all items for each users
	 * @param firstDim the first dimension will be the users
	 * @param secondDim the second, the items
	 * @param rec the recommender interface
	 * @param defaultPredictedValue the predicted value to put in case that the prediction could not be made.
	 */
	public static void testAllItems(List<Element> firstDim,
			List<SorteableLong> secondDim, RecommenderSystemIF<Long, Long> rec,
			double defaultPredictedValue) {
		for (Element u : firstDim) {
			ArrayList<SorteableLong> result = new ArrayList<SorteableLong>();
			for (SorteableLong i : secondDim) {
				// if the item has been rated, then we do not consider it
				if (u.getElementsRated().search(i) == null) {
					double p = rec
							.predictInteraction(u.getId(), i.getWrapped());
					if (!Double.isNaN(p)) {
						result.add(new SorteableLong(i.getWrapped(), p));
					} else if (!Double.isNaN(defaultPredictedValue)) {
						result.add(new SorteableLong(i.getWrapped(),
								defaultPredictedValue));
					}
				}
			}
			Collections.sort(result, Collections.reverseOrder());
			u.setRecommended(result);
		}
	}

	/***
	 * Method to obtain the ratings of all items for each users
	 * @param firstDim the items
	 * @param secondDim the users
	 * @param rec the recommender interface
	 * @param defaultPredictedValue the predicted value to put in case that the prediction could not be made.
	 */
	public static void testAllUsers(List<SorteableLong> firstDim,
			List<Element> secondDim, RecommenderSystemIF<Long, Long> rec,
			double defaultPredictedValue) {
		for (SorteableLong u : firstDim) {
			ArrayList<SorteableLong> result = new ArrayList<SorteableLong>();
			for (Element i : secondDim) {
				// if the item has been rated, then we do not consider it
				if (i.getElementsRated().search(u) == null) {
					double p = rec
							.predictInteraction(u.getWrapped(), i.getId());
					if (!Double.isNaN(p)) {
						result.add(new SorteableLong(i.getId(), p));
					} else if (!Double.isNaN(defaultPredictedValue)) {
						result.add(new SorteableLong(i.getId(),
								defaultPredictedValue));
					}
				}
			}
			Collections.sort(result, Collections.reverseOrder());
			u.setRecommended(result);
		}
	}

	/***
	 * Computation of the evaluation bean.
	 * @param relevanceThreshold the relevance to consider a item relevant
	 * @param cutoffs the cutoffs of the list of recommender items
	 * @param evaluationDim the users
	 * @return a bean with the results of all the metrics
	 */
	public static EvaluationBean evaluate(double relevanceThreshold,
			int[] cutoffs,
			final List<? extends UserTest<SorteableLong>> evaluationDim) {
		EvaluationBean eb = new EvaluationBean();
		eb.setCutoffs(cutoffs);

		int cont = 0;
		for (UserTest<SorteableLong> u : evaluationDim) {
			if (u.getTest() != null && u.getRecommended() != null
					&& u.getTest().isEmpty() == false) {
				cont++;

				List<SorteableLong> sortedReals = new ArrayList<SorteableLong>(
						u.getTest());
				Collections.sort(sortedReals, Collections.reverseOrder());

				precisionRecall(sortedReals, u.getRecommended(),
						relevanceThreshold, cutoffs, eb);
				mrr(sortedReals, u.getRecommended(), relevanceThreshold, eb);
				map(sortedReals, u.getRecommended(), relevanceThreshold, eb);
				ndcg(sortedReals, u.getRecommended(), relevanceThreshold,
						cutoffs, eb);
				error(sortedReals, u.getRecommended(), eb);
			}
		}
		eb.setN(cont);
		// normalize all metrics
		eb.setMrr(eb.getMrr() / (double) eb.getN());
		eb.setMap(eb.getMap() / (double) eb.getN());
		eb.setUserRmse(eb.getUserRmse() / (double) eb.getN());
		eb.setUserMae(eb.getUserMae() / (double) eb.getN());
		eb.setRmse(Math.sqrt(eb.getRmse() / (double) eb.getnError()));
		eb.setMae(eb.getMae() / (double) eb.getnError());
		double[] pr = eb.getPrecision();
		double[] r = eb.getRecall();
		double[] ndcg = eb.getNdcg();
		for (int i = 0; i < cutoffs.length; i++) {
			pr[i] = pr[i] / (double) eb.getN();
			r[i] = r[i] / (double) eb.getN();
			ndcg[i] = ndcg[i] / (double) eb.getN();
		}
		eb.setPrecision(pr);
		eb.setRecall(r);
		eb.setNdcg(ndcg);

		return eb;
	}

	/**
	 * Computes precision and Recall metrics
	 * @param sortedReals the items really rated
	 * @param recommended the items recommended
	 * @param relevanceThreshold the relevance to consider an item useful
	 * @param cutoffs number of items to consider in the recommender items set
	 * @param bean the evaluation bean
	 */
	public static void precisionRecall(final List<SorteableLong> sortedReals,
			final List<SorteableLong> recommended, double relevanceThreshold,
			int[] cutoffs, EvaluationBean bean) {
		double[] good = new double[cutoffs.length];
		Arrays.fill(good, 0.0);
		int n = 0;
		for (SorteableLong real : sortedReals) {
			// only consider items in the test set above the relevance threshold
			if (real.getValue() < relevanceThreshold) {
				continue;
			}
			n = 0;
			for (SorteableLong r : recommended) {
				if (Double.isNaN(r.getValue())) {
					continue;
				}
				n++;
				if (r.getWrapped().equals(real.getWrapped())) {
					for (int i = 0; i < cutoffs.length; i++) {
						if (n <= cutoffs[i]) {
							good[i] = good[i] + 1;
						}
					}
				}
			}
		}
		double[] precision = bean.getPrecision();
		if (precision == null) {
			precision = new double[cutoffs.length];
			Arrays.fill(precision, 0.0);
		}
		double[] recall = bean.getRecall();
		if (recall == null) {
			recall = new double[cutoffs.length];
			Arrays.fill(recall, 0.0);
		}
		for (int i = 0; i < cutoffs.length; i++) {
			precision[i] += (recommended.size() == 0 ? 0.0 : good[i]
					/ (double) Math.min(recommended.size(), cutoffs[i]));
			recall[i] += (sortedReals.size() == 0 ? 0.0 : good[i]
					/ (double) sortedReals.size());
		}
		bean.setPrecision(precision);
		bean.setRecall(recall);
	}

	/**
	 * Computes Mrr metric
	 * @param reals the items really rated
	 * @param recommended the items recommended
	 * @param relevanceThreshold the relevance to consider an item useful
	 * @param bean the evaluation bean
	 */
	public static void mrr(final List<SorteableLong> reals,
			final List<SorteableLong> recommended, double relevanceThreshold,
			EvaluationBean bean) {
		int rank = 0;
		for (SorteableLong rec : recommended) {
			rank++;
			if (Double.isNaN(rec.getValue())) {
				continue;
			}
			for (SorteableLong r : reals) {
				// only consider items in the test set above the relevance
				// threshold
				if (r.getValue() < relevanceThreshold) {
					continue;
				}
				if (rec.getWrapped().equals(r.getWrapped())) {
					double acc = (double) 1 / (double) rank;
					bean.setMrr(acc + bean.getMrr());
					return;
				}
			}
		}
		// if no relevant item was found, add nothing to the MRR
	}

	
	public static void map(final List<SorteableLong> reals,
			final List<SorteableLong> recommended, double relevanceThreshold,
			EvaluationBean bean) {
		int rank = 0;
		int rel = 0;
		double ap = 0.0;
		for (SorteableLong rec : recommended) {
			rank++;
			if (Double.isNaN(rec.getValue())) {
				continue;
			}
			boolean relFound = false;
			for (SorteableLong r : reals) {
				// only consider items in the test set above the relevance
				// threshold
				if (r.getValue() < relevanceThreshold) {
					continue;
				}
				if (rec.getWrapped().equals(r.getWrapped())) {
					relFound = true;
					break;
				}
			}
			if (relFound) {
				rel++;
				ap += 1.0 * rel / rank;
			}
		}
		double map = (rel == 0 ? 0.0 : ap / (double) rel);
		bean.setMap(map + bean.getMap());
	}

	/**
	 * Computes Ndcg metric
	 * @param sortedReals the items really rated
	 * @param recommended the items recommended
	 * @param relevanceThreshold the relevance to consider an item useful
	 * @param cutoffs number of items to consider in the recommender items set
	 * @param bean the evaluation bean
	 */
	public static void ndcg(final List<SorteableLong> sortedReals,
			final List<SorteableLong> recommended, double relevanceThreshold,
			int[] cutoffs, EvaluationBean bean) {
		double[] dcgs = new double[cutoffs.length];
		Arrays.fill(dcgs, 0.0);
		double[] idcgs = new double[cutoffs.length];
		Arrays.fill(idcgs, 0.0);

		// compute the ideal
		int rank = 0;
		double idcg = 0.0;
		for (SorteableLong r : sortedReals) {
			rank++;
			if (r.getValue() < relevanceThreshold) {
				continue;
			}
			double log2rank = (Math.log(rank) / Math.log(2));
			if (rank == 1) {
				idcg += r.getValue();
			} else {
				idcg += r.getValue() / log2rank;
			}
			for (int i = 0; i < cutoffs.length; i++) {
				if (rank <= cutoffs[i]) {
					idcgs[i] = idcg;
				}
			}
		}
		// compute the DCG
		rank = 0;
		double dcg = 0.0;
		for (SorteableLong r : recommended) {
			rank++;
			if (Double.isNaN(r.getValue())) {
				continue;
			}
			SorteableLong realItem = null;
			for (SorteableLong item : sortedReals) {
				if (r.getWrapped().equals(item.getWrapped())) {
					realItem = item;
					break;
				}
			}
			// this item is not relevant
			if (realItem == null) {
				continue;
			}
			// this item is not relevant enough
			if (realItem.getValue() < relevanceThreshold) {
				continue;
			}
			// this item is relevant
			double log2rank = (Math.log(rank) / Math.log(2));
			if (rank == 1) {
				dcg += realItem.getValue();
			} else {
				dcg += realItem.getValue() / log2rank;
			}
			for (int i = 0; i < cutoffs.length; i++) {
				if (rank <= cutoffs[i]) {
					dcgs[i] = dcg;
				}
			}
		}

		// normalize
		double[] ndcg = bean.getNdcg();
		if (ndcg == null) {
			ndcg = new double[cutoffs.length];
			Arrays.fill(ndcg, 0.0);
		}
		for (int i = 0; i < cutoffs.length; i++) {
			ndcg[i] += (idcgs[i] == 0 ? 0.0 : dcgs[i] / idcgs[i]);
		}
		bean.setNdcg(ndcg);
	}

	/**
	 * Computes Mae and rmse metrics (prediction metrics)
	 * @param sortedReals the items really rated
	 * @param recommended the items recommended
	 * @param bean the evaluation bean
	 */
	public static void error(final List<SorteableLong> sortedReals,
			final List<SorteableLong> recommended, EvaluationBean bean) {
		double rmse = 0.0;
		double mae = 0.0;
		int n = 0;

		for (SorteableLong r : recommended) {
			if (Double.isNaN(r.getValue())) {
				continue;
			}
			SorteableLong realItem = null;
			for (SorteableLong item : sortedReals) {
				if (r.getWrapped().equals(item.getWrapped())) {
					realItem = item;
					break;
				}
			}
			// this item is not relevant
			if (realItem == null) {
				continue;
			}
			double diff = r.getValue() - realItem.getValue();
			mae += Math.abs(diff);
			rmse += diff * diff;
			// We only want to consider those that are really evaluated using
			// error
			n++;
		}

		bean.setnError(n + bean.getnError());
		bean.setMae(mae + bean.getMae());
		bean.setRmse(rmse + bean.getRmse());
		bean.setUserMae(mae / n + bean.getUserMae());
		bean.setUserRmse(Math.sqrt(rmse / n) + bean.getUserRmse());
	}

	/**
	 * Evaluation bean class. It has all the results of the metrics
	 * @author Pablo
	 *
	 */
	public static class EvaluationBean {
		private int n;
		private int nError;
		private int[] cutoffs;
		private double[] precision;
		private double[] recall;
		private double mrr;
		private double map;
		private double[] ndcg;
		private double rmse;
		private double mae;
		private double userRmse;
		private double userMae;

		public EvaluationBean() {
			n = 0;
			nError = 0;
			mrr = 0.0;
			map = 0.0;
			rmse = 0.0;
			mae = 0.0;
			userRmse = 0.0;
			userMae = 0.0;
		}

		public int getN() {
			return n;
		}

		public void setN(int n) {
			this.n = n;
		}

		public int[] getCutoffs() {
			return cutoffs;
		}

		public void setCutoffs(int[] cutoffs) {
			this.cutoffs = cutoffs;
		}

		public double[] getPrecision() {
			return precision;
		}

		public void setPrecision(double[] precision) {
			this.precision = precision;
		}

		public double[] getRecall() {
			return recall;
		}

		public void setRecall(double[] recall) {
			this.recall = recall;
		}

		public double getMrr() {
			return mrr;
		}

		public void setMrr(double mrr) {
			this.mrr = mrr;
		}

		public double getMap() {
			return map;
		}

		public void setMap(double map) {
			this.map = map;
		}

		public double[] getNdcg() {
			return ndcg;
		}

		public void setNdcg(double[] ndcg) {
			this.ndcg = ndcg;
		}

		public double getRmse() {
			return rmse;
		}

		public void setRmse(double rmse) {
			this.rmse = rmse;
		}

		public double getMae() {
			return mae;
		}

		public void setMae(double mae) {
			this.mae = mae;
		}

		public int getnError() {
			return nError;
		}

		public void setnError(int nError) {
			this.nError = nError;
		}

		public double getUserRmse() {
			return userRmse;
		}

		public void setUserRmse(double userRmse) {
			this.userRmse = userRmse;
		}

		public double getUserMae() {
			return userMae;
		}

		public void setUserMae(double userMae) {
			this.userMae = userMae;
		}

	}
}
