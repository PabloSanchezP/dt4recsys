package es.uam.eps.tfg.structures.trees.bsearchtrees;

import es.uam.eps.tfg.structures.Sorteable;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.BinaryNode;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.RedBlackNode;
import es.uam.eps.tfg.structures.nodes.bsearchnodes.RedBlackNode.Color;
import java.io.Serializable;

/**
 * *
 * Red Black Tree Class. A red black tree is similar to a BinarySearchTree, but
 * it more balanced, so the search method should be always O(log N)
 *
 * @author Pablo
 * @param <T> the element that will have the class
 */
public class RedBlackTree<T extends Sorteable<T>> extends BinaryTree<T> implements Serializable {

    protected RedBlackNode<T> nil;

    /**
     * Constructor of the RedBlackTree. It initializes the sentinel nil and the
     * root.
     */
    public RedBlackTree() {
        super();
        nil = new RedBlackNode<T>(null);
        nil.setColor(Color.BLACK);

        nil.setLeft(nil);
        nil.setRight(nil);
        this.root = nil;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(T element) {
        RedBlackNode<T> node = new RedBlackNode<T>(element);
        RedBlackNode<T> y = this.nil;
        RedBlackNode<T> x = (RedBlackNode<T>) this.root;
        while (!x.equals(this.nil)) {
            y = x;
            int diff = node.getK().sortAgainst(x.getK());

            /* Smaller node */
            if (diff < 0) {
                x = (RedBlackNode<T>) x.getLeft();
            } else if (diff > 0) {
                x = (RedBlackNode<T>) x.getRight();
            } else if (this.duplicatesAllowed == true) {
                x = (RedBlackNode<T>) x.getLeft();
            } else {
                return;
            }
        }
        ((BinaryNode<T>) node).setParent(y);
        /* Root */
        if (y.equals(this.nil)) {
            this.root = (BinaryNode<T>) node;
            ((RedBlackNode<T>) node).setColor(Color.BLACK);
        } /* Smaller */ else if (node.getK().sortAgainst(y.getK()) < 0) {
            y.setLeft((BinaryNode<T>) node);
        } else {
            y.setRight((BinaryNode<T>) node);
        }

        ((BinaryNode<T>) node).setLeft(this.nil);
        ((BinaryNode<T>) node).setRight(this.nil);
        node.setColor(Color.RED);
        fixup((RedBlackNode<T>) node);
        this.totalElements++;
    }

    /**
     * *
     * Fix-up method it is called every time we insert a new node in the tree. A
     * new node may violated the restriction of red-black trees, so we need to
     * maintain the tree in every insertion
     *
     * @param z the node where we apply fixup
     */
    protected void fixup(RedBlackNode<T> z) {
        RedBlackNode<T> y = this.nil;
        while (((RedBlackNode<T>) z.getParent()).getColor() == Color.RED) {

            if (z.getParent().equals(z.getParent().getParent().getLeft())) {
                y = (RedBlackNode<T>) z.getParent().getParent().getRight();

                if (y.getColor() == Color.RED) {
                    ((RedBlackNode<T>) z.getParent()).setColor(Color.BLACK);
                    y.setColor(Color.BLACK);
                    ((RedBlackNode<T>) z.getParent().getParent())
                            .setColor(Color.RED);
                    z = (RedBlackNode<T>) z.getParent().getParent();
                } else {
                    if (z.equals(z.getParent().getRight())) {
                        z = (RedBlackNode<T>) z.getParent();
                        leftRotation(z);
                    }
                    ((RedBlackNode<T>) z.getParent()).setColor(Color.BLACK);
                    ((RedBlackNode<T>) z.getParent().getParent())
                            .setColor(Color.RED);
                    rightRotation((RedBlackNode<T>) z.getParent().getParent());
                }

            } else {
                y = (RedBlackNode<T>) z.getParent().getParent().getLeft();
                if (y.getColor() == Color.RED) {
                    ((RedBlackNode<T>) z.getParent()).setColor(Color.BLACK);
                    y.setColor(Color.BLACK);
                    ((RedBlackNode<T>) z.getParent().getParent())
                            .setColor(Color.RED);
                    z = (RedBlackNode<T>) z.getParent().getParent();
                } else {
                    if (z.equals(z.getParent().getLeft())) {
                        z = (RedBlackNode<T>) z.getParent();
                        rightRotation(z);
                    }
                    ((RedBlackNode<T>) z.getParent()).setColor(Color.BLACK);
                    ((RedBlackNode<T>) z.getParent().getParent())
                            .setColor(Color.RED);
                    leftRotation((RedBlackNode<T>) z.getParent().getParent());
                }
            }
        }
        ((RedBlackNode<T>) this.root).setColor(Color.BLACK);
    }

    /**
     * *
     * Right Rotation method. If we have 2 nodes (1 children of the other),
     * right rotation will make to swap those nodes and fix the children. This
     * method only considers the node in the left
     *
     * @param x the node to apply left rotation.
     */
    protected void rightRotation(RedBlackNode<T> x) {
        RedBlackNode<T> y = (RedBlackNode<T>) x.getLeft();
        x.setLeft(y.getRight());
        if (!y.getRight().equals(this.nil)) {
            y.getRight().setParent(x);
        }
        y.setParent(x.getParent());
        if (x.getParent().equals(this.nil)) {
            this.root = y;
        } else if (x.equals(x.getParent().getRight())) {
            x.getParent().setRight(y);
        } else {
            x.getParent().setLeft(y);
        }
        y.setRight(x);
        x.setParent(y);
    }

    /**
     * *
     * Left Rotation method. If we have 2 nodes (1 children of the other), left
     * rotation will make to swap those nodes and fix the children. THis method
     * only considers the node in the right
     *
     * @param node the node to apply left rotation.
     */
    protected void leftRotation(RedBlackNode<T> node) {
        RedBlackNode<T> aux = (RedBlackNode<T>) node.getRight();
        node.setRight(aux.getLeft());
        if (!aux.getLeft().equals(this.nil)) {
            aux.getLeft().setParent(node);
        }
        aux.setParent(node.getParent());
        if (node.getParent().equals(this.nil)) {
            this.root = aux;
        } else if (node.equals(node.getParent().getLeft())) {
            node.getParent().setLeft(aux);
        } else {
            node.getParent().setRight(aux);
        }
        aux.setLeft(node);
        node.setParent(aux);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T search(T k) {
        BinaryNode<T> x = this.root;
        while (x != null && x.getK() != null && x.getK().sortAgainst(k) != 0) {
            if (x.getK().sortAgainst(k) > 0) {
                x = x.getLeft();
            } else {
                x = x.getRight();
            }
        }
        return x.getK();
    }

    /**
     * *
     * Method to obtain the minimum node of the red black tree
     *
     * @param node the node in which we are going to start searching
     * @return the minimum node
     */
    public BinaryNode<T> minimum(BinaryNode<T> node) {
        BinaryNode<T> x = node;
        while (!x.getLeft().equals(this.nil)) {
            x = x.getLeft();
        }
        return x;
    }

    /**
     * *
     * Method to obtain the maximum node of the red black tree
     *
     * @param node the node in which we are going to start searching
     * @return the maximum node
     */
    public BinaryNode<T> maximum(BinaryNode<T> node) {
        BinaryNode<T> x = node;
        while (!x.getRight().equals(this.nil)) {
            x = x.getRight();
        }
        return x;
    }

    /**
     * Method to transplant a node to another node
     *
     * @param u the first node
     * @param v the second node.
     */
    protected void transplant(BinaryNode<T> u, BinaryNode<T> v) {

        if (u.getParent().equals(this.nil)) {
            this.root = v;
        } else if (u.equals(u.getParent().getLeft())) {
            u.getParent().setLeft(v);
        } else {
            u.getParent().setRight(v);
        }
        v.setParent(u.getParent());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(T element) {
        RedBlackNode<T> z = (RedBlackNode<T>) nodeSearch(element);
        RedBlackNode<T> y = (RedBlackNode<T>) z;
        BinaryNode<T> x;
        Color y_color = y.getColor();
        if (((BinaryNode<T>) z).getLeft().equals(this.nil)) {
            x = ((BinaryNode<T>) z).getRight();
            transplant((BinaryNode<T>) z, ((BinaryNode<T>) z).getRight());
        } else if (((BinaryNode<T>) z).getRight().equals(this.nil)) {
            x = ((BinaryNode<T>) z).getLeft();
            transplant((BinaryNode<T>) z, ((BinaryNode<T>) z).getLeft());
        } else {
            y = (RedBlackNode<T>) minimum(((RedBlackNode<T>) z).getRight());
            y_color = y.getColor();
            x = y.getRight();
            if (y.getParent().equals(z)) {
                x.setParent(y);
            } else {
                transplant(y, y.getRight());
                y.setRight(((BinaryNode<T>) z).getRight());
                y.getRight().setParent(y);
            }
            transplant((BinaryNode<T>) z, y);
            y.setLeft(((BinaryNode<T>) z).getLeft());
            y.getLeft().setParent(y);
            y.setColor(((RedBlackNode<T>) z).getColor());
        }
        if (y_color == Color.BLACK) {
            deleteFixup((RedBlackNode<T>) x);
        }
        this.totalElements--;
    }

    /**
     * *
     * Same method as before but it returns a node, not an element
     *
     * @param k
     * @return
     */
    public BinaryNode<T> nodeSearch(T k) {
        BinaryNode<T> x = this.root;
        while (x != this.nil && x.getK().sortAgainst(k) != 0) {
            if (x.getK().sortAgainst(k) > 0) {
                x = x.getLeft();
            } else {
                x = x.getRight();
            }
        }
        if (x == null) {
            return null;
        }
        return x;
    }

    /**
     * Method that is called after delete. This method maintains the red black
     * property after the deletion by making rotations. (In a similar way that
     * it is used in insertFixup)
     *
     * @param x the node which we are going to fix
     */
    private void deleteFixup(RedBlackNode<T> x) {
        while (x != this.root && x.getColor() == Color.BLACK) {
            if (x.equals(x.getParent().getLeft())) {
                RedBlackNode<T> w = (RedBlackNode<T>) x.getParent().getRight();
                if (w.getColor() == Color.RED) {
                    w.setColor(Color.BLACK);
                    ((RedBlackNode<T>) x.getParent()).setColor(Color.RED);
                    leftRotation((RedBlackNode<T>) x.getParent());
                    w = (RedBlackNode<T>) x.getParent().getRight();
                }
                if (((RedBlackNode<T>) w.getLeft()).getColor() == Color.BLACK
                        && ((RedBlackNode<T>) w.getRight()).getColor() == Color.BLACK) {
                    w.setColor(Color.RED);
                    x = (RedBlackNode<T>) x.getParent();
                } else {
                    if (((RedBlackNode<T>) w.getRight()).getColor() == Color.BLACK) {
                        ((RedBlackNode<T>) w.getLeft()).setColor(Color.BLACK);
                        w.setColor(Color.RED);
                        rightRotation(w);
                        w = (RedBlackNode<T>) x.getParent().getRight();
                    }
                    w.setColor(((RedBlackNode<T>) x.getParent()).getColor());
                    ((RedBlackNode<T>) x.getParent()).setColor(Color.BLACK);
                    ((RedBlackNode<T>) w.getRight()).setColor(Color.BLACK);
                    leftRotation((RedBlackNode<T>) x.getParent());
                    x = (RedBlackNode<T>) this.root;
                }

            } else {
                RedBlackNode<T> w = (RedBlackNode<T>) x.getParent().getLeft();
                if (w != null) {
                    if (w.getColor() == Color.RED) {
                        w.setColor(Color.BLACK);
                        ((RedBlackNode<T>) x.getParent()).setColor(Color.RED);
                        rightRotation((RedBlackNode<T>) x.getParent());
                        w = (RedBlackNode<T>) x.getParent().getLeft();
                    }
                    if (((RedBlackNode<T>) w.getRight()).getColor() == Color.BLACK
                            && ((RedBlackNode<T>) w.getLeft()).getColor() == Color.BLACK) {
                        w.setColor(Color.RED);
                        x = (RedBlackNode<T>) x.getParent();
                    } else {
                        if (((RedBlackNode<T>) w.getLeft()).getColor() == Color.BLACK) {
                            ((RedBlackNode<T>) w.getRight()).setColor(Color.BLACK);
                            w.setColor(Color.RED);
                            leftRotation(w);
                            w = (RedBlackNode<T>) x.getParent().getLeft();
                        }
                        w.setColor(((RedBlackNode<T>) x.getParent()).getColor());
                        ((RedBlackNode<T>) x.getParent()).setColor(Color.BLACK);
                        ((RedBlackNode<T>) w.getLeft()).setColor(Color.BLACK);
                        rightRotation((RedBlackNode<T>) x.getParent());
                        x = (RedBlackNode<T>) this.root;
                    }

                }
            }
        }
        x.setColor(Color.BLACK);
    }

    @Override
    public String getStructure() {
        return "RedBlackTree";
    }

    @Override
    public String toString() {
        return toStringRevRB((RedBlackNode<T>) this.root);
    }

    private String toStringRevRB(RedBlackNode<T> node) {
        String result = "";
        /*
		 * As the implementation of red Black node uses the sentinel nil, we
		 * need to check if the node's key is null
         */
        if (node == null || node.getK() == null) {
            return result;
        }
        result += node.toString();
        if (node.getColor() == Color.BLACK) {
            result += "b";
        } else {
            result += "r";
        }
        result += " (" + toStringRevRB((RedBlackNode<T>) node.getLeft()) + ") ("
                + toStringRevRB((RedBlackNode<T>) node.getRight()) + ")";
        return result;
    }

}
